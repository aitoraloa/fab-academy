# Embedded Programming

For this week’s individual assignment I had to read the [ATtiny44A](https://www.microchip.com/wwwproducts/en/ATtiny44A) datasheet, in order to familiarize with it. Later, I had to program [echo hello-world board](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) that I had redraw during the Electronics Design week, and make it do as many different things as possible (light the LED when the button is pushed, send a message to the computer when the button is pushed, etc) using as many different programing languages and programing environments as possible.

On the other hand, as group assignment we needed to compare the performance and development workflows for other architectures.


## **Reading the ATtiny44A datasheet**

[Sparkfun’s How to Read a Datasheet tutorial](https://www.sparkfun.com/tutorials/223)

[ATtiny44A Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/doc8183.pdf)
[](https://www.sparkfun.com/tutorials/223)


### **Features** 

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526924879585_attiny44a-datasheet.png)


### **Pin configurations**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526924980665_attiny44a-datasheet+copia.png)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526922059643_687474703a2f2f6472617a7a792e636f6d2f652f696d672f50696e6f7574543834612e6a7067.jpeg)


### **Clock System**

- Clock sources
- Crystal Oscillator / Ceramic Resonator
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526927081704_attiny44a-datasheet+copia+5.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526927104084_attiny44a-datasheet+copia+6.png)


### **I/O Ports**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1527272416855_attiny44a-datasheet+copia+10.png)


### **Timer/Counter Prescaler**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1527270711455_attiny44a-datasheet+copia+7.png)


### **Analog comparator**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1527272607290_attiny44a-datasheet+copia+11.png)


**Analog to digital converter**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1527272622143_attiny44a-datasheet+copia+12.png)


### **Electrical Characteristics**

- Absolute Maximum Ratings
- DC Characteristics
![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526925636882_attiny44a-datasheet+copia+3.png)


### **Typical Characteristics**

- ATtiny44A


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526926848381_attiny44a-datasheet+copia+4.png)


### **Ordering information**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1527271045060_attiny44a-datasheet+copia+8.png)


### **Packaging information**

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1527271096822_ATtiny24A-44A-84A.png)


And I would like to learn more about re-programing fuses, how the ADC, interrupts, …



## **Programming my echo hello-world with C**

In order to check that my hello.fdti.44 with LED and button board was working fine, I had already programmed it using C during the Electronics Design assignment.

Anyway, the C code that I had used for testing my board was written with the original hello.fdti.44 board in mind (which does not have a LED or a button), so the C code used did not include any parameter to control the LED or the push-button of the re-drawn hello.fdti.44 board.

In sum, for making these two parts to do something I needed to modify the existing C code, or to write my own C code from scratch. 

My experience with C was quite limited at the time of this writing, and despite that I was trying to learn in full swing, I was still quite a newbie. The best idea was to follow Neil’s suggestion an use somebody else code so I looked at examples from Fab Academy students from previous years.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526907973829_IMG-3434.JPG)


### **Turning the LED on**

Used Brackets to create a C file named “hello.ftdi.44.turnledon.c” with two simple instructions to turn the LED on

Enabled the pin connected to the LED to work as an output

Instructed the micro controller to turn on the output pin (and light the LED up)

    $ // Turn LED on
    
    #include <avr/io.h> 
    // AVR device-specific IO definitions
    // https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__io.html
    
    /*
    Pin Mapping
    LED = PA7 = Pin 7
    */
    
    
    int main(void)
    {
    DDRA = _BV(PA7); // Enable output on the LED pin 7
    PORTA = _BV(PA7); // Turn LED on
    }

Used the same MAKE file that I had used before but changing the 1st line to 

    PROJECT=hello.ftdi.44.turnledon 

and also adjusted the frequency:

    PROJECT=hello.ftdi.44.blinkfasterwhenbuttonpushed
    SOURCES=$(PROJECT).c
    MMCU=attiny44
    F_CPU = 20000000
    
    CFLAGS=-mmcu=$(MMCU) -Wall -Os -DF_CPU=$(F_CPU)
    
    $(PROJECT).hex: $(PROJECT).out
        avr-objcopy -O ihex $(PROJECT).out $(PROJECT).c.hex;\
        avr-size --mcu=$(MMCU) --format=avr $(PROJECT).out
     
    $(PROJECT).out: $(SOURCES)
        avr-gcc $(CFLAGS) -I./ -o $(PROJECT).out $(SOURCES)
     
     program-bsd: $(PROJECT).hex
        avrdude -p t44 -c bsd -U flash:w:$(PROJECT).c.hex
    
    program-dasa: $(PROJECT).hex
        avrdude -p t44 -P /dev/ttyUSB0 -c dasa -U flash:w:$(PROJECT).c.hex
    
    program-avrisp2: $(PROJECT).hex
        avrdude -p t44 -P usb -c avrisp2 -U flash:w:$(PROJECT).c.hex

    program-avrisp2-fuses: $(PROJECT).hex
        avrdude -p t44 -P usb -c avrisp2 -U lfuse:w:0x5E:m
    
    program-usbtiny: $(PROJECT).hex
        avrdude -p t44 -P usb -c usbtiny -U flash:w:$(PROJECT).c.hex
    
    program-usbtiny-fuses: $(PROJECT).hex
        avrdude -p t44 -P usb -c usbtiny -U lfuse:w:0x5E:m
    
    program-dragon: $(PROJECT).hex
        avrdude -p t44 -P usb -c dragon_isp -U flash:w:$(PROJECT).c.hex
    
    program-ice: $(PROJECT).hex
        avrdude -p t44 -P usb -c atmelice_isp -U flash:w:$(PROJECT).c.hex

Created a new folder and save the two files in the same folder

Navigated using the terminal to the folder

Typed

    $ make -f hello.ftdi.44.turnledon.c.make

and the console prompted

    avr-gcc -mmcu=attiny44 -Wall -Os -DF_CPU=20000000 -I./ -o hello.ftdi.44.turnledon.out hello.ftdi.44.turnledon.c
    hello.ftdi.44.turnledon.c: In function 'main':
    hello.ftdi.44.turnledon.c:15:1: warning: control reaches end of non-void function [-Wreturn-type]
    }
    ^
    avr-objcopy -O ihex hello.ftdi.44.turnledon.out hello.ftdi.44.turnledon.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.turnledon.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:      64 bytes (1.6% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)

Meaning that the make file compiled de C file into the hex file

Typed


    $ make -f hello.ftdi.44.turnledon.c.make program-usbtiny-fuses

and the console prompted

    avr-objcopy -O ihex hello.ftdi.44.turnledon.out hello.ftdi.44.turnledon.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.turnledon.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:      64 bytes (1.6% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)
    
    avrdude -p t44 -P usb -c usbtiny -U lfuse:w:0x5E:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0x5E"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0x5E:
    avrdude: load data lfuse data from input file 0x5E:
    avrdude: input file 0x5E contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:5E)
    
    avrdude done.  Thank you.

Meaning that the fuses had been set

Typed

    $ make -f hello.ftdi.44.turnledon.c.make program-usbtiny

and console prompted

    avr-objcopy -O ihex hello.ftdi.44.turnledon.out hello.ftdi.44.turnledon.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.turnledon.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:      64 bytes (1.6% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)
    
    avrdude -p t44 -P usb -c usbtiny -U flash:w:hello.ftdi.44.turnledon.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.ftdi.44.turnledon.c.hex"
    avrdude: input file hello.ftdi.44.turnledon.c.hex auto detected as Intel Hex
    avrdude: writing flash (64 bytes):
    
    Writing | ################################################## | 100% 0.10s
    
    avrdude: 64 bytes of flash written
    avrdude: verifying flash memory against hello.ftdi.44.turnledon.c.hex:
    avrdude: load data flash data from input file hello.ftdi.44.turnledon.c.hex:
    avrdude: input file hello.ftdi.44.turnledon.c.hex auto detected as Intel Hex
    avrdude: input file hello.ftdi.44.turnledon.c.hex contains 64 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 0.14s
    
    avrdude: verifying ...
    avrdude: 64 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:5E)
    
    avrdude done.  Thank you.

Meaning that the hex file had been loaded to the board.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526908003071_IMG-3435.JPG)


### **Turning the LED on and blink**

Used Brackets to create a C file named “hello.ftdi.44.turnledonandblink.c”


    // Turn LED on and blink
    
    #include <avr/io.h> 
    // AVR device-specific IO definitions
    // https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__io.html
    
    #include <util/delay.h> 
    // Convenience functions for busy-wait delay loops 
    // https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__util__delay.html
    
    /*
    Pin Mapping
    LED = PA7 = Pin 7
    */
    
    int main(void)
    {
    
            DDRA = _BV(PA7); // Enable output on the LED pin 7
    
    
      while(1) // Start the loop
      {
          PORTA |= _BV(PA7); // Turn LED on
          _delay_ms(100); // Wait 100 milliseconds
          PORTA = 0; // Turn LED off
          _delay_ms(100); // Wait 100 milliseconds 
      }
    }

Edited the MAKE file

    PROJECT=hello.ftdi.44.turnledonandblink
  
Created a new folder and save the two files in the same folder

Navigated using the Terminal to the folder

Used the same make commands


    $ make -f hello.ftdi.44.turnledonandblink.c.make

    $ make -f hello.ftdi.44.turnledonandblink.c.make program-usbtiny-fuses

    $ make -f hello.ftdi.44.turnledonandblink.c.make progrma-usbtiny


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526909761692_blink_optimized.gif)



### **Blink faster when the button is pushed**


Used Brackets to create a C file named “hello.ftdi.44.blinkfasterwhenbuttonpushed.c”


    // LED blink when button pushed 
    
    #include <avr/io.h> 
    // AVR device-specific IO definitions
    // https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__io.html
    
    #include <util/delay.h> 
    // Convenience functions for busy-wait delay loops 
    // https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__util__delay.html
    
    /*
    Pin Mapping
    LED = PA7 = Pin 7
    Button = PA3 = Pin 3 
    */
    
    int main(void)
    {
    
        DDRA = _BV(PA7); // Enable output on the LED pin 7
        PORTA|= _BV(PA3); // Turn pull-up resistor on the button pin 3
      
      while(1) // Start the loop
      {
        if(PINA & _BV(PA3)) // if button is not pressed
        {
            PORTA |= _BV(PA7); // Turn LED on
            _delay_ms(100); // wait 100 milliseconds
    
            PORTA = 0; // Turn LED off
            _delay_ms(100); // wait 100 milliseconds
        } 
          
        else 
        {
            PORTA |= _BV(PA7); // Turn LED on
            _delay_ms(10); // wait 10 milliseconds
    
            PORTA = 0; // Turn LED off
            _delay_ms(10); // wait 10 milliseconds
        }
      }
    }


Edited the MAKE file

    PROJECT=hello.ftdi.44.blinkfasterwhenbuttonpushed

Created a new folder and save the two files in the same folder

Navigated using the Terminal to the folder

Used the same make commands

    $ make -f hello.ftdi.44.blinkfasterwhenbuttonpushed.c.make  

    $ make -f hello.ftdi.44.blinkfasterwhenbuttonpushed.c.make program-usbtiny-fuses 

    $ make -f hello.ftdi.44.blinkfasterwhenbuttonpushed.c.make progrma-usbtiny


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526910981446_button_optimized.gif)



## **Programming the echo hello-world board with Arduino IDE**

### **Installing Arduino IDE**


Installed Arduino IDE following the [guide](https://www.arduino.cc/en/Guide/HomePage) 


### **Adding ATtiny support  to the Arduino IDE**


Installed ATtiny support in Arduino 1.6.4

[https://github.com/damellis/attiny](https://github.com/damellis/attiny)

[http://highlowtech.org/?p=1695](http://highlowtech.org/?p=1695)


### **Making the LED blink**


Launched Arduino IDE


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526911314902_Captura+de+pantalla+2018-05-21+a+las+15.58.12.png)

Checked that the boards were still connected

Prepared a sketch based on Arduino IDE’s blink example

![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526911335535_Captura+de+pantalla+2018-05-21+a+las+13.51.31+2.png)


    void setup() {
      pinMode(A7, OUTPUT);
    }
    
    void loop() {
      digitalWrite(A7, HIGH);
      delay(5000);
      digitalWrite(A7, LOW);
      delay(5000);
    }

Checked that the board and communication settings were ok


![](https://d2mxuefqeaa7sj.cloudfront.net/s_33AEDE612E087B9E659174928C66927160AAAC3227FA13C3370A4BAC084B72B0_1526911356712_Captura+de+pantalla+2018-05-21+a+las+15.59.35.png)


Uploaded the code

It worked!


## **Completing the group project**

probe a little board with the osciloscope and learn how to read the bit rate to check out the bit timing



## **What I have learned**

How to read a Datasheet

Installing ATtiny support in Arduino IDE

[Pull-up resistors](https://learn.sparkfun.com/tutorials/pull-up-resistors)



## **Issues that I had**

Understanding the Datasheet


## **Tools used**


- MacBook Pro with macOS High Sierra v10.13.3
- Preview v10.0
- Brackets v1.12
- Terminal v2.8
- Arduino IDE v1.6.4
- FabISP programmer
- Echo Hello-World board with LED and Button
- USB-A to mini-USB cable
- FTDI Serial TTL-232 USB cable
- 6 pin ISP ribbon cable 



## **Files to download**

