# Electronics Design

For [this week’s assignment](http://academy.cba.mit.edu/classes/electronics_design/index.html) we had to redraw the [echo hello-world board](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) using any of the [Electronic Design Automation (EDA) software](https://en.wikipedia.org/wiki/Electronic_design_automation) packages mentioned during the weekly lecture, and adding (at least) one [push-button](https://en.wikipedia.org/wiki/Push-button) and one [LED (Light Emitting Diode)](https://en.wikipedia.org/wiki/Light-emitting_diode) to the circuit. 

With the echo hello-world board redrawn, we had to check the design rules, make it, and test it.


## **Getting to know the echo hello-world board**

The echo hello-world board is a simple board with an [ATTiny44 micro-controller](https://www.digikey.es/product-detail/es/microchip-technology/ATTINY44A-SSU/ATTINY44A-SSU-ND/1914708) built-in that a computer can talk to, and that it can talk back to the computer, thanks to its little processor.

![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511287179920_hello.ftdi.44.png)


As I did in the Electronics Production week, I started this assignment by downloading all the PNG files related to the echo hello-world board that I was able to find ([board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.png), [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.components.jpg), [traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.traces.png), [interior](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.interior.png), and [programming](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.program.png)). Then, I saved all these files in my student folder at [IAAC’s cloud](https://cloud.iaac.net:5001/index.cgi). 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511287250224_hello.ftdi.44.traces.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511287250220_hello.ftdi.44.interior.png)


Note that the aforementioned files where not in the [Fab Academy’s “Electronics Design” week page](http://academy.cba.mit.edu/classes/electronics_design/index.html), but that there was a link in the “**assignment**” area at the end of that web page that sent me to the [Fab Academy’s “Embedded Programming” week page](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo), where these PNG images (plus other programming files) of the echo hello-world board were available.



## **Redrawing the echo hello-world board**

I would have loved to use for this assignment more than one of the EDA software packages presented during the lecture, but as a newbie in electronics design I guessed that it would have taken me much more time than desired to learn how to use them, so I decided to use only one. 

Although other EDA software packages such as [Autodesk Circuits](https://circuits.io/) or [KiCad](http://kicad-pcb.org/) were also very appealing to me (and I definitely plan to practice with them as soon as I can), the software chosen to complete this part of the assignment was [EAGLE](https://www.autodesk.com/products/eagle/overview).

[As seen in the wikipedia](https://en.wikipedia.org/wiki/EAGLE_(program)), EAGLE is a scriptable electronic design automation application with [schematic capture](https://en.wikipedia.org/wiki/Schematic_capture), [printed circuit board (PCB)](https://en.wikipedia.org/wiki/Printed_circuit_board) [layout](https://en.wikipedia.org/wiki/PCB_layout), [auto-router](https://en.wikipedia.org/wiki/Auto-router) and [computer-aided manufacturing (CAM)](https://en.wikipedia.org/wiki/Computer-aided_manufacturing) features owned also by [Autodesk](https://www.autodesk.com/). 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510766155820_redrawing-the-echo-hello-world-board-using-eagle-01.jpg)


By the time I was completing this assignment, there was a (restricted) [freeware version available online for hobbyists and makers](https://www.autodesk.com/products/eagle/free-download). Besides, the amount of EAGLE tutorials for beginners available online was huge, making the decision of using it ideal for someone with zero experience like me.  

On the other hand, DIY electronics giants like [Arduino](https://www.arduino.cc/), [Sparkfun](https://www.sparkfun.com/) and [Adafruit](https://www.adafruit.com/) offer part libraries for EAGLE. And there is also a [Fab Library (fab.lbr)](https://github.com/Academany/FabAcademany-Resources/blob/master/files/fab.lbr) maintained by the Fab Network, which turned out to be essential in order to complete this part of the assignment smoothly. 


**Installing EAGLE** 

Following the [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html) available at the English section of the [docs.academany.org web site](http://docs.academany.org/FabAcademy-Tutorials/_book/en/), I [downloaded the latest Mac OS X installer](https://www.autodesk.com/eagle-download-mac) available at the time, and followed the onscreen installation instructions, until the installation process had finished.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523527790759_Captura+de+pantalla+2018-04-12+a+las+12.09.06.png)


Once the EAGLE installation was completed, I launched the application. But as the freeware version of EAGLE was a subscription-based cloud tool, it asked me to create an Autodesk account before I could start using the software program. 

Luckily, I already had an Autodesk account created from before, so I just had to sign in, and EAGLE’s program window (or Control Panel) appeared as soon as my details were validated.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510766147620_redrawing-the-echo-hello-world-board-using-eagle-02.jpg)

~~****~~

**Downloading the “hello.fdti.44 schematic” file**

According to the [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html), the next step should have had been to download the echo hello-world board schematic example, and then add the necessary components to it. But the link for downloading the “hello.fdti.44 schematic” file in that tutorial was broken, so I had to search for it online. 

Fortunately, I rapidly found a [web page inside the docs.academany.org platform](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english/hello-ftdi-44.sch) were the content of the “**hello.fdti.44.sch**” file could be seen as plain XML text file. Once that page was completely loaded, I right-clicked over it and selected the “Save as” option from the drop-down menu, saving the downloaded file on my computer as “**hello.fdti.44.sch.xml**”.

Then, I used the file manager properties on my computer to remove the XML extension, leaving the name of the downloaded file as “**hello.fdti.44.sch**”, which allowed me to open it with EAGLE.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510766134399_redrawing-the-echo-hello-world-board-using-eagle-03.jpg)

~~****~~
Once the schematic example of the “Hello Echo” board was opened with EAGLE, I could have [created a project adding the existing schematic](https://www.element14.com/community/message/125735/l/how-to-add-an-existing-schematic-to-a-project#125735). But I felt much more tempted to start the echo hello-world board’s schematic from scratch, instead of just adding the push-button, the LED and the two corresponding resistors to an schematic that was practically done.

Anyway, for any of the two options that I would had decided to chose, the next step had to be downloading and installing [Fab Lab Network’s EAGLE library](https://github.com/Academany/FabAcademany-Resources/blob/master/files/fab.lbr). Supposedly, that “fab.lbr” file contained most of the symbols for the electronic components listed in the Fab Inventory. Or at least, all those components to be used for designing the hello.fdti.44’s electronic schematic.


**Installing the “fab.lbr” library**

In order to install the “fab.lbr” library, I started by downloading it from [Fab Academy’s Electronics Design page](http://academy.cba.mit.edu/classes/electronics_design/index.html). It was available under “circuits”, right next to EAGLE and Fusion360 links.

Immediately after, I moved the downloaded “fab.lbr” file from my computer’s “Downloads” folder to the “lbr” folder inside the EAGLE installation directory (“~/Aplicaciones/EAGLE-8.0.2/lbr”), where it will be sharing space with hundreds of other libraries that EAGLE includes by default.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510766123396_redrawing-the-echo-hello-world-board-using-eagle-05.jpg)


The next step was making sure EAGLE knew that I wanted to use the “**fab.lbr**” library.

I went back to EAGLE’s control panel, and in the navigation area on the left-side of the screen program, I expanded the “**Libraries”** drop down menu by clicking on the arrow.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523529597870_Captura+de+pantalla+2018-04-12+a+las+12.39.41.png)


Then, I searched for “**fab.lbr**” in the “**Libraries”** drop down menu. 

Once I found it, I right-clicked on it, and I selected the ”**Use**” option from the drop-down menu.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510767600231_redrawing-the-echo-hello-world-board-using-eagle-06.jpg)


And that was it! The “**fab.lbr**” library was correctly activated, and I could start creating my first PCB design using EAGLE. You can not imagine how happy I was!! 😛 


**Creating a new project**

As far as I have had understood while reading [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic) (absolutely recommended lecture), PCB design in EAGLE is a two-step process. I had to design the schematic for the “**hello.fdti.44**” board first, and then I had to layout a PCB based on that schematic. 

In any case, the very first thing that I needed to address before I could any of the two aforementioned steps was to create a new project folder. And in order to do that, I had to use EAGLE’s control panel once again.

I started by expanding the “**Projects**” drop down menu in the navigation area on the left-side of the screen program. Then, I right-clicked on the “**eagle**” folder,  and I selected the “**New Project**” option from the drop-down menu.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510770140745_redrawing-the-echo-hello-world-board-using-eagle-07.jpg)


Once the “**New_Project**” folder was created, it was recommended to give a more descriptive name to it, so I changed the name given by default to “**hello.fdti.44.aitor**”.

This “**hello.fdti.44.aitor**” folder was going to house both the schematic and board design files that I was about to design. And eventually, it could also house the [Gerber files](https://en.wikipedia.org/wiki/Gerber_format) created later. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510770665477_redrawing-the-echo-hello-world-board-using-eagle-08.jpg)



**Designing the schematic**

With the project folder created and properly named, it was time to begin with the design process by laying out the schematic, so I right-clicked the “**hello.fdti.44.aitor**” folder, and I selected “**New > Schematic**” in the drop-down menu that appeared.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510785627032_redrawing-the-echo-hello-world-board-using-eagle-09.jpg)


As soon as I selected the “**Schematic**” option from the drop-down menu tree, a new blank window (also know as the schematic editor) popped up in EAGLE’s screen program.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1510785627041_redrawing-the-echo-hello-world-board-using-eagle-10.jpg)


According to the [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic) that I was still following, the schematic design is also a two step process in which I had to start by adding all of the parts to the schematic sheet. And once all the parts were added to the schematic sheet, I had to wire all those parts together. 

Sparkfun’s EAGLE tutorial also says that these two steps can be intermixed (adding a few parts, wiring a few parts, then adding some more), but taking advantage of that there was a reference schematic of the “hello.fdti.44 with LED and button” available in Fab Academy’s  [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html), I preferred to add all the parts to the schematic at once. 

In order to start adding parts to the schematic editor, I maximized the schematic editor window (manias that I have), and I typed "add” in the schematic sheet’s command line.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511289398741_redrawing-the-echo-hello-world-board-using-eagle-11.jpg)


Then, I pressed “Enter” on my keyboard, and the “Add” dialog window (also know as “library navigator”) opened, showing a list with the specific libraries installed in EAGLE (at the left side).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511289848825_redrawing-the-echo-hello-world-board-using-eagle-12.jpg)


I looked through the listed libraries until I found the “fab” library. And once I found it, I expanded the "fab” library’s accordion menu by clicking the arrow.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511289848788_redrawing-the-echo-hello-world-board-using-eagle-13.jpg)


From the “fab” library’s accordion menu, I selected the "6MM_SWITCH” part, and the two windows on the upper right-side of the screen program were updated showing both the schematic symbol of the push-button, and its package.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511289966714_redrawing-the-echo-hello-world-board-using-eagle-14.jpg)


Then, I clicked the “OK” button at the lower left-side corner of the library navigator, and the library navigator closed taking me back to the schematic editor.

In the schematic editor window, the symbol of the push-button that I had jus selected could be seen hovering around and following the mouse. 

In order to place it on the schematic sheet, I left-clicked once. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1511290551445_redrawing-the-echo-hello-world-board-using-eagle-15.jpg)


Note that after placing the push-button on the schematic sheet, the add tool was still active and the program was assuming that I wanted to add another push-button to the schematic.

This was not the case, so I had to hit the escape (ESC) key once to get out of the “add-mode”.

Hitting the ESC key took me back to the “library navigator”, where I had to repeat the very same process as many times as necessary until I added to the schematic window all the other fab library parts that were necessary to complete the “hello.fdti.44 with LED and button” schematic:


- 1x ATTINY44SSU (Microcontroller)
- 1x AVRISPSMD (ISP header)
- 1x CAP-UAS1206FAB (Capacitor)
- 1x FTDI-SMD-HEADER (FTDI header)
- 1x LED1206FAB (Light Emitting Diode)
- 3x RES-US1206FAB (Resistor)
- 1x RESONATOR (Resonator)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513183712606_Captura+de+pantalla+2017-12-13+a+las+17.48.13.png)

~~****~~
In addition to the parts of the “fab” library that I had already added to the schematic window, I also had to add parts from other libraries, because the “VCC” and “GND” symbols that I needed to complete the “hello.fdti.44 with LED and button” schematic were not available in the “fab” library.

As explained in [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic), the search functionality of the library navigator is very helpful in order to find a part among the dozens of libraries present. Having no idea where to find the “VCC” and “GND” symbols, I though that using this feature will be really helpful. 

So, I introduced the term “VCC” in the “Search” field at the lower-left side of the library navigator, and two options appeared in the library navigator, inside the “supply1” and “supply2” libraries. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513184200602_Captura+de+pantalla+2017-12-13+a+las+17.55.47.png)


The right “VCC” symbol in this case was the one inside the “supply1” library, so I selected it and left-clicked the “OK” button at the lower left-side corner of the library navigator. 

Back in the schematic editor, I placed four “VCC” symbols on the schematic sheet.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513184457567_Captura+de+pantalla+2017-12-13+a+las+17.55.49.png)


Then, I searched for “GND”. As with the previous search, two different “GND” symbols were available inside the library navigator’s “supply1” and “supply2” libraries. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513184457551_Captura+de+pantalla+2017-12-13+a+las+17.56.02.png)


The “GND” symbol inside the “supply1” library was the right one, so I selected it, left-clicked the “OK” button, and placed five of them on the schematic sheet.

Then, I hit the escape (ESC) key twice. The first hit to got out from the add-mode and go back to the library navigator, as I had previously done after adding other symbols. And the second one to close the library navigator and go back to the schematic editor.

~~****~~
![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513184457593_Captura+de+pantalla+2017-12-13+a+las+17.59.51.png)


At that point, all the parts that I needed in order to lay out the schematic of the “hello.fdti.44 with LED and button” were placed in the schematic editor. 

Anyway, some of them were not oriented as they appeared in the reference schematic of the [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html), so I had to use the “Rotate” command (also available in the left toolbar or under the Edit menu) in order to orient them as in the reference schematic.

After a little while using the “Rotate” tool, all the parts in the schematic were correctly oriented. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513193371613_redrawing-the-echo-hello-world-board-using-eagle-21.jpg)


It was time to save the work done so far and… wire all those parts together! 😉 

Anyway, before I could start wiring the parts on the schematic editor, I also had to move some of them around so they were more or less placed like in the reference schematic, as this would make wiring them much more easier.

In order move the parts, I used the “Move” command (also available in the left toolbar or under the Edit menu). Once the “Move” tool was activated, I left-clicked over each part’s red “+” to pick it up, and I left-clicked again when they were placed in the right spot.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513194263646_Captura+de+pantalla+2017-12-13+a+las+20.44.05.png)


With all the parts for the “hello.fdti.44 with LED and button” inserted in the schematic editor, correctly oriented, and properly placed on the schematic sheet, I could finally start wiring them!

Following the instructions given in the “Using the NET Tool” section of  [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic), I used the “Net” command (also available in the left toolbar or under the Edit menu) to start joining all the parts together.

With the schematic reference of the “hello.fdti.44 with LED and button” as guide, I wired the capacitor, the LED, and the push-button to their corresponding resistor, “VCC” and/or “GND” symbols.  


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513194596410_Captura+de+pantalla+2017-12-13+a+las+20.49.42.png)


Then, I wired the the pins “1” (VCC) and “14” (GND) of the micro controller to their corresponding “VCC” and “GND” symbols. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513194841792_Captura+de+pantalla+2017-12-13+a+las+20.53.51.png)


And the pin “4” [(PCINT11/RESET/DW) PB3] of the micro controller to “VCC”  through the corresponding resistor.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513195015489_Captura+de+pantalla+2017-12-13+a+las+20.56.44.png)


Finally, I wired the pins “2” [(PCINT8/XTAL1/CLK) PB0] and “3” [(PCINT9/XTAL2) PB1] of the micro controller to the resonator. And the resonator to the last “GND” symbol available.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513207567169_Captura+de+pantalla+2017-12-14+a+las+0.25.56.png)


At that point, all the connections that could be completed using the “net” tool were done, but there were still many other connections waiting to be made. Unfortunately, these remaining connections could not be “wired” using the “net” tool because they could not be cleanly route. 

As explained in the “Making Named, Labeled Net Stubs” section of [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic), for these nets that I could not cleanly route, I needed to use the “Name” and “Label” commands (also available in the left toolbar or under the Edit menu). 

So, I started this second part of the wiring process by using the “net” tool again, but adding short, one-sided nets to each of the pins of the FTDI header and the ISP header.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513207714380_Captura+de+pantalla+2017-12-14+a+las+0.28.22.png)


Then, I also added one-sided nets to the pins of the micro controller that had not been wired yet, except for pins “5” [(PCINT10/INT0/OC0A/CKOUT) PB2], and “11” [(PCINT2/AIN1/ADC2) PA2].


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513208000917_Captura+de+pantalla+2017-12-14+a+las+0.33.07.png)


And I also added a short, one-sided net to the resistor that was connected to the LED.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261256881_Captura+de+pantalla+2017-12-14+a+las+0.33.08.png)


Once all the one-sided nets were created, I used the “Name” command to name each of them.

With the “Name” tool active, I clicked the one-sided net connected to the “VVC” pin of the ISP header, and a new dialog window (“Name”) opened. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261269093_Captura+de+pantalla+2017-12-14+a+las+0.39.52.png)


I deleted the auto-generated name, and replaced it with the corresponding pin name (VCC).

Then, I clicked the “OK” button in the lower right-side corner of the “Name” dialog window.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261289502_Captura+de+pantalla+2017-12-14+a+las+0.39.55.png)


As soon as I clicked the “OK” button, the “Name” dialog window closed, and a warning dialog window appeared asking if I wanted to connect the selected net to “VCC”. 

As that was what I wanted, I clicked “Yes”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261303393_Captura+de+pantalla+2017-12-14+a+las+0.40.00.png)


After that, I repeated the very same process with all the other pins of the ISP header. 

Note that the warning dialog did not appear when I was naming every pin on the ISP header. Actually, it only appeared when I named the “VCC” and the “GND” pins. Later, when I was naming the nets of the micro controller and the FTDI header, I realized that this warning dialog only appears if there is another pin already named the same in the schematic.

Anyway, after naming all the nets of the ISP header, I used the “Label” command (also available in the left toolbar or under the Edit menu) to add a text label to each of the ISP header pins. 

With the “Label” tool selected, I left-clicked on the “VCC” net. This spawned a piece of text with the name of the net, and I jus had to left-click again on the “VCC” net to place the label down right on top of the net.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261315810_Captura+de+pantalla+2017-12-14+a+las+11.19.38.png)


Then, I repeated the “labeling” process with the rest of the pins on the ISP header.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261342710_Captura+de+pantalla+2017-12-14+a+las+11.24.05.png)


Once the ISP header was done, I used the reference schematic as guide to repeat the naming and labeling process with the nets of the FTDI header and the micro controller.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523261372795_Captura+de+pantalla+2017-12-14+a+las+11.38.48.png)


Then, I also named and labeled the net of the LED’s resistor. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513251308567_Captura+de+pantalla+2017-12-14+a+las+12.32.16.png)


Just before I was going to terminate this part of the wiring process, I realized that the button’s net in the reference schematic was also named and labeled, so I completed this last part by repeating the very same process that I had already done with all the other parts.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513251326203_Captura+de+pantalla+2017-12-14+a+las+12.32.18.png)


At that moment, the schematic layout was completed. Niceeeee! 

But before moving forward and creating the board, I followed the “Tips and Tricks” section of [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic) in order to complete the values of the resistors and the capacitor. 

And I also verified that all the connections between parts were ok,  using the “Show” command.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513270756033_Captura+de+pantalla+2017-12-14+a+las+12.32.19.png)


Everything seemed to be ok, so could move to the next stage and start with the board layout! 


**Laying out the board**

In order to create a board from the schematic, I followed [the second part of Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-board-layout). This schematic-to-board process in EAGLE was extensively explained there, and it was very good idea reading this document before starting.

Once I finished reading the tutorial, the first thing I did was switching from the schematic editor to the related board, by clicking the “Generate/Switch to Board*”* button on the top toolbar (also available as “Switch to Board” under “File” in the main menu bar).

As soon as I clicked the “Generate/Switch to Board” button, a new dialog window (also known as as “board editor”) opened. All the parts that I had previously added in the schematic sheet were in that new dialog window but in a disorderly manner, so I needed to correctly place and route them.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513271782325_Captura+de+pantalla+2017-12-14+a+las+18.01.39.png)


Using the “Move” and “Rotate” commands, I arranged all the parts in the lower left corner of the white frame of the board editor, so my layout was as close as possible to the echo hello board layout showed in the [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html) that I was still checking at every step.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513271789511_Captura+de+pantalla+2017-12-14+a+las+18.15.54.png)


Once the parts were correctly placed and oriented in the board editor window, I needed to adjust the dimension layer (represented by the four thin dimension lines of the white frame). 

In order to do so, I started by erasing the four thin dimension lines that were in the board editor from the beginning.  Then, I activated the “Wire” command, and set the layer to “20 Dimension” in the layers drop-down menu of the the top toolbar.  And finally, I drew a box around the parts.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513272939803_Captura+de+pantalla+2017-12-14+a+las+18.35.25.png)


With the parts laid out and the dimension layer adjusted, I was time to start routing some copper!

In order to do that, the first thing I did was activating the “Route” command, and checking that the options available at the toolbar (bend style, radius, width, etc) for this tool were the right ones. 

Then, I started routing all the parts by left-clicking on the pins (where the airwires terminated).

Note that while making the routes, I had to activate the grid (View > Grid at the main menu bar), so I could have a precise control. And that I also had to made a few slight changes in the position of some of the parts so there was enough space between two different signal traces.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1513275726493_Captura+de+pantalla+2017-12-14+a+las+19.19.01.png)


Once the board was completely routed, it was time to check if there was any error.

According to [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic), the first check was making myself sure that I was actually routed all of the nets in my schematic. 

In order to do that, I hit the “RATSNEST” icon at the bottom of the left-hand toolbar. 

Immediately after, I checked the bottom left status box, and the message in the bottom left status box was “Ratsnest: Nothing to do!”, meaning that I had made every route required.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523225055173_Captura+de+pantalla+2018-04-09+a+las+0.02.39.png)


Once the “ratsnest” check was done, there was one more check to do in order to find any possible error. This check was the “Design Rule heck” or DRC.
 

**Checking  the design rules**

As seen in the wikipedia, the [design rules checking](https://en.wikipedia.org/wiki/Design_rule_checking) ensures that the layout of the PCB satisfies a series of recommended parameters. In other words, it ensures that a PCB layout designed meets the manufacturer’s limitations, or the manufacturing process’ constrains.

In this particular case, I was going to use a Roland SRM-20 mini CNC machines and a 1/64 inch end mill, so I needed to be sure that the separation between adjacent traces and component pads in my “hello.fdti.44 with LED and button” board was going to be big enough as for the end mill to complete the tool path strategies without trouble.

In order to complete the design rule check (or DRC) there were two options. I could download the DRU file available in the fab Academy archives that included the right settings for layers, distances, clearance, etc. Or I could edit the values directly in the corresponding tabs of the DRC dialog window.

I decided to go for the second method, so I opened the DRC dialog window by clicking the “DRC” button at the bottom of the left-hand tools bar, and I modified all the values in the “Clearance” tab.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523291089579_Captura+de+pantalla+2018-04-09+a+las+11.01.11.png)


Once all the values in the “Clearance” tab were changed to 15.625mil (the equivalent to the 0.396875 mm of the 1/64” milling bit’s diameter), I pressed the “Check” button in the bottom right-hand corner of the DRC dialog window.

As soon as I clicked the “Check” button,  the DRC dialog window closed and a new dialog window named “DRC Errors” opened. In this new dialog window, there were 4 overlap errors.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523291286973_Captura+de+pantalla+2018-04-09+a+las+11.01.14.png)


These overlap errors were just routes going to the pads and they were not going to be relevant at the time of manufacturing the circuit board using the SRM-20 mini CNC machine. 

Therefore, I selected the errors and approved all of them by clicking the “Approve” button at the bottom right-hand corner of the “DRC Errors” dialog window. 

  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523291331759_Captura+de+pantalla+2018-04-09+a+las+11.01.31.png)


Finally, I just had to close the “DRC Errors” dialog window by clicking the red “x” button at the upper left-side corner of the  dialog window, and the board design was ready to be exported.


**Exporting the board design**

Following the instructions in the [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html), the first thing I did in order to export the board design that I had just completed, was opening the “Display” dialog window by clicking in “View” at the main menu bar, and selecting “Layer settings…” in the drop-down menu that appeared.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523293297824_Captura+de+pantalla+2018-04-09+a+las+19.00.43.png)


Once the “Display” dialog window was open, I deselected all the layers in blue, leaving only three layers selected: Top, Pads and Vias. Then, I clicked the “Apply” button at the bottom left-hand corner of the “Display” dialog window. 

As soon as I clicked the “Apply” button, all the layers that I had deselected in the “Display” dialog window disappeared from the board editor, leaving only visible the board’s traces and pads.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523297940761_Captura+de+pantalla+2018-04-09+a+las+19.16.43.png)


Next, I clicked the “OK” button at the bottom right-hand corner of the “Display” dialog window, and the “Display” dialog window closed.

The board design was completely ready to be exported as an image! 

In order to do that, I clicked “File” at the main menu bar, and I selected “Export > Image” in the drop-down menus that popped-up. This opened the “Export file” dialog window.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523299301053_Captura+de+pantalla+2018-04-09+a+las+19.29.11.png)


In the “Export file” dialog window, I started by clicking the “Browse” button in order to select the folder were I was going to save the exported file of my “hello.fdti.44 with LED and button” board.

Then, I selected the “Monochrome” option, I changed the “Resolution” value to 500 dpi,  and I double-checked that “Full” was selected under “Area”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523297960383_Captura+de+pantalla+2018-04-09+a+las+19.29.12.png)


Finally, I clicked the “OK” button at the bottom right-hand corner of the “Export file” dialog window. The dialog window closed, and the PNG file was created in the corresponding directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526312024396_Captura+de+pantalla+2018-04-09+a+las+21.02.46.png)


**Editing the PNG file**

Once I the PNG file with the traces was exported, I needed to prepare it for Fab Modules.

This involved some raster graphics editing, so I launched GIMP, and I opened the PNG file with the traces that I had just created. 

Then, I added a 20 px border around the image via Image > [Canvas Size](https://docs.gimp.org/en/gimp-image-resize.html).

And finally, I exported it as PNG file.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526312085150_Captura+de+pantalla+2018-05-14+a+las+17.34.32.png)


I already had the PNG file for the traces, and I could start preparing the RML file for milling the PCB in the SRM-20, but I still needed to create the PNG file for the board’s outline.

In order to do that, I just edited the image of the traces that is was still open. 

I filled the traces in black using the [Fill with BG Color](https://docs.gimp.org/en/gimp-edit-fill-bg.html) tool, I inverted the colors via Colors > [Invert](https://docs.gimp.org/2.6/en/gimp-layer-invert.html), and exported the image as PNG, being careful of changing the file name.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523301030067_Captura+de+pantalla+2018-04-09+a+las+21.02.47.png)



## **Making the hello echo-world board**

Once I had the two PNG files for the “hello.fdti.44 with LED and button” board that I had designed in EAGLE, I just needed to replicate all the steps that I had previously followed for manufacturing the hello.ISP.44 board during the Electronics Production week assignment. 

**Milling the PCB**

I prepared the RML files using Fab Modules, and I milled the board in the Roland SRM-20 mini CNC machine using the 1/64” and 1/32” milling bits, and taking into account all the tips and tricks that I had learned. 

After a few minutes, the “hello.fdti.44 with LED and button” board was finished, and ready to have the electronics components solder onto it.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526313678033_IMG-3365.jpg)

~~****~~

**Soldering the parts**

As I did before when I had to assemble my hello.ISP.44 board, I started by identifying and collecting all the electronic components needed for the “hello.fdti.44 with LED and button” board. 

Taking advantage of the work that I had done when I was drawing the board’s schematic in EAGLE, I made a list indicating the name and the quantity that I needed of each component:


- 1x AVR ATTiny 44 Microcontroller IC 8-Bit 20MHz 4KB (2K x 16) FLASH 14-SOIC
- 1x 6 Positions ISP Header Connector 0.100" (2.54mm) Surface Mount
- 1x Positions Header Breakaway Connector 0.100" (2.54mm) Surface Mount, Right Angle
- 1x 20MHz Ceramic Resonator Surface Mount
- 1x 1µF ±10% 50V Ceramic Capacitor X7R 1206 SMD
- 1x 499 Ohm 1/4W 1% Resistor 1206 SM
- 2x 10k Ohm 1/4W 1% Resistor 1206 SMD
- 1x LED SMD 1206
- 1x SMD Switch Tactile SPST-NO 0.05A 24V

Then, I searched for their [Digi-Key](https://en.wikipedia.org/wiki/Digi-Key) Part Number in the [Fab Lab Inventory](https://docs.google.com/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html), and I also double-checked the product pages for each of these parts:


- ATTiny 44 microcontroller → [Digi-key Part Number: ATTINY44A-SSU-ND](https://www.digikey.com/products/en?keywords=ATTINY44A-SSU-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 6 pin FTDI header → [Digi-key Part Number: S1143E-36-ND](https://www.digikey.com/products/en?keywords=S1143E-36-ND)
- 20MHz resonator → [Digi-key Part Number: XC1109CT-ND](https://www.digikey.com/products/en?keywords=XC1109CT-ND%09)
- 1uF capacitor →  [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND)
- 499 Ohm resistor → [Digi-key Part Number: 311-499FRCT-ND](https://www.digikey.com/products/en?keywords=311-499FRCT-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- SMD LED → [Digi-key Part Number: 160-1167-1-ND](https://www.digikey.com/products/en?keywords=160-1167-1-ND)
- SMD switch → [Digi-key Part Number: SW262CT-ND](https://www.digikey.com/products/en?keywords=SW262CT-ND%09)

Note that I had already used some of these parts in the hello.ISP.44, but others were totally new, so I found the information available in their product pages (and data sheets) pretty interesting.

Next, I used the aforementioned parts list for collecting the electronics components from Fab Lab Barcelona’s electronic room, and when I had them all, I went back home in order to assemble the board in the tranquility of my desk.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526313742459_IMG-3366.jpg)


  

## **Testing the Hello Echo-World board**

Once my hello.ftdi.44 board (with LED and Switch) was assembled, it was time to program it using the [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c) and [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make) files available under “host communication” in the [Fab Academy’s Embedded Programing assignment page.](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) 

This time, I put them in a local directory that I had created for programming purposes.

Besides the two aforementioned files, I was also going to need the (fully functional) FabISP that I had assembled and set as programmer during the Electronics Production week, the 6 pin ISP ribbon cable that I had made during that very same week, one [FTDI Serial TTL-232 USB cable](http://www.ftdichip.com/Products/Cables/USBTTLSerial.htm), and one USB-A to mini-USB cable.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526322497961_IMG-3367.jpg)


~~****~~**Connecting the boards together**

First, I connected the FabISP programmer to my laptop using the USB-A to mini-USB cable. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526322510445_IMG-3368.jpg)


Then, I connected the hello.ftdi.44 board to my laptop using the FTDI Serial TTL232 USB cable. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526322520276_IMG-3369.jpg)


And finally, I connected the two boards together using the ISP ribbon cable.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526322533258_IMG-3371.jpg)



**Checking the system profiler**

With all the connections done, I thought that verifying that both devices had been recognized by my computer would a good idea before doing anything else. 

Therefore, I opened my machine’s System Profiler. A “FabISP”  and a “TTL232R” devices could be seen listed in the USB devices three, meaning that I could move on.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523544263469_Captura+de+pantalla+2018-04-12+a+las+16.29.38.png)



**Compiling the program files**

Once the connections were done and the two devices connected, the next step was opening the Terminal program, and navigating to the directory were I had saved both the [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c) and the [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make) files:


    $ cd  Desktop/FabAcademy/electronics-design/hello.ftdi.44/programing/ 

Once in the right directory, I typed:


    $ make -f hello.ftdi.44.echo.c.make 

Which prompted in the console:


    avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:     758 bytes (18.5% Full)
    (.text + .data + .bootloader)
    
    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)

Meaning that the corresponding executable (hello.ftdi.44.echo.out) and hexadecimal (hello.ftdi.44.echo.c.hex) files that I needed to program the board later had been generated.

Note that these two files had been created in the exact same directory where the [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c) and [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make) files were located.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1523545105500_Captura+de+pantalla+2018-04-12+a+las+16.58.02+2.png)


**Setting the fuses**

Once the executable and hexadecimal files had been created, it was time set the fuses.

Therefore, I typed:


    $ make -f hello.ftdi.44.echo.c.make program-usbtiny-fuses

And the console prompted:


    avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:     758 bytes (18.5% Full)
    (.text + .data + .bootloader)
    
    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)
    
    avrdude -p t44 -P usb -c usbtiny -U lfuse:w:0x5E:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0x5E"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.01s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0x5E:
    avrdude: load data lfuse data from input file 0x5E:
    avrdude: input file 0x5E contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:5E)
    
    avrdude done.  Thank you.

Meaning that the fuses had been correctly set.


**Flashing the echo hello.world board**

Finally, once the fuses had been set, it was time to program the board. So, I typed:


    $ make -f hello.ftdi.44.echo.c.make program-usbtiny 

And the console prompted:


    avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:     758 bytes (18.5% Full)
    (.text + .data + .bootloader)
    
    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)
    
    avrdude -p t44 -P usb -c usbtiny -U flash:w:hello.ftdi.44.echo.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.ftdi.44.echo.c.hex"
    avrdude: input file hello.ftdi.44.echo.c.hex auto detected as Intel Hex
    avrdude: writing flash (758 bytes):
    
    Writing | ################################################## | 100% 1.21s
    
    avrdude: 758 bytes of flash written
    avrdude: verifying flash memory against hello.ftdi.44.echo.c.hex:
    avrdude: load data flash data from input file hello.ftdi.44.echo.c.hex:
    avrdude: input file hello.ftdi.44.echo.c.hex auto detected as Intel Hex
    avrdude: input file hello.ftdi.44.echo.c.hex contains 758 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 1.60s
    
    avrdude: verifying ...
    avrdude: 758 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FE)
    
    avrdude done.  Thank you.

Meaning that my “hello.fdti.44 with LED and button” board had been correctly programmed, and that everything should be working fine. 

Anyway, I could not be totally sure without testing it.


**Communicating with the board**

[Term.py](http://fab.cba.mit.edu/about/fab/hello/python/term.py) is a little [Python](https://www.python.org/) program written by Neil that can be launched in order to connect a computer and a board via [serial](https://en.wikipedia.org/wiki/Serial_communication), and communicate with it bidirectionally. 
 ****
I downloaded this “term.py” program from the [Fab Academy’s Embedded Programing assignment page (](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo)under “host communication”), and I saved it in the very same directory that I had used for programming the “hello.fdti.44 with LED and button” board a few of minutes before. 

Note that in order to launch the “term.py” program, I needed to have Python installed in my computer so the next step should have been downloading and installing Python. Anyway, macOS does include a Python version by default. This version is not the latest, but it seemed to be also suitable for launching the “term.py” program.

So, I went back to the terminal window and I typed:


    $ python --version

Which prompted:


    Python 2.7.10

Meaning the I actually had a copy of Python v2.7.10 installed in my computer.

The next thing to do was checking the USB port were the EHW board was connected.

In order to do that, I typed:

    $ ls /dev/cu.*

Wich prompted:

    /dev/tty.Bluetooth-Incoming-Port /dev/tty.usbserial-FTEZU7NB

Meaning that the echo hello-world board was connected to /dev/tty.usbserial-FTEZU7NB.

At that point, I just had to type:


    $ python term.py  /dev/tty.usbserial-FTEZU7NB 115200

As soon as I hit enter, a new dialog window called “term.py” opened:


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526320437748_Captura+de+pantalla+2018-05-14+a+las+19.50.47+2.png)

~~****~~
Then, I started to type “Hello World!”, and the “term.py” dialog window sent back a message with every key stroke, confirming that the echo hello-world board was correctly programmed.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_3CF0321C2BD55D938DB0BE33FC270B9FC02AD849848A5B35C3F98A1F9FF9C034_1526321639150_termpyEHWboard-bis-optimized.gif)




## **Completing the group project**

****use the test equipment in your lab to observe the operation of a microcontroller circuit board


**Using the multimeter**

what is a mutltimeter


**Using the osciloscope**








## **What I have learned**

reading an electronic schematic

designing an electronic board with eagle

using term.py 


## **Issues that I had**

changing EAGLE version while working on a project

 


## **Tools used**


- For redrawing the echo hello-world board
  - MacBook Pro with macOS High Sierra v10.13.3
  - EAGLE v8.0.2
  - GIMP v2.8.22


- For making the echo hello-world board
  - Milling 
    - Roland SRM-20
    - Hex key
    - FR1 PCBs
    - 1/64” SE 2FL milling bit 
    - 1/32” SE 2FL milling bit
    - Double-sided tape
    - Spatula
    - Paper towels
    - Rubbing alcohol
    - Steel wool
  - Assembling
    - Soldering iron with a 0.5mm conical fine-point tip
    - Soldering iron stand with sponge
    - Distilled water
    - 0.35 mm soldering wire
    - Desoldering wick
    - Tweezers
    - Vacuum base PanaVise PCB holder
    - Solder fume extractor
    - Magnifying glass
    
- For Testing the Hello Echo-World board
  - MacBook Pro with macOS High Sierra v10.13.3
  - Terminal v2.8.2 (404)
  - Python v2.7.10
  - term.py program
  - FabISP programmer
  - USB-A to mini USB cable
  - FTDI Serial TTL-232 USB cable
  - 6 wire IDC ribbon cable
  - Multimeter
  - Osciloscope



## **Files to download**

