# Computer-Aided Design

As assignment for this week I had to experiment with the greatest number of software tools possible among those that appear listed on the [week’s assignment page](http://academy.cba.mit.edu/classes/computer_design/index.html).


## **My software testing experience**

I must admit that this was great news for me, as I am always happy to test new software apps in my computer (or my iPhone). In fact, one of the things I enjoy from time to time is searching the internet for application reviews. Most of the times, out of necessity but also for pure fun. 

After reading the reviews, I often install the applications that seemed more appealing to me, and I play around with them until I get an idea of how they work. For example, in my recent return to the world of the self-employed workers I needed a tool to help me managing my time, and I have been testing two time tracking systems that I would really recommend in case you need one someday: [Harvest](https://www.getharvest.com/) and [Toggl](https://toggl.com/). But there are many many more: [Freckle](https://letsfreckle.com/), [Everhour](https://everhour.com/), [Mavenlink](https://www.mavenlink.com/), [Timecamp](https://www.timecamp.com/), [Timedoctor](https://www.timedoctor.com/),… I would even say that there are too many! 

In any case, when it comes to design software things get a bit more complicated. Apart from a few exceptions, mastering a [technical drawing](https://en.wikipedia.org/wiki/Technical_drawing) or [graphic design](https://en.wikipedia.org/wiki/Graphic_design) software requires spending a lot of time in front of the computer solving the challenges as they appear. And even after many hours working with these programs you reach a point where you realize that you almost always use the same tools, and that these are equivalent to a very small percentage of the program’s capabilities. 

As multifaceted [designer](https://en.wikipedia.org/wiki/Designer), I have had the chance to work with many of the software programs that appear in this week’s assignment page. And despite some of them are quite similar, it is entirely true that you end up liking some of them on top of the others.

In my particular case, I have always moved from one software to the other based mainly on the requirements of the project I was working on, but right now one of the most important principles underlying this decision for me is the nature of the software. In other words, if the software is [open-source](https://en.wikipedia.org/wiki/Open-source_software) or not.


## **2D design > raster**

A very clear example of the latter was noticeable in the group of “2D Design > Raster” programs listed on the “Computer-Aided Design” assignment page, as I learned [raster graphics editing](https://en.wikipedia.org/wiki/Raster_graphics_editor) with [Adobe Photoshop](https://www.adobe.com/products/photoshop.html?promoid=PC1PQQ5T&mv=other) (and I have used it for many many years), but lately I mostly use [GIMP](https://www.gimp.org/).

### **What is GIMP?**

**GIMP** (from GNU Image Manipulation Program) is a free and open-source raster graphics editor used for image [retouching](https://en.wikipedia.org/wiki/Photo_manipulation) and [editing](https://en.wikipedia.org/wiki/Image_editing), free-form [drawing](https://en.wikipedia.org/wiki/Drawing), or converting between different [image formats](https://en.wikipedia.org/wiki/Image_file_formats), that is available for [Linux](https://en.wikipedia.org/wiki/Linux), [macOS](https://en.wikipedia.org/wiki/MacOS), and [Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows).

For simple (but common) tasks like [scaling images](https://docs.gimp.org/2.8/en/gimp-tutorial-quickie-scale.html), [adding text to images](https://docs.gimp.org/2.8/en/gimp-image-text-management.html), [applying filters](https://docs.gimp.org/2.8/en/filters.html), or more sophisticated operations as [creating animations](https://www.gimp.org/tutorials/Simple_Animations/), I think GIMP is a really convenient software. 

Once you have [downloaded](https://www.gimp.org/downloads/) and [installed](https://howtogimp.com/help/help-with-gimp/how-to-instal-gimp/) the latest stable version of the program, you can learn how perform any of the aforementioned exercises within seconds. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1517954085175_animation_optimized.gif)


Apart from the two programs listed under “2D Design > Raster” that I just mentioned I was already familiar with (Photoshop and GIMP), there were two other programs that I had never used before and I was looking forward to experiment with: [MyPaint](http://mypaint.org/) and [ImageMagick](http://www.imagemagick.org/script/index.php). 

Note that I had never used [Krita](https://krita.org/) and [GraphicsMagick](http://www.graphicsmagick.org/) either, but Krita was not the kind of program I was interested in at that moment (too illustration-oriented for me), and GraphicsMagick seemed to be used for the same purposes as ImageMagick.

In any case, the first thing I had to do in order to start experimenting with MyPaint and ImageMagick, was obviously downloading and installing them on my computer. 


### **Installing MyPaint and drawing something with it**

MyPaint is a user-friendly free and open-source [raster graphics editor](https://en.wikipedia.org/wiki/Raster_graphics_editor) for [digital painters](https://en.wikipedia.org/wiki/Digital_painting) (and not painters) that is available for Windows, macOS, and Linux. 

In order to install MyPaint on my computer (a Macbook Pro with masOS High Sierra installed), I started by visiting [MyPaint’s website](http://mypaint.org/) and browsing to the [downloads page](http://mypaint.org/downloads/). 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1518379560794_2d-design-raster-02.png)


On MyPaint’s downloads page, I scrolled down and I saw under “Mac OS X” (about half way down the page) that the latest stable and development builds were only available via MacPorts, so I clicked the link there, which redirect me to the [search engine index](https://www.macports.org/ports.php?by=name&substr=MyPaint) inside the [MacPorts website](https://www.macports.org/).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1518381424646_2d-design-raster-03.png)


Once in the MacPorts site, I clicked "Installing MacPorts” on the left side menu (under “Getting Started”), and I followed the step-by-step instructions available on the page that opened.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1518382055165_2d-design-raster-04.png)


A few minutes later, I had installed the latest version of all the components needed ([XCode](https://developer.apple.com/xcode/), [Xcode Command Line Tools](https://idmsa.apple.com/IDMSWebAuth/login?appIdKey=891bd3417a7776362562d2197f89480a8547b108fd934911bcbea0110d07f757&path=%2Fdownload%2Fmore%2F&rv=1) and [XQuartz](https://www.xquartz.org/)), and I could install MacPorts in my laptop by downloading the [installation package (.pkg file) for High Sierra](https://github.com/macports/macports-base/releases/download/v2.4.2/MacPorts-2.4.2-10.13-HighSierra.pkg) available under "macOS Package (.pkg) Installer”. And then, running the system's installer by double-clicking on the .pkg file contained therein. 

This was supposed to be the easiest way to install it on a Mac, and it actually was! I just had to follow the on-screen instructions until the MacPorts installation was completed.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1517957844041_Captura+de+pantalla+2018-02-06+a+las+22.40.37.png)


After the MacPorts installation had finished, it was the time to install My Paint.

Anyway, it was recommended before installing MyPaint to check that the MacPorts base installed was the latest version, so I opened the [Terminal application](https://en.wikipedia.org/wiki/Terminal_(macOS))  and I typed:


    $ sudo port selfupdate

Which basically confirmed that the MacPorts base installed was completely updated:


    --->  Updating MacPorts base sources using rsync
    MacPorts base version 2.4.2 installed,
    MacPorts base version 2.4.2 downloaded.
    --->  Updating the ports tree
    --->  MacPorts base is already the latest version
    
    The ports tree has been updated. To upgrade your installed ports, you should run
      port upgrade outdated

Then, in order to definitely install MyPaint on my computer, I typed:


    $ sudo port install mypaint

Once the whole installation process had finished (after a few seconds of text lines prompting down the console), I was finally able to launch MyPaint via the terminal window by typing:


    $ mypaint

As soon as the screen program opened, I started playing around with different core tools available at the [main menu bar](https://github.com/mypaint/mypaint/wiki/v1.2-Main-Toolbar). And also with the drop-down menus available at the [user interface toolbar](https://github.com/mypaint/mypaint/wiki/v1.2-UI-Toolbar). 

The [v1.2 User Manual](https://github.com/mypaint/mypaint/wiki/v1.2-User-Manual) available at [MyPaint’s documentation page](https://github.com/mypaint/mypaint/wiki/Documentation) was a great help for me when I was trying to get a better idea of what it could be done with the program.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1517958381662_Captura+de+pantalla+2018-02-07+a+las+0.06.03.png)


 
 
### **Installing ImageMagick and converting an image file**
 
[ImageMagick](https://www.imagemagick.org/) is a free and open-source [software suite](https://en.wikipedia.org/wiki/Software_suite) created in 1987 that can be used for displaying, converting, and [editing](https://en.wikipedia.org/wiki/Image_editing) raster image and vector image files. It can read and write over 200 [image file formats](https://en.wikipedia.org/wiki/Image_file_formats) including PNG, JPEG, GIF,  TIFF, PDF, SVG, and many more. 

The simplest way to install ImageMagick on a Mac OS X computer was using MacPorts, or [Homebrew](http://mxcl.github.com/homebrew/). I had Homebrew already installed in my computer, and I had just installed MacPorts in order to install MyPaint, so I was good with any of the two options. 

Do not ask me why, but I finally decided to use Homebrew rather than MacPorts. 

I opened a new Terminal window on my computer, and I typed the command:

    $ brew install imagemagick

As soon as the installation finished, I double-checked that the installation was ok by typing:

    $ imagemagick -version

Which confirmed that ImageMagick was correctly installed:
 

    Copyright: © 1999-2018 ImageMagick Studio LLC
    License: http://www.imagemagick.org/script/license.php
    Features: Cipher DPC HDRI Modules 
    Delegates (built-in): bzlib freetype jng jpeg ltdl lzma png tiff xml zlib

 
After reading [ImageMagick’s command-line page](http://www.imagemagick.org/script/command-line-processing.php), I was aware that the [command line options](http://www.imagemagick.org/script/command-line-options.php) available for ImageMagick were almost infinite. But as I just wanted to test that the program was working fine, I decided to start my experience with this program by converting a .png file that I had screen-captured a few minutes before into a .jpg file.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521320816707_2d-design-raster-07.png)


In order to do so, I went back to the terminal’s program window first. 

Then, I used the command line to navigate to my Desktop directory where the .png file that I wanted to convert was located.

    $ cd Desktop/

 Once in the Desktop directory, I typed the command:
 
    $  magick test.png test.jpg
 
Almost immediately, the conversion process was completed, and the corresponding .jpg file that had been created was visible and accessible in my Desktop directory. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521320831160_2d-design-raster-08.png)


As a test it was very interesting, but I still had to do a lot more research in the ImageMagick’s command-line so I could perform other basic techniques with the graphic content generated during the Fab Academy such [as resizing or scaling images](https://www.imagemagick.org/Usage/resize/), [color modifications](https://www.imagemagick.org/Usage/color_mods/), and [others](https://www.imagemagick.org/Usage/).


## **2D design > vector**

Regarding the software programs listed in the “2D Design > Vector” group, the history was basically the same as with the software programs listed in the “2D Design > Raster” group. 

Over the years I had intensively used [Illustrator](https://www.adobe.com/products/illustrator.html) and [CorelDraw](https://www.coreldraw.com/la/), as these were the two [vector graphics editors](https://en.wikipedia.org/wiki/Vector_graphics_editor) that I learned while I was at the school of design were I studied. Both are really powerful tools for creating and editing vector graphics, but for the last few years I have been much more tempted to work with [Inkscape](https://inkscape.org/en/) when I needed to create or edit vector graphics.

###  **Enjoying the Inkscape extensions**

Inkscape is a free and open-source vector graphics editor that can be used to create or edit vector graphics such as [illustrations](https://en.wikipedia.org/wiki/Illustration), [diagrams](https://en.wikipedia.org/wiki/Diagram), [charts](https://en.wikipedia.org/wiki/Chart), [logos](https://en.wikipedia.org/wiki/Logo) and complex paintings. Although, its primary vector graphics format is [(SVG)](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) [Scalable Vector Graphics](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics), many other formats can be imported and exported.

Apart from the complex things that can be made with Inkscape, there are also many other really interesting operations as [applying filters](http://wiki.inkscape.org/wiki/index.php/Filter_Effects) to images, or [creating technical drawings](http://wiki.inkscape.org/wiki/index.php/BlueprintGeometricAndTechDrawing), that can be performed by this program, and that could be useful along the Fab Academy.  

Besides, Inkscape also does supports [a big list of extensions](http://wiki.inkscape.org/wiki/index.php/Extension_repository) that allows to complete relatively complex operations as well as fairly simple ones too. [Barcodes](https://inkscape.org/en/~doctormo/%E2%98%85barcode-extension-from-inkscape-092), [jigsaw shaped pieces](https://inkscape.org/en/~Neon22/%E2%98%85lasercut-jigsaw), [Apollonian gaskets](https://inkscape.org/en/~macbuse/★apollonian-master), [laser-cut tabbed boxes](https://inkscape.org/en/~Neon22/%E2%98%85lasercut-tabbed-box), [multi grids](https://inkscape.org/en/~cds4/%E2%98%85multi-grids), [knob scales](https://inkscape.org/en/~sincoon/%E2%98%85knob-scale-generator), [zoetropes](https://inkscape.org/en/~Neon22/%E2%98%85zoetrope-maker), [ratchets](https://inkscape.org/en/~kie27/%E2%98%85draw-ratchet), and [many more](https://inkscape.org/en/gallery/=extension/) interesting things with just a few clicks! 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521105617969_2d-design-vector-01.gif)



### **Getting to know LibreOffice’s Draw**

It has been a while since I do not use [Microsoft’s Office](https://en.wikipedia.org/wiki/Microsoft_Office) (unless there is not choice), and that I moved to [LibreOffice](https://www.libreoffice.org/). LibreOffice is a free and open-source [office suite](https://en.wikipedia.org/wiki/Office_suite) that allows you to do practically the very same that can be done with Microsoft’s Office suite. 

In any case, [Draw](https://www.libreoffice.org/discover/draw/) is one of the LibreOffice tools that I have barely used. As I already had LibreOffice installed in my computer, I though that giving it a try would be interesting. 

Creating a new Draw document was as simple as launching LibreOffice and selecting the “Draw” module under “Archive > New” in the main menu bar at the left-side hand of the screen program.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521105629766_2d-design-vector-02.jpg)


Once the drawing area opened, I spent a few minutes playing around with the buttons and options available in the different toolbars and information areas surrounding the workspace. 

I did not have in mind any specific use for this program, so the final outcome turned out to be pretty abstract. In any case, downloading and reading the [Draw Guides](https://documentation.libreoffice.org/en/english-documentation/draw/) available at [LibreOffice’s documentation webpage](https://documentation.libreoffice.org/en/english-documentation/) help me to roughly understand how everything works.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521105641772_2d-design-vector-03.jpg)



###  **Remembering how to use Scribus**

[Scribus](https://www.scribus.net/) is another of the programs that I had to get used to when I decided to stop using [Adobe Creative Suite](https://es.wikipedia.org/wiki/Adobe_Creative_Suite). It is released under the [GNU General Public License](https://en.wikipedia.org/wiki/GNU_General_Public_License) as [free software](https://en.wikipedia.org/wiki/Free_software), and based on the free [Qt toolkit](https://en.wikipedia.org/wiki/Qt_(software)), with native versions available for [Unix](https://en.wikipedia.org/wiki/Unix), Linux, [BSD](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution), macOS, [Haiku](https://en.wikipedia.org/wiki/Haiku_(operating_system)), Windows, [OS/2](https://en.wikipedia.org/wiki/OS/2) and [eComStation](https://en.wikipedia.org/wiki/EComStation) operating systems.

Designed for layout, [typesetting](https://en.wikipedia.org/wiki/Typesetting), and preparation of files for professional-quality image-setting equipment, it can also be used to create animated and [interactive](https://en.wikipedia.org/wiki/Interactive) [PDF](https://en.wikipedia.org/wiki/Portable_Document_Format) presentations. But it is mostly oriented for writing newspapers, [brochures](https://en.wikipedia.org/wiki/Brochure), [newsletters](https://en.wikipedia.org/wiki/Newsletter), [posters](https://en.wikipedia.org/wiki/Poster), and books.

To be honest, I have never had to make much [desktop publishing](https://en.wikipedia.org/wiki/Desktop_publishing) but I have had Scribus installed in my computer for years and, when needed, it has proved to be as efficient as [Adobe InDesign](https://www.adobe.com/es/products/indesign.html). 

Once [downloaded](https://www.scribus.net/downloads/stable-branch/) and [installed](https://wiki.scribus.net/canvas/Category:Installation), it possible to start making publications in seconds! In my particular case, [Scribus wiki’s HOWTO page](https://wiki.scribus.net/canvas/Category:HOWTO) and the hundreds of tutorials available online helped me to remember some of the program’s basic features.  


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521108015805_2d-design-vector-04.jpg)



### **Installing FreeCAD and completing a couple of basic usage tutorials**

In order to round off this overview of the vector graphic software listed in the in the “2D Design > vector” group, I really wanted to play around with [FreeCAD](https://www.freecadweb.org/), as this was one of the few software applications in that list that I had never used before. Besides, it seemed like a really good option to try after the very good comments heard about it during (and after) Neil’s lecture.

FreeCAD is an multi-platform (Windows, macOS and Linux) open-source [parametric](https://en.wikipedia.org/wiki/Parametric_design) [CAD modeler](https://en.wikipedia.org/wiki/Computer-aided_design) made primarily to design real-life objects of any size. 

Installing it on a macOS X computer was pretty easy. I just had to visit [the project’s GitHub page](https://github.com/FreeCAD/FreeCAD/releases), and [download the official release of FreeCAD for Mac OS platforms](https://github.com/FreeCAD/FreeCAD/releases/download/0.16.6712/FreeCAD_0.16-6712.da2d364-OSX-x86_64.dmg) available there.  Once the mountable [disk image](https://en.wikipedia.org/wiki/Disk_image) (.dmg file) was completely downloaded, I double-clicked it.

Then, on the window that prompted, I moved the FreeCAD icon to the applications folder.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521508329571_2d-design-vector-05.png)


With the FreeCAD application already in the applications folder, I right-clicked it and selected “Open” from the drop-down menu in order to launch the program. 

When FreeCAD opened, the screen program showed a “Start Center” (“Centro de Inicio” in Spanish) where I could either access a [“Getting started” guide](https://www.freecadweb.org/wiki/Getting_started), jump to one of the most common workbenches, or see the latest news from the FreeCAD world.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521508346384_2d-design-vector-06.png)


I opted for reading the getting started guide first. And as far as I understood, when working with FreeCAD it is possible to start different workbenches depending on the type of job.

A workbench is a collection of tools suited for a specific task. There is a [PartDesign Workbench](https://www.freecadweb.org/wiki/PartDesign_Workbench) for mechanical models, or more generally any small-scale object; a [Draft Workbench](https://www.freecadweb.org/wiki/Draft_Workbench), or the [Sketcher Workbench](https://www.freecadweb.org/wiki/Sketcher_Workbench) (if constraints are needed) for working in 2D; an [Arch Workbench](https://www.freecadweb.org/wiki/Arch_Workbench) for BIM. There is even a special [Ship Workbench](https://www.freecadweb.org/wiki/Ship_Workbench) if you are working on ships design.

In order to make something in 2D, I started the Draft Workbench, and I navigated through the hole GUI trying to understand how the different tools worked. A few minutes later I had the workspace full of scribbles, but it help me to roughly understand how the program works.

Then, I decided to follow the [Sketcher tutorial available at FreeCAD’s wiki](https://www.freecadweb.org/wiki/Sketcher_tutorial).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006584752_2d-design-vector-07.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006584714_2d-design-vector-08.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006584678_2d-design-vector-09.png)


And after completing the Sketcher tutorial, I created a new FreeCAD file, started a Draft Workbench, and followed the [Draft tutorial available at FreeCAD’s wiki](https://www.freecadweb.org/wiki/Draft_tutorial).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006799943_2d-design-vector-10.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006799851_2d-design-vector-11.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006800000_2d-design-vector-12.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006799877_2d-design-vector-13.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522006799907_2d-design-vector-14.png)


I must say that, despite my experience with other CAD software, using FreeCAD for the first time was quite intriguing. I guess that for newbies like me it is pretty recomendable to deeply read the user documentation in [FreeCAD’s wiki](https://www.freecadweb.org/wiki/Main_Page), or follow any of the [many tutorials available](https://www.freecadweb.org/wiki/Tutorials). 

What was totally clear for me was that if I would like to intensively use FreeCAD in the near future… I would have practice a lot more with it!


## **3D design**

As for the [3D modeling software](https://en.wikipedia.org/wiki/3D_modeling), it was very funny for me to see that I had already used more than the 50% of the software listed under “3D design > programs” at some point in my lifetime.

[SketchUp](https://www.sketchup.com/), [Blender](https://www.blender.org/), [Rhino](https://www.rhino3d.com), [AutoCAD](https://www.autodesk.com/products/autocad/overview), [Inventor](https://www.autodesk.com/products/inventor/overview#), [Solid Works](https://www.3ds.com/products-services/solidworks/), [Maya](https://www.autodesk.com/products/maya/overview#), [3d Max](https://www.autodesk.com/products/3ds-max/overview), [Cinema 4D](https://www.maxon.net/en/products/cinema-4d/overview/) or
[Catia](https://www.3ds.com/products-services/catia/) were programs that I had used both at amateur level as well as at professional level before. I had even used CAD software that did not appear in that list as [VectorWorks](http://www.vectorworks.net/) or [Solid Edge](https://www.plm.automation.siemens.com/en/products/solid-edge/).

In any case, in the last few months I have been very motivated on learning how to use and work with these new kind of browser-based cloud computer-aided design software platforms like [TinkerCAD](https://www.tinkercad.com/), [Fusion360](https://www.autodesk.com/products/fusion-360/overview) or [OnShape](https://www.onshape.com/). They definitely are not as powerful yet, and lack many features that are accessible in other CAD programs, but all these features will eventually be added. I really believe that getting used to them from now on it is worth it! 

Personally, I was already registered as user in all these three applications, and I had occasionally used them before for different home projects.


### **Designing a kitchen cloth hanger with TinkerCAD**

TinkerCAD is an amazing tool for [getting started](https://www.tinkercad.com/learn/) in 3d design. I had used it before every time I needed to design a super-simple 3D object for [3d printing](https://en.wikipedia.org/wiki/3D_printing), and I, quite simply, love it.

It happened that by the time I was doing this assignment, I desperately needed a system to hang the kitchen cloths. After a long time placing the cloths all over the kitchen, I realized that the right place to put them was one of the kitchen’s door handle. Sadly, the cloths were falling off the door handle every time we were jumping around the kitchen.

In order to properly hang the cloths from the door handle I needed a couple of hooks. And the best way to attach the hooks to the door handle seemed to be sliding them along the door handle’s profile. I took measurements of the door handle with both a caliper and a ruler, and I worked on a 3D design using TinkerCAD’s basic forms and a shape generator.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522010403280_3d-design-01.png)


The initial design’s 3D print that I made looked fine and it could be slipped over the door handle but it was too tight to move it easily, so I worked on a second iteration of the design, but updating one of the inner dimensions Then, I 3D printed two units… and they are making a really good service since then. Our life is much more better now!! 😛 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522012281976_3d-design-02.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522012280123_3d-design-03.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522012283868_3d-design-04.png)



### **Designing a korean tea table with Fusion360**

Although I have not been able to practice with it as much as I would have wished, Fusion 360 is one of the 3D design tools I desperately [wanted to learn](https://www.autodesk.com/products/fusion-360/get-started) since I first heard about it.

The most interesting thing I could think of in order to complete an exercise with Fusion 360 at the time of this writing, was designing a foldable Korean tea table. My girlfriend is Korean and loves tea, so a Korean tea table would be an amazing birthday present for her.  

After watching a few of the [instructional videos available online](https://www.youtube.com/user/AutodeskFusion360/featured), I felt confident enough as to start designing the table with Fusion360:


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522014740715_3d-design-05.png)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522014740820_3d-design-06.png)


Not looking bad at all, but I guess it will be necessary to spend more time deciding how to join the different parts together and modifying the design accordingly, until I can start manufacturing one. 

I have already purchased online the plastic locking parts for the foldable legs, and it seems that they could perfectly be 3D printed at home. This could be an interesting challenge too!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522016680916_3d-design-07.png)



### **Designing a modular bike chain with OnShape**

So many interesting CAD tools to try and so little time! OnShape is another of the 3D design tools that caught my attention instantly, and that I am [looking forward to learn over the next months](https://learn.onshape.com/). 

I had already done some stuff with OnShape, but still wanted to make more complicated designs in order to keep on learning how to properly use the program so… why not designing a 3D printed chain for the mechanical design week? I though this could be really cool!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522018897436_3d-design-08.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522018897452_3d-design-09.png)


Note that going through [OnShape’s video library](https://www.onshape.com/videos) was very revealing in order to learn how this software works, with especial mention to the [Onshape Fundamentals learning pathways](https://learn.onshape.com/collections/onshape-fundamentals-cad).

After a while designing the individual parts and making adjustments so these parts would perfectly fit, I end up having a pretty solid 3D printed modular bike chain.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522020131478_3d-design-10.png.jpg)



## **Game engines**

To be completed…


## **Simulation**

To be completed…


## **Audio & Video**

Along with the “Game engines” and “Simulation” software groups (that included software that I have never used before), the "Audio-Video" software group included a decent number of interesting programs of which I knew absolutely nothing until the lecture.

I have used [Audacity](https://www.audacityteam.org/) before (many many times). Also Adobe’s [Premiere](https://www.adobe.com/products/premiere.html) and [After Effects](https://www.adobe.com/products/aftereffects.html). I would say that [VideoLAN’s VLC](https://www.videolan.org/vlc/index.html) is my "bedside media player", but I guess that when it comes to audio and (mostly) video editing, the programs that I was used to were much more “sophisticated” (Avid Pro tools, Logic Audio, Final Cut Pro, Avid Media Composer, etc).


### **Updating Audacity and recording/editing with it**

Audacity is an open-source, easy-to-use, [multi-track](https://en.wikipedia.org/wiki/Multitrack_recording) and multi-platform [audio editor](https://en.wikipedia.org/wiki/Audio_editing_software) and recorder that had been available since 2000, which makes almost impossible for any audio enthusiast like me not to have messed around with this amazing piece of software at some point.

From supporting basic audio recordings, to enhanced sound quality and superior editing features, Audacity has been one of the many software tools that I had been using for the last few years, every time I needed to record live audio and computer playback at home. In fact, I was very happy to see it listed under “Audio and Video” on this week’s assignment page.

In any case, it had been a long time since I had used it for the last time so the first thing I did was downloading the latest version available at [Audacity’s website download page.](https://www.audacityteam.org/download/) 

The installation process for mac computers was still really easy to perform, and I just had to double-click the mountable [disk image](https://en.wikipedia.org/wiki/Disk_image) (.dmg file) once the download finished, and drag-and-drop the “Audacity” icon to the applications folder in the window that prompted.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1519684536212_Captura+de+pantalla+2018-02-26+a+las+23.28.31.png)


As soon as I moved the “Audacity” icon to the applications folder, the system asked me if I wanted to overwrite the previous version of Audacity that I had installed. I clicked yes!

Once the previous version of Audacity had been overwritten, I launched the application by double-clicking the new Audacity icon available in the applications folder.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1523126072956_audio-video-02.png)


I thought that a nice example of how to use Audacity would be to record live audio, and then play a little bit with the recorded sounds by using any of the many available effects. 

In order to record live audio, I just made myself sure that the right audio input was selected in the “microphone” drop-down menu under the playback control toolbar. Then, I clicked the “Record” button at the rightmost of the playback control toolbar, and I spoke into the computer’s microphone repeating five times the word “Probando!”.

As soon as I clicked the “Record” button, a mono track was added to the project window, and it was possible to see how the waveform for the word that I was repeating was being created real-time, meaning that the recording was taking place.

Once I had recorded the word “Probando!” enough times, I clicked the “Stop” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521113673498_audio-video-03.gif)


As the mono track was already created, I could start adding some effects to it. 

First, I had to select the part of the track that I wanted to apply effects to. Then, I just add to click “Effect” on the main menu bar and select one of the effects available in the drop-down menu.

For this example I used only the “Delay” effect, which basically made the last word of the recorded track to have a slight delay. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521115556068_audio-video-04.gif)


As mentioned before, the possibilities with Audacity for audio editing are endless. I could spend hours and hours adding new tracks, editing them, adding effects to them,… but there was much more stuff to try, so I moved on to the next software programs that I was looking forward to try.


### **Updating OpenShot and getting started** 

[OpenShot Video Editor](https://www.openshot.org/) is an user-friendly open-source multi-platform [video editing software](https://en.wikipedia.org/wiki/Video_editing_software) that can be used to easily create videos, films, and animations .

I already had OpenShot installed in my computer but it was an old version and it crashed a couple of times when trying to edit some transitions. I thought that upgrading the software could fix this issue, so I visited the OpenShot Video Editor [official download page](https://www.openshot.org/download/) and I downloaded the latest version available at that time (v2.4.1).

The installation process was also really easy to complete. I just had to double-click in the downloaded mountable [disk image](https://en.wikipedia.org/wiki/Disk_image) (.dmg file), and drag-and-drop the “OpenShot Video Editor” icon to the applications folder in the window that prompted. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522153215816_audio-video-05.png)


Despite I have had OpenShot Video Editor installed in my computer for a while, I had barely opened it, so probably the best way to get started in order to make a simple video editing exercise with it, was reading the [OpenShot User Guide](https://www.openshot.org/static/files/user-guide/index.html).

After reading the [Quick Tutorial](https://www.openshot.org/static/files/user-guide/quick_tutorial.html), I launched the program.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522153600358_audio-video-06.png)


Then, I [added one short video](https://www.openshot.org/static/files/user-guide/files.html) that I had in my computer to the “Projects files” area, and I also [created a couple of titles](https://www.openshot.org/static/files/user-guide/titles.html) to use them as intro and outro bumpers. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522153242795_audio-video-07.png)


After splitting the imported video clip so it would be only 12 seconds long I moved it from the “Projects files” area to the program’s timeline, along with the two titles that I had prepared before.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522153258665_audio-video-08.png)


Then, I [added a few transitions](https://www.openshot.org/static/files/user-guide/transitions.html) to the clips in the timeline. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522153267727_audio-video-09.png)


And after double-checking the preview…. [the video was ready to be exported](https://www.openshot.org/static/files/user-guide/profiles.html)!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1522153503665_audio-video-10.png)


### **Installing FFmpeg and converting a video file**

[FFmpeg](https://www.ffmpeg.org/) was one of the programs in the "Audio-Video" group that I had never tried. To be honest, I had not even heard of it before, which is pretty weird because, as I found while searching information about it online, at the moment I was writing this… it was already is 17 years old!

A seen in the wikipedia, **FFmpeg** is a free software project published under the [GNU Lesser General Public License](https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License) 2.1+ or GNU General Public License 2+ (depending on which options are enabled), that produces [libraries](https://en.wikipedia.org/wiki/Library_(computing)) and [programs](https://en.wikipedia.org/wiki/Computer_program) for handling [multimedia](https://en.wikipedia.org/wiki/Multimedia) data. It includes [libavcodec](https://en.wikipedia.org/wiki/Libavcodec), an audio/video [codec](https://en.wikipedia.org/wiki/Codec) library used by several other projects, **libavformat** (Lavf), an audio/video [container](https://en.wikipedia.org/wiki/Container_format_(digital)) [mux and demux](https://en.wikipedia.org/wiki/Multiplexing) library, and the **ffmpeg** [command line](https://en.wikipedia.org/wiki/Command_line) program for [transcoding](https://en.wikipedia.org/wiki/Transcoding) multimedia files. 

In order to start using it, the first thing I needed to do was installing it in my computer. After taking a look to the files available in the [FFmpeg downloads page](https://www.ffmpeg.org/download.html), I made a second search online about how to install it and I found out that [the simplest way to install FFmpeg on a Mac OS X computer](https://github.com/fluent-ffmpeg/node-fluent-ffmpeg/wiki/Installing-ffmpeg-on-Mac-OS-X) was also using Homebrew.

As I already had Homebrew installed in my laptop, I opened the Terminal and I typed:


    $ brew install ffmpeg

And as soon as the installation finished, I double-checked that the installation was ok by typing:


    $ ffmpeg -version

Which confirmed that version 3.4.2 of FFmpeg was correctly installed in my computer:
 

    ffmpeg version 3.4.2 Copyright (c) 2000-2018 the FFmpeg developers
    built with Apple LLVM version 9.0.0 (clang-900.0.39.2)
    configuration: --prefix=/usr/local/Cellar/ffmpeg/3.4.2 --enable-shared --enable-pthreads --enable-version3 --enable-hardcoded-tables --enable-avresample --cc=clang --host-cflags= --host-ldflags= --disable-jack --enable-gpl --enable-libmp3lame --enable-libx264 --enable-libxvid --enable-opencl --enable-videotoolbox --disable-lzma
    libavutil      55. 78.100 / 55. 78.100
    libavcodec     57.107.100 / 57.107.100
    libavformat    57. 83.100 / 57. 83.100
    libavdevice    57. 10.100 / 57. 10.100
    libavfilter     6.107.100 /  6.107.100
    libavresample   3.  7.  0 /  3.  7.  0
    libswscale      4.  8.100 /  4.  8.100
    libswresample   2.  9.100 /  2.  9.100
    libpostproc    54.  7.100 / 54.  7.100 

 
At that point, I could start making any of the operations allowed by the program. 

The first operation I wanted to try was converting a video file to a different format. And luckily, I had available for that a couple of .mov files of the previous Audacity example that I had recorded with Quicktime that could serve as test file. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521313426004_audio-video-05.png)


So I went back to the terminal window, and I used the command line there to navigate to the directory where the .mov file that I wanted to convert was located.


    $ cd Desktop/Fab\ Academy\ 2018/week-02_computer-aided design/audio-video/brutos 

 
Once in the right directory, I typed:
  

    $ ffmpeg -i audacity1.mov audacity1.avi

And as soon as I hit enter, the conversion process started:


    ffmpeg version 3.4.2 Copyright (c) 2000-2018 the FFmpeg developers
      built with Apple LLVM version 9.0.0 (clang-900.0.39.2)
      configuration: --prefix=/usr/local/Cellar/ffmpeg/3.4.2 --enable-shared --enable-pthreads --enable-version3 --enable-hardcoded-tables --enable-avresample --cc=clang --host-cflags= --host-ldflags= --disable-jack --enable-gpl --enable-libmp3lame --enable-libx264 --enable-libxvid --enable-opencl --enable-videotoolbox --disable-lzma
      libavutil      55. 78.100 / 55. 78.100
      libavcodec     57.107.100 / 57.107.100
      libavformat    57. 83.100 / 57. 83.100
      libavdevice    57. 10.100 / 57. 10.100
      libavfilter     6.107.100 /  6.107.100
      libavresample   3.  7.  0 /  3.  7.  0
      libswscale      4.  8.100 /  4.  8.100
      libswresample   2.  9.100 /  2.  9.100
      libpostproc    54.  7.100 / 54.  7.100
    Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'audacity1.mov':
      Metadata:
        major_brand     : qt  
        minor_version   : 0
        compatible_brands: qt  
        creation_time   : 2018-03-15T11:13:04.000000Z
        com.apple.quicktime.make: Apple
        com.apple.quicktime.model: MacBookPro9,1
        com.apple.quicktime.software: Mac OS X 10.13.3 (17D102)
        com.apple.quicktime.creationdate: 2018-03-15T12:09:14+0100
      Duration: 00:00:36.80, start: 0.000000, bitrate: 1812 kb/s
        Stream #0:0(und): Video: h264 (Main) (avc1 / 0x31637661), yuv420p(tv, bt709), 1920x1200 [SAR 1:1 DAR 8:5], 1805 kb/s, 59.10 fps, 60 tbr, 6k tbn, 12k tbc (default)
        Metadata:
          creation_time   : 2018-03-15T11:13:04.000000Z
          handler_name    : Core Media Data Handler
          encoder         : H.264
    Stream mapping:
      Stream #0:0 -> #0:0 (h264 (native) -> mpeg4 (native))
    Press [q] to stop, [?] for help
    Output #0, avi, to 'audacity1.avi':
      Metadata:
        major_brand     : qt  
        minor_version   : 0
        compatible_brands: qt  
        com.apple.quicktime.creationdate: 2018-03-15T12:09:14+0100
        com.apple.quicktime.make: Apple
        com.apple.quicktime.model: MacBookPro9,1
        com.apple.quicktime.software: Mac OS X 10.13.3 (17D102)
        ISFT            : Lavf57.83.100
        Stream #0:0(und): Video: mpeg4 (FMP4 / 0x34504D46), yuv420p, 1920x1200 [SAR 1:1 DAR 8:5], q=2-31, 200 kb/s, 60 fps, 60 tbn, 60 tbc (default)
        Metadata:
          creation_time   : 2018-03-15T11:13:04.000000Z
          handler_name    : Core Media Data Handler
          encoder         : Lavc57.107.100 mpeg4
        Side data:
          cpb: bitrate max/min/avg: 0/0/200000 buffer size: 0 vbv_delay: -1
    frame= 2175 fps=203 q=31.0 Lsize=   47865kB time=00:00:36.80 bitrate=10655.1kbits/s speed=3.44x    
    video:47806kB audio:0kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.122525%

After a few seconds, the conversion process had finished and the corresponding .avi file was visible in the corresponding directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521313729781_audio-video-06.png)


### **Using FFmpeg to split a video file into images**

Another operation that I wanted to try with FFmpeg was splitting one .mov file into a group of .png image files, so I could use these images to create a GIF animation with GIMP later.

Splitting a .mov file was as simple as going back to the terminal window and type:


    ffmpeg -i audacity1.mov -vf fps=2 audacity1%04d.png

And few seconds later, 2 .png files had been created per every second of the .mov file, and they where also visible and accessible in the corresponding directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_C59BA792EECADC1075E6CD93E7923A0B51EBF3820284F9F0354A336DC349726F_1521315074350_audio-video-07.png)



## **What I have learned**

At this point in the Fab Academy, I still had not a clue what to do as the final project. This is the reason why I rather made a sort of introductory exercises than any other thing.

In any case, making these introductory exercises helped me remember working with softwares that I had forgotten how to use, practicing with some programs that I was learning at that moment, and introducing new ones that I had never used before. So, I can not complain!


## **Issues that I had**

The only bad thing for me about this assignment was that I had not enough time to try all the programs that I would have loved to try. For example, I could not try any of the game engines or simulation program of the list, and I had to leave this testing for the future.


## **Tools used** 

- MacBook Pro with macOS High Sierra v10.13.3
- GIMP v2.8.22
- Terminal v2.8.2
- MacPorts v2.4.2
- XQuartz v2.7.11
- Xcode v9.2
- Xcode Command Line Tools v9.2.0.0.1.1510905681
- MyPaint v1.2.1
- ImageMagick v7.0.7-26
- Homebrew v1.5.12
- Inkscape v0.91
- LibreOffice v6.0.2
- Scribus v1.4.6
- FreeCAD v0.16
- TinkerCAD v3.11
- Fusion360 v2.0.3803
- OnShape v1.79
- Audacity v2.2.2
- OpenShot Video Editor v2.4.1
- FFmpeg v3.4.2



## **Files to download**

[Download](https://gitlab.com/aitoraloa/fab-academy/-/archive/master/fab-academy-master.zip)
 






