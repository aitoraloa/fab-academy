# Molding and Casting

For this week’s individual assignment I had to design a 3D mold having into account the stock material and tools available at Fab Lab Barcelona. Ince the mold was designed, I had to CNC machine it, and use it to cast parts in the most suitable material.

On the other hand, as group assignment we had to review the safety data sheets for each of the molding and casting materials used, then make and compare test casts with them.


## **Deciding what to make**

This time it was quite easy for me to decide what to make as molding and casting exercise, and the selected object was an icon of my hometown, the "Baldosa de Bilbao" pavement tile.

The "Baldosa de Bilbao" pavement tile was invented in the first half of the 20th century as a surface covering for the pavements of Bilbao in replacement of asphalt, which was used until then.


![](https://www.dropbox.com/s/ybnjdm4p17r4xzt/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-00.jpg?dl=1)


It was originally manufactured using concrete and coarse sand, with a covering of iron shavings in a 15 x 15 cm format. Currently manufactured with cement, the standard 30 x 30 cm pavement tile features grooves in the shape of a flower, one for each of the four quadrants with channels draining water off to the sides. This tile’s rosette style design is highly effective in wet climates, making life safer for pedestrians.


## **Designing the mold**

**Making a solid 3D model of the object**

I was completely sure that somebody should have done a 3D design of the tile before me, so I searched online and… bingo! I rapidly found a 3D model in the [SketchUp's massive online library](https://3dwarehouse.sketchup.com/?hl=es). 

Unfortunately, at that time there was only one model and it was oddly designed. 

When I imported the SKP file to Rhinoceros, I could not make a solid figure out of it. I tried both importing it as "Trimmed planes" and "Mesh" but none of the two methods seemed to work fine.


![](https://www.dropbox.com/s/6ipf8wj12gi1zph/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-01.jpg?dl=1)


In order to make the 3D model of the "Baldosa de Bilbao" pavement tile with Rhino, but without having to start from scratch, I imported the SKP file as "Trimmed planes” again. And after deleting the parts that were not useful, I exploded the model.  

Then, I selected the surface with the rosette style design and I used the “Make2D" command to generate a new surface based on the exploded model of the "Baldosa de Bilbao" pavement tile. 

Note that I had to be in the right viewport so the surface was correctly projected.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_32A817ACBC02AB8E5FEF60710902B72863A17472CEACEC753F83C8254EBAC5B9_1527097639313_Captura+de+pantalla+2018-05-23+a+las+19.46.57+2.png)


Immediately after that, I joined the curves of the projected surface with the rosette style design and I extruded them just a bit. And then, I applied a "chamferEdge" command to all the edges on the upper plane of the extruded figures. 

Reached that point, the upper shape of the tile with the rosette style design was ready and the last thing I needed to do was extruding a square box to give the tile the desired thickness. Et voilá! 

The "Baldosa de Bilbao" pavement tile was ready as a solid model.


![](https://www.dropbox.com/s/x18ra6ms2bkb7b3/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-02.jpg?dl=1)


### **Creating a rigid positive mold**

The next step in this adventure was to create a rigid positive mold using the solid model, And in order to do that, I needed to put the 3D model of the tile inside an “open-from-above box”.

Although initially it did not seemed something complicated to do, there were at least 3 ultra-relevant details to consider at this point:


1. **The size of the machinable wax block**

From the very beginning, we all knew that the Roland SRM-20 desktop milling machine was going to be designated CNC machine for milling the wax mold. 

Anyway, at that point it was important to remember that the Roland’s maximum operation volume was 202 x 153 x 60 mm, meaning that the machinable wax block could never be bigger than that.

To that effect, each of us was supposed to have a 150 x 100 x 40 mm wax block available for this assignment. But somehow, I end up sharing a smaller 150 x 150 x 35 wax block.


![](https://www.dropbox.com/s/t094d1p2tfm4tmw/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-03.jpg?dl=1)


As I also wanted to make a small two-parts mold later, I needed more machinable wax than the amount entitled to me. And for that reason, I decided to purchase a second 90 x 90 x 30 mm block of green machinable wax that I could use for this one-side mold exercise.


![](https://www.dropbox.com/s/57mj37a086pcqnw/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-04.jpg?dl=1)


Unlike the blue wax (which is quite soft, easy to carve and has a low melting point), the green wax is the hardest, densest and has the highest melting point of the common waxes available. 

Although my tile design did not have really fine details, I bought green wax because I though it would be very interesting to work with both blue and green waxes so I could see the difference. 

I was planning to use a purple one later too, but I needed to see how the green one worked first.

In sum, the size of the wax block I had handy for this first one-side molding and casting exercise was 90 x 90 x 30 mm, and I these were the maximum outer dimensions that I had to have in mind when designing my rigid positive mold. My mold could be smaller than that but never bigger!


2. **The diameter of the milling bit(s)** 

For this one side mold exercise I had the option to use two milling bits, a 1/8" (3.175 mm) flat-end bit for the roughing stage and a 1/32 (0.793 mm) flat-end end bit for the finishing stage. 

Alternatively, I could also use just one milling bit for both roughing and finishing stages, which in this case would be the 1/8 "(3.175 mm) flat-end bit.  

I decided to go for the first option and use both the 1/8" (3.175 mm) and the 1/32 (0.793 mm) flat-end end bits. That meant that the rigid positive mold had be designed having into account that these two bits were going to be used for completing the milling process.


![](https://www.dropbox.com/s/hobgjy06his3sv3/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-05.JPG?dl=1)

****
In view of the above, I had to consider the diameter of the bigger milling bit when placing the "Tile of Bilbao" in the bottom of the mold, as I needed enough clearance for the 1/8" (3.175 mm) milling bit  to reach the botton around the whole tile’s lower perimeter. 

The distance between the lower perimeter of the rigid mold’s inside walls and the lower perimeter of the tile should be at least 1/8" (3.175 mm), if not bigger. 

I finally left a gap of 4 mm between the mold’s inside walls bottom and the tile’s lower perimeter.


![](https://www.dropbox.com/s/go9vgzx979u6g7y/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-06.jpg?dl=1)


The final size of my  "Baldosa de Bilbao" pavement tile scale version was going to be 70 x 70 mm. with an approximated depth of 12 mm. Big enough for my purposes! 


3. **Sloped inside walls** 

Sloped inside walls are very helpful when de-molding the parts (although as I was using silicon which made it not so critical). But the sloped walls are also a determining factor when you are using bits that are not straight throughout their whole length. 

Since the 1/8 "(3.175 mm) flat bit is completely straight, using this milling bit was not problematic, but as I was planning to use also the 1/32 (0.793 mm) flat/square bit for the finishing stage, how and where using this milling bit was something important to consider when designing the mold. 


![](https://www.dropbox.com/s/fpp7dvvr4iy1sja/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-07.jpg?dl=1)


The 1/32 (0.793 mm) flat bit has a smaller diameter in the cutting end than in the end that is fitted in the machine’s collet (shank diameter). This means that if you are milling straight vertical surfaces with the 1/32 (0.793 mm) bit, the height of these surfaces should be always lower than the cutting length of the milling bit. If not, there is a danger of collision between the milling bit’s upper part (with a bigger diameter) and the walls/surfaces of the mold.


![](https://www.dropbox.com/s/wh9ctd2uznsooke/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-08.jpg?dl=1)


In my case, I decided that I was going to use the 1/32” (0.793 mm) flat bit only for finishing the grooves in the upper part of the tile. The depth of the grooves was just 1.17 mm and all the grooves had a smooth chamfer of approximately 45 degrees, so using the 1/32” (0.793 mm) flat bit on that area (unlike the sides of the tile) should not be a problem. 

And that was it! With all these 3 important points in mind, I was able to come up with the 3D model of the rigid positive mold of the "Baldosa de Bilbao" pavement tile that I was going to mill later using the Roland SRM-20 desktop milling machine. 


![](https://www.dropbox.com/s/xsarhbsmyw0hf89/modelling-the-baldosa-de-bilbao-pavement-tile-with-rhino-09.jpg?dl=1)


**Exporting the STL file**

At that point, I had the 3D model of the rigid positive mold saved as 3dm. Anyway, if I wanted to open that 3D model with Roland’s MODELA Player 4 software (which was the software that I was going to use to prepare the milling files), I needed a STL file.

Thus, the last step before closing Rhinoceros was to exporting the mold of the "Baldosa de Bilbao" pavement tile as STL file via “File > Export > STL stereolitography”.


## **Preparing the milling files**

Preparing the milling files involved both learning how to use the Modela Player 4 and understanding the CNC milling process for 3D models. 

**Learning Modela Player 4** 

MODELA Player 4 is an easy-to-use CAM software by Roland that processes STL files produced by popular CAD programs, including SolidWorks, Inventor, Rhino and others. 

It allows for uniform 3D scaling, selection of milling direction and automatic generation and display of the tool path. Cutting depths and speeds are controlled from a predefined material library or can be set to user-defined specifications. 

I had ever used this software before, and I even though that we were going to use Fab Modules instead. But on the other hand, using MODELA Player 4 seemed the right tool for this part of the assignment and everybody in the class was using it, so I thought that I was going to leave using Fab Modules for later, and I was going to focus on having the mold ready ASAP.

As a first step, I searched for tutorials on how to use this tool. I found a few interesting websites and videos but the most useful documentation I found was the one available in the “General Machines Tutorials” section of the [Fab Academy’s tutorials webpage](http://fabacademy.org/archives/content/tutorials/index.html).

After reading the whole [Modela Player tutorial](http://fabacademy.org/archives/content/tutorials/General_Machine_Tutorials/Milling/ModelaPlayer4_tutorial_v2.html), I was ready to try it by myself, so I launched MODELA Player 4 in one of the Fab Lab’s computers, and I opened the STL file of the "Baldosa de Bilbao" pavement tile positive mold that I had previously exported.


![](https://www.dropbox.com/s/ub79nm5x3zq61lf/preparing-the-milling-file-with-modela-player-01.jpg?dl=1)


**Setting the material**

Once the the 3D model was opened, I changed the view mode from “Wire Frame” to “Rendering”. 

Then, I double checked  that the material selected in the pull down menu located in the top right corner of the program window was correct. I was going to use machinable wax, and the material selected there was “Machinable wax”. Puuurfect!


![](https://www.dropbox.com/s/azbjcwkbvhi0lg5/preparing-the-milling-file-with-modela-player-02.jpg?dl=1)


**Setting the model parameters**

The next step in the preparation of the milling files was setting the model parameters.

In order to do that, I opened the “Model” window dialog by clicking the button with a compass in the top right corner of the program window (on the right of the pull down menu). 

In the “Model” window dialog that opened there were two tabs: Size and orientation and Origin.

I left all the parameters in the “Size and orientation” tab as they were. I just double checked that the details there were correct. As everything seemed right, I did not modify anything there.


![](https://www.dropbox.com/s/gh9g3dxyfxvytbw/preparing-the-milling-file-with-modela-player-03.jpg?dl=1)


In contrast, in the “Origin” tab there was a very important change to make.

By default the point of origin appeared in the middle of the model. Anyway, I needed this origin point to be in the top left front corner  of the model (as it had to be aligned with the point of origin in the Modela SRM-20’s platform), so I moved the point.


![](https://www.dropbox.com/s/es0gdeoj8cehgt0/preparing-the-milling-file-with-modela-player-04.gif?dl=1)


Then, I clicked the “OK” button in the “Model” dialog window, and the application went back to the workspace where my model of the "Baldosa de Bilbao" pavement tile was still opened. 

Predictably, the point of origin had changed and it was correctly located in the top left front corner.


![](https://www.dropbox.com/s/ue94nnzmc29nn6e/preparing-the-milling-file-with-modela-player-05.jpg?dl=1)


 
Once the origin point was set, I opened the “Modelling Form” dialog by clicking on the “Modeling Form” button at the top right side of the program window (just below “Material”).

In the “Modelling Form” window dialog I found 4 tabs: Margin, Depth, Slope, and Cutting area.

In **the “Margin” tab**, I selected “Manual” and inserted 0 mm in all the dimensions. This setting would not leave any margin at all around my model, minimizing the waste of material.


![](https://www.dropbox.com/s/5p8gayh5z7m52hd/preparing-the-milling-file-with-modela-player-06.jpg?dl=1)


In **the “Depth” tab**, I just double checked that the details there were correct, leaving all the parameters as they were by default. Top depth (D1): 30 mm


![](https://www.dropbox.com/s/ztj9ocuz8y7hwtc/preparing-the-milling-file-with-modela-player-07.jpg?dl=1)


In **the “Slope” tab**, I also left all the parameters as they were by default. 

There was nothing that I could change in this tab, and I did not need a slope around the model either because the mold’s inner slope was already included in my 3D model.


![](https://www.dropbox.com/s/qdsx9tvwogjsh8q/preparing-the-milling-file-with-modela-player-08.jpg?dl=1)


In **the “Cutting area” tab**, I did not change anything either. As the dimensions of the cutting area were 90 x 90 mm, everything was correctly set there too.


![](https://www.dropbox.com/s/qb1lrh72eb8bspf/preparing-the-milling-file-with-modela-player-09.jpg?dl=1)


Once all the “Modeling form” tabs were set, I clicked the “OK” button in the “Modeling Form” dialog window to apply the changes and close it. 

At that point, the model was set and it was time to create the milling strategies.


**Creating the roughing strategy**

The roughing process is used to mill out the rough shape of the model. 

In order to create a new process, I clicked in the “New Process” button on the right side of the program window. This opened the “New Process Creation” dialog window which that allows
to program the milling processes. 

In the “New Process Creation” dialog window, I selected the “Roughing” and clicked “Next”. 


![](https://www.dropbox.com/s/mfdsjtehjy84g3u/preparing-the-milling-file-with-modela-player-10.jpg?dl=1)


After clicking “Next”, I had to go through a series of sub-windows where (as in the in the previous dialog boxes) some parameters had to be changed and some others not: 

In the “Select the cutting surface” sub-window, I just left everything as it was. I had only one cutting surface and it was Top (+Z), meaning that I was going to cut from the top surface.


![](https://www.dropbox.com/s/7iejneacpmas41d/preparing-the-milling-file-with-modela-player-11.jpg?dl=1)


In the “Choose the tool (blade) to use for the cutting” sub-window, I selected 3.175 mm square from the “Tool” dropdown menu, as this was the tool I was going to use for the roughing process.


![](https://www.dropbox.com/s/39557oyrvwy3j2w/preparing-the-milling-file-with-modela-player-12.jpg?dl=1)


In this “Cutting area and depth” sub-window, I just left everything as it was again. 

I wanted to machine the whole machinable wax area so I just left the checkbox “All” selected.


![](https://www.dropbox.com/s/xh0i20l7yaar9ns/preparing-the-milling-file-with-modela-player-13.jpg?dl=1)


In the “Set the type of tool path” sub-window , I selected “Contour Lines: Up Cut”

Note that this point I had many doubts about which type of tool path should I use as there were many options, and in theory all of them were valid.

In any case, a tool path could be selected initially and changed later, based on the results obtained when the software calculates the tool path. 

In my particular case, I selected “Scan lines: X” first, but the tool path calculations showed that the tool was not reaching some parts of the positive mold, so I changed it to “Contour Lines: Up Cut” which seemed to be the perfect choice. 

Note that I also tested other combinations. “Scan lines: X+Y” turned to be an interesting alternative. However, “Scan lines: Y”, “Unidirectional: X” and “Unidirectional: Y” showed the same issues as with “Scan lines: X”.


![](https://www.dropbox.com/s/hcqd5cq2xc6g6o8/preparing-the-milling-file-with-modela-player-14.jpg?dl=1)


In the “Set the cutting parameters” sub-window, I just left all the parameters as they were by default once again. The parameters were set by default based on the material and tool selected.


![](https://www.dropbox.com/s/733ypcuokygofgd/preparing-the-milling-file-with-modela-player-15.jpg?dl=1)


In the “Enter a name” sub-window, I changed the name that appeared by default  to: “baldosa_bilbo_mold_roughing”. 


![](https://www.dropbox.com/s/t85e4nol5hsp4bn/preparing-the-milling-file-with-modela-player-16.jpg?dl=1)


Then, I clicked the “Finish” button at the bottom-right of the “New Process Creation” dialog window, and a new small window with a progress bar appeared. 

When the progress bar reached the 100%, this new small window disappeared, and the 3D model in the screen showed the tool path for the roughing process. 

On the other hand, I could also see the  “baldosa_bilbo_mold_roughing” strategy listed in the process list on the right hand side of the program window. 


![](https://www.dropbox.com/s/fnwzajh9eqjqp8z/preparing-the-milling-file-with-modela-player-17.jpg?dl=1)


The roughing strategy was ready! Next stop the finishing strategy.


**Creating the finishing strategy**

The finishing process is used to create a more smooth finish.

In order to create the finishing process I clicked in the “New Process” button again. But  this time, I selected “Finishing”.


![](https://www.dropbox.com/s/22hlaggt3dzj3q3/preparing-the-milling-file-with-modela-player-18.jpg?dl=1)


Then, I clicked the “Next” button and I had to go through the same series of sub-windows that I have had to go through during the roughing process.

In the “Select the cutting surface” sub-window, I just left everything as it was. Top (+Z).


![](https://www.dropbox.com/s/yvml5t4frwm48wq/preparing-the-milling-file-with-modela-player-19.jpg?dl=1)


In the “Choose the tool (blade) to use for the cutting” sub-window, I selected 0.793 mm square from the “Tool” drop-down menu, as this was the tool I was going to use for the finishing process. 


![](https://www.dropbox.com/s/o9e7mfcucnyhfm6/preparing-the-milling-file-with-modela-player-20.jpg?dl=1)


As I just wanted to apply the finishing process to the grooves in the upper part of the tile, in the “Cutting area and depth”, I selected the checkbox “Partial” and I changed the parameters in “Lower left: X and Y” and “Upper Right: X and Y” fields. All the other parameters were left as they were by default.


![](https://www.dropbox.com/s/457xq5g1210ezjc/preparing-the-milling-file-with-modela-player-21.jpg?dl=1)


In the “Set the type of tool path”, I selected “Countour Lines: Up Cut” again


![](https://www.dropbox.com/s/lugoedy71imvruh/preparing-the-milling-file-with-modela-player-22.jpg?dl=1)


As mentioned before,  the parameters in the “Set the cutting parameters” were set by default based on the material and the tool selected, so I did not change anything there either.


![Set the cutting parameters sub-window with default parameters for the finishing process](https://www.dropbox.com/s/r51t4ehpgnhx0rs/preparing-the-milling-file-with-modela-player-23.jpg?dl=1)


In the “Enter a name” sub-window, I just changed the name that appeared by default  to “baldosa_bilbo_mold_finishing”.


![](https://www.dropbox.com/s/813w6w0tnz0gfsr/preparing-the-milling-file-with-modela-player-24.jpg?dl=1)


Then, I clicked the “Finish” button in the “New Process Creation” window, and the little window with the progress bar appeared. 

As it had happened before, when the progress bar reached the 100% (it took longer then with the roughing process I have to say), the window with the progress bar just disappeared and the 3D model in the screen showed the tool path for the finishing process. 

As you can see it the image below, the finishing process tool path only appeared over the grooves in the upper part of the tile. 

On the other hand, the  “baldosa_bilbo_mold_finishing” strategy appeared also listed in the process list, just under the previously generated roughing strategy.


![Finishing process tool path](https://www.dropbox.com/s/ye80z478ya1vkb4/preparing-the-milling-file-with-modela-player-25.jpg?dl=1)



**Generating the G-code**

With both the roughing and finishing strategies created, I clicked the “Cut” button located on the lower-right corner of the program window. 

As soon as I clicked “Cut”, the “Cutting-position Setup” dialog window opened. 


![](https://www.dropbox.com/s/bk1v4z89zhbkkyd/preparing-the-milling-file-with-modela-player-26.jpg?dl=1)


In the “Cutting-position Setup” dialog window everything was correctly configured, so I just clicked the “OK” button, and a new small window appeared where some details about the machine where shown: Name, Status, Type, etc. 

As everything seemed to make sense, so I just clicked the “OK” button.


![](https://www.dropbox.com/s/uaq795xfsal441i/preparing-the-milling-file-with-modela-player-27.jpg?dl=1)


When I clicked “OK”,  a new dialog box appeared with the “Output to file” title.

In the “Output to file” window, I selected the name for the roughing process PRN file first (which was the same as the one I entered during the roughing process creation), I selected the location where I wanted to save it, and I clicked the “OK” button.


![](https://www.dropbox.com/s/4zxzm1stmbdhtf3/preparing-the-milling-file-with-modela-player-28.jpg?dl=1)


Then, I had to repeat the very same procedure for the PRN file of the finishing process. 


![](https://www.dropbox.com/s/ht21ds3wp3mjq82/preparing-the-milling-file-with-modela-player-29.jpg?dl=1)


At that point, I had generated the two PRN files that I was going to use for milling the mold with the Roland SRM-20, and I was completely ready to go ahead in this assignment.

But before moving ahead, I saved the MODELA Player 4 project as Modela Player project  file (MPJ) for future reference/use. This was as simple as clicking on “File > Save as…” in the applications’ main menu bar, and then click on the “Save” button (after giving it a proper name). 

Note that saving the MPJ file took much more time than I had expected.

Anyway, with all the files I needed from the MODELA Player 4 software generated and safely saved,  I took advantage of another interesting Roland application to simulate the milling job.


## **Simulating the milling job**

Roland’s software package used to simulate milling operations is Virtual MODELA.
****
Virtual MODELA provides a quick preview of the entire milling operations. Using it helps eliminating milling errors, enables simulation of finished shapes and estimates production time. On the other hand, lighting effects, material color or bitmap overlays can be also added for accurate finished product representation.

In order to launch the Virtual MODELA software I selected the roughing process (which was the process I wanted to simulate first) in MODELA Player’s processes window.

Then, I clicked on the “Preview Cutting” button located to the left of the “Cut” button. 


![MODELA Player’s program window showing the location of the “Preview Cutting” button](https://www.dropbox.com/s/7hw8if3bfkwzg9x/simulating-the-milling-with-virtual-modela-01.jpg?dl=1)


Clicking “Cut”, started the Virtual MODELA application, and showed a flat (two-dimensional) view of the mold with the roughing process simulation being performed.


![Flat (two-dimensional) view of the mold with the roughing process simulation being performed.](https://www.dropbox.com/s/9ablfncg3rel1ru/simulating-the-milling-with-virtual-modela-02.jpg?dl=1)


By clicking the “3D Simulation” button in the Toolbar, I changed the view from flat (two-dimensional) to solid (three-dimensional) view.


![Virtual MODELA's solid (three-dimensional) view of the mold after the](https://www.dropbox.com/s/mnag1lnodnnx7mx/simulating-the-milling-with-virtual-modela-03.jpg?dl=1)


I also clicked the “Estimated Cutting Time” button to open the “Cutting Data Information” window where a prediction of how much time cutting will take was displayed (0:23 min). 

Note that this is a general estimation but, although I did not timed it later when I was milling the mold, I would say that it was pretty close to reality.


![](https://www.dropbox.com/s/vspyuehz2gm6oxo/simulating-the-milling-with-virtual-modela-04.jpg?dl=1)


At that moment, I just wanted to see the simulation of the imported tool path again, so I closed the “Cutting Data Information” window by clicking the “OK” button. 

Then,  I clicked the “Redo Cutting” button of the toolbar, and the cutting simulation just performed again in the solid (three-dimensional) view. 

~~****~~
![](https://d2mxuefqeaa7sj.cloudfront.net/s_32A817ACBC02AB8E5FEF60710902B72863A17472CEACEC753F83C8254EBAC5B9_1527784245884_virtualmodelaB_optimized.gif)


As I wanted to simulate the finishing process too, I went back to the MODELA Player program window, I selected the finishing process in the processes window, and clicked on the “Preview Cutting” button.

Next, I went back to the Virtual MODELA’s program window where a dialog window asked me if I wanted to save the previous simulation. I clicked “NO”, and the simulation for the finishing process was performed in the same solid (three-dimensional) view.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_32A817ACBC02AB8E5FEF60710902B72863A17472CEACEC753F83C8254EBAC5B9_1527784613194_virtualmodelaAbis_optimized.gif)


The latest thing I did before closing the Virtual MODELA was saving the cutting configuration. 

This was as simple as clicking on “File” in the main menu bar, and select the “Save Cutting Configuration…” option.

Note that playing around with Virtual MODELA a little bit more I realized that I could also simulate only the finishing process without sending the roughing process first. In any case, as the tool path for my finishing process was partial it did not make too much sense to me. 

I guess that it would make more sense if the tool path for both the roughing and finishing is the same. Nevermind! Using Virtual MODELA had been really interesting in order to understand how the whole milling process was going to be performed, and I was completely ready to mill the mold.


## **Milling the mold** 

At this point, I was feeling very confident with the idea of using the Roland SRM-20 machine for milling the mold as I had been using it repeatedly for the electronics related assignments.

In any case, the milling process of a 3D model was a bit different from all the 2 1/2 milling processes that I had performed before. 


**Cleaning the machine**

As I I wanted to recycle the wax shavings (something very interesting when you want to prolong your wax block lifespan), the first thing I did was cleaning the SRM-20 machine thoroughly.

In order to leave the machine flawless, I used a vacuum cleaner first and then compressed air.

Note that compressed air cleaning procedure was done outside (Fab’s backyard).

~~****~~
![](https://d2mxuefqeaa7sj.cloudfront.net/s_32A817ACBC02AB8E5FEF60710902B72863A17472CEACEC753F83C8254EBAC5B9_1527765939172_IMG-3597.jpg)


**Changing the bed**

Note that once the machine was completely free of residues from previous milling activities, I did not put the bed used for milling PCBs back in the machine, and I replaced it with a acrylic bed.

In general, the bed used for milling PCBs consists of a folded metal-sheet bed and a thick MDF piece on top (or at least that is the way the bed is set up in Fab Lab Barcelona’s SRM-20s). 

The point here was that placing the 30-40 mm thick wax block over MDF piece, which is already 20-25 mm thick, could become a space problem in the Z axis.

Therefore, changing the original metal bed + MDF set-up to the acrylic base set-up (which is just 5 mm thick) was a must in order to avoid any problem due to a lack of space in the Z axis. 


![](https://www.dropbox.com/s/jwyv0dknv21621z/milling-the-mold-with-roland-srm-20-01.jpg?dl=1)


**Attaching the material**

The next step was attaching the wax block to the bed, and in order to do that properly I used double sided cloth tape, which was much more stickier than the one used with the PCBs. 

Note that using an adequate double sided tape was very important because the wax block (which had much more volume than any of the PCBs) needed to be really well fixed to the bed. 


![](https://www.dropbox.com/s/qgep40oonn8eb1p/milling-the-mold-with-roland-srm-20-02.jpg?dl=1)


Next, I placed the wax block on the acrylic bed making sure that its nearest side to me was completely parallel to the front side of the acrylic bed. 

Aligning the wax block correctly was important as the walls of my mold were only 3 mm thick and I did not want to screw up the mold by stepping outside the wax block perimeter.


![](https://www.dropbox.com/s/ahy1kscw92bywwa/milling-the-mold-with-roland-srm-20-03.jpg?dl=1)


**Inserting the milling bit**

With the wax block correctly placed and well sticked onto the machine’s bed, it was the time to add the milling bit to the equation. 

As the first stage of this operation was the roughing process, I picked up the 3.175 mm (1/8") flat bit and, after checking that it was in good condition, I inserted it in the SRM-20’s spindle collet. 

Once the milling bit was correctly fixed in the spindle, I needed to set the origin point. 


**Setting the origin point**

The origin setting procedure was identical to the one used when milling PCBs.  

I started by using Roland VPanel’s control buttons to align the tip of the milling bit with the top-left front corner of the wax block in the X and Y axis. 

Next, I moved the Z slowly (also using Roland VPanel’s control buttons) until the tip of the milling bit was 3-4 mm above the wax block’s top-left front corner. 

Once the tip of the milling bit was at the right distance, I undid the collet screw so the milling bit landed on the wax block’s surface, and I tightened the collet screw again. 


![](https://www.dropbox.com/s/mjlstpnlyobufyt/milling-the-mold-with-roland-srm-20-04.jpg?dl=1)


Then, I just had to click the “X/Y” and “Z” buttons of the VPanel’s “Set Origin Point” section, and the relative zero point was reseted.

The material and the machine were ready, and it was time to launch the milling jobs!


![](https://www.dropbox.com/s/xxs0wgy2rno57qi/milling-the-mold-with-roland-srm-20-05.jpg?dl=1)


**Sending the roughing job**

In order to send the roughing job, I just clicked the “Cut” button. 

In the new window the appeared, I selected the PRN file for the roughing process that I had saved before, and I clicked the “Output” button, so the machine started to run.


![](https://www.dropbox.com/s/f5he606350vfv5c/milling-the-mold-with-roland-srm-20-06.jpg?dl=1)


Around 30 minutes later the roughing process was completely finished and the entire interior of the machine was covered with a considerable amount of wax shavings.


![](https://www.dropbox.com/s/hequ63rtscqmh7o/milling-the-mold-with-roland-srm-20-07.jpg?dl=1)


I cleaned the wax shavings as good as possible with a brush and checked that the roughing process had been completed correctly. 

Everything was ok, so I could move on to the finishing process.


![](https://www.dropbox.com/s/ccdr2pjf40zmr2s/milling-the-mold-with-roland-srm-20-08.jpg?dl=1)


**Sending the finishing job**

The first thing I did for the finishing job was changing the milling bit. 

I removed the 3.175 mm (1/8 ") bit from the spindle, and I replaced it with the 0.793 mm (1/32") bit.

Then, I sent the machine back to the origin point using the “Move > To Origin” buttons. 

Note that I inserted almost the whole length of the bit in the spindle to avoid the risk of collision, so when I sent the machine back to the origin point, the tip of the bit was 10-15 mm above the wax block’s top-left front corner. 

As with the previous bit, I undid the collet screw so the milling bit landed on the wax block’s surface, and I tightened the collet screw again. 


![](https://www.dropbox.com/s/4bc2mmtxyin0h0w/milling-the-mold-with-roland-srm-20-09.jpg?dl=1)


Then, I clicked the “Cut” button in VPanel and a new window browser appeared.

I selected the PRN file for the finishing process, and I clicked the “Output” button.

The machine started to run again.


![](https://www.dropbox.com/s/s1ro998g6fts8yj/milling-the-mold-with-roland-srm-20-10.jpg?dl=1)


After another ~30 minutes of impatient waiting, the finishing process was completed. 

The shavings generated this time were not as many as with the roughing process so it was pretty easy and fast to clean everything up inside the Roland SRM-20. 


![](https://www.dropbox.com/s/j7exg8smq18rnda/milling-the-mold-with-roland-srm-20-11.jpg?dl=1)


And there it was! The positive mold for my scaled down version of the “Baldosa de Bilbao” pavement tile, correctly milled in wax and ready for the next step. 


![](https://www.dropbox.com/s/xwv5tcubmncncts/milling-the-mold-with-roland-srm-20-12.jpg?dl=1)


Note that to conclude this stage of the assignment, I picked up all the wax savings from the dust-collection tray and I placed them inside a zip-bag so they could be stored, and hopefully recycled.


![](https://www.dropbox.com/s/7wnhndymka98rqm/milling-the-mold-with-roland-srm-20-13.jpg?dl=1)


 

## **Creating the negative mold**

CNC milling the positive mold was only half the assigment.

Next step was creating the negative mold that I would use to cast the pavement tile later, and for completing that stage I needed to make decisions that would condition the rest of the exercise.


**Choosing the casting material**

As the aim was casting a pavement tile, it totally made sense to me using concrete.

Note that this was a very important decision to make because the material to be used for making the negative mold was totally dependent on the material that is going to be casted on it later (i.e. when making edible foods out from a silicone mold, the silicon used to do so needs to be certified safe for food contact).   

In my particular case, I had never worked with concrete before, but I was eager to try. 

Besides, Fab Lab Barcelona’s workshop is full of bags of different types of concrete sands with leftovers that former (or current) IaaC/Fab Lab students’ had used for their projects.


![](https://www.dropbox.com/s/0jc0tsj8yx5380h/creating-the-negative-mold-in-urethane-rubber-01.jpg?dl=1)


**Choosing the molding material**

Asking around to people in the lab who had done projects with concrete before, I came to the conclusion that I could use regular silicone for casting concrete but that it would be even better using one material that had been specially formulated for it. 

[Smooth-On](https://www.smooth-on.com/) has a complete series of [two-component urethane rubbers called VytaFlex](https://www.smooth-on.com/product-line/vytaflex/) that offer superior physical and performance properties for casting concrete.  ****


![](https://www.dropbox.com/s/u7b7yeeg7df3h3h/creating-the-negative-mold-in-urethane-rubber-02.jpg?dl=1)


Luckily for me, there were also several containers of VytaFlex 40 in the Lab. 

These containers were also leftovers from different projects done in the Fab Lab before and they were almost empty. But as they were medium-big size containers and I did need just a small volume of material, it was enough urethane rubber for me. 

I finally used the leftovers of the medium containers as they were much more easy to handle.


![](https://www.dropbox.com/s/uieajo2fjvyt9x3/creating-the-negative-mold-in-urethane-rubber-03.jpg?dl=1)


**Learning about urethane rubbers**

At this point, I knew I had available all the material I needed for the negative molding and the following casting, so I just needed to learn about both. 

When reading the Vitaflex 40 instructions it got me worried the condition of the material. In the instructions says that “the shelf life of this product is reduced after opening”. All the VytaFlex containers had been opened and I did not know how long ago. 

In order ensure that I could use the Vitaflex 40 leftovers I went to the Form X shop that they have recently opened 150 meters away from the Fab Lab looking for answers, and maybe for buying also a Vitaflex trial size unit, as they had available all the VytaFlex variants.


![](https://www.dropbox.com/s/8a840q96cpbdvf5/creating-the-negative-mold-in-urethane-rubber-04.jpg?dl=1)


The charming woman there told me that the only way to figure out if the material was still in good shape was making a test. She also explained me that I should conscientiously stir part A and part B separately for 2-3 minutes(and not just the part B as it says in the instructions). That then, I had to mix both parts and stir the mixture thoughtfully for another 2-3 minutes. And that finally, I had to leave it curing for at least 16 – 24 hours at room temperature before de-molding. In sum, she basically explained to me the whole regular process but adding the step of stirring the part A separately. 

In any case, I went back to the Fab Lab and performed a test using a 3D printed mold of the “Baldosa de Bilbao” pavement tile that I had printed with a Zortrax machine while learning how to use the MODELA Player 4 software. The very next day I checked the test and it was… great!


![](https://www.dropbox.com/s/f923h62ivsitkbv/creating-the-negative-mold-in-urethane-rubber-05.jpg?dl=1)


This implied that I could preform the whole process again but using the wax mold this time.

So I collected the two buckets of VytaFlex, mold release spray, brush, mixing containers, wooden stick stirrers, scale, security googles and gloves. Basically, all the things I had collected the day before when using the 3D printed mold, but with the purpose of using them with the wax mold.

Note that later I found out that wooden paint stirrers can trap moisture and contaminate urethanes if used for mixing prior to cure. And paper mixing cups/tubs can do the same damage to urethanes. This means that I should have used plastic or metal stirrers and containers! Ups!


**Preparing the mold**

The urethanes are adhesive and, when models are made of porous materials, the manufacturer recommends to use a sealer before to prevent adhesion between the rubber and model surface. 

As I was using machinable wax (which is a non-porous material), it only required a release agent to facilitate de-molding when casting into or over most surfaces. 


![](https://www.dropbox.com/s/bpazcdty0kr9cwl/creating-the-negative-mold-in-urethane-rubber-06.jpg?dl=1)


I shaken the spray bottle and then sprayed the wax mold holding it 25-30 cm form the bottle’s stream, applying a continuous light mist coating to the mold forms. 

Then, I brushed the release agent over entire surface mold with a brush, in order to break the surface tension and minimize air entrapment. 

Finally, I applied another light mist coating of release agent and let stand for 5-10 minutes. 
 

![https://www.smooth-on.com/products/universal-mold-release/](https://www.dropbox.com/s/txr602gdlzdcr3x/creating-the-negative-mold-in-urethane-rubber-07.jpg?dl=1)


**Mixing the urethane rubber**

While the release agent was standing, I conscientiously stirred part A and part B separately for 2-3 minutes. Next, I poured equal (1A:1B) amounts of parts A and B into the mixing container.  

Note that in order to calculate the volume of urethane rubber needed, I had previously filled the mold with water and I had poured the water in the mold to a plastic cup, measuring and marking the volume that it took. Later, I used this mark in the cup as a reference to pour the right amount of material.


![](https://www.dropbox.com/s/zyg7qz4ecbd86fu/creating-the-negative-mold-in-urethane-rubber-08.jpg?dl=1)


With the equal amount of parts A and B in the plastic cup, I thoroughly mixed the content with the wooden stick for at least 3 minutes, making sure that I was scraping the sides and bottom of the mixing container. 


![](https://www.dropbox.com/s/d74rdbhjhf01tf9/creating-the-negative-mold-in-urethane-rubber-09.jpg?dl=1)


**Pouring the mixture**

Once the recommended mixing time had passed, I poured the mixture in a single spot in the middle point of the container with a uniform flow. 

In that way, the rubber sought its level up and over the model, minimizing entrapped air. 


![](https://www.dropbox.com/s/xinnm826vmf5430/creating-the-negative-mold-in-urethane-rubber-10.jpg?dl=1)


**Removing the air bubbles**

Although VytaFlex is formulated to minimize air bubbles, it is recommended vacuum degassing it prior and/or after pouring the rubber to further reduce entrapped air. 

Thus, once the mold was filled with the mix, I placed the mold in the DIY vacuum de-gasser we had available in the lab, and I left it there for a couple of minutes, checking that the mixture would not overflow due to the effect of the vacuum pump.


![](https://www.dropbox.com/s/ycbjxe1pukt63ob/creating-the-negative-mold-in-urethane-rubber-11.jpg?dl=1)


A few minutes later, I removed the mold from the DIY vacuum de-gasser machine, and I placed it in a location where nobody could touch it. 

Ideally, the rubber had to cure a minimum of 16-24 hours at room temperature before de-molding it, so I left it in that remote location overnight.



![](https://www.dropbox.com/s/tqbir8ak5cmqf9z/creating-the-negative-mold-in-urethane-rubber-12.jpg?dl=1)


**Demolding the urethane rubber  mold** 

On the morrow, I picked up the mold from where I left it and the rubber looked totally cured.

As everything seemed ok, I first separated the urethane rubber from the upper edges, and then I used a putty knife to separate the urethane rubber from the mold’s inner walls. 

The urethane rubber mold came out of the wax mold with hardly any effort, meaning that the universal mold release spray applied to the wax mold before pouring the mix served its purpose.


![](https://www.dropbox.com/s/w7pjnmwrrvtpx91/creating-the-negative-mold-in-urethane-rubber-13.jpg?dl=1)


And that was it! My brand new urethane rubber mold was ready for casting the scale version of the "Baldosa de Bilbao" pavement tile out from it... very promising, right!?


![](https://www.dropbox.com/s/h4gidmngtvbe49t/creating-the-negative-mold-in-urethane-rubber-14.jpg?dl=1)



  
## **Casting the pavement tile**

As pointed out before, I had never worked with concrete so I needed to learn the very basics of it.

**Reading the cement technical sheets** 

In that sense, I started by downloading the technical sheet and MSDS for the concrete that (upon recommendation) I was going to use, which was the Kerakoll Geolite 40 repair mortar. 

From the technical sheet I could extract general information about the composition, performance, etc. But I also learned very essential info as the water/cement ratio or the setting time. 

From the MSDS I understood the potential hazards associated with using this material, and I also confirmed the individual protection equipments that I should wear while working with it.


![](https://www.dropbox.com/s/px8zgn5sqpnppdl/casting-the-tile-from-the-urethane-rubber-mold-01.jpg?dl=1)


**Learning how to work with concrete**

That night while dealing with Kerakoll Geolite 40’s documentation (and I imagine that by one of those chance occurrences that happen sometimes in life), I received an instructables newsletter. 

Curiously, one of the staff picks for that newsletter was a “Concrete Class”. 

I didn't think it twice! I enrolled myself for the class and I did not go to bed until I completed it.


![](https://www.dropbox.com/s/m0ul0eicsxrcqvm/casting-the-tile-from-the-urethane-rubber-mold-02.jpg?dl=1)


The next day I was pretty tired as I stayed until late finishing the “Concrete Class” lessons, but I was also totally convinced of what to do. Taking that class the night before was an excellent idea! 


**Arranging the workspace**

Everything started by buying a trowel, a plastic mixing bucket and a tupperware in one of the local Chinese shops on my way to the Fab Lab. 

Once in the Fab Lab, I set up a proper workspace in the building’s backyard. 

Then, I collected a small zip-bag with cement, water, mineral-oil, brush, different size mixing containers, wooden stick stirrers, scale, security googles, latex gloves and the two urethane rubber molds that I had handy. 


![](https://www.dropbox.com/s/r9lafoafdtl85ou/casting-the-tile-from-the-urethane-rubber-mold-03.jpg?dl=1)


 

**Measuring the cement amount**

Once I had all the required tools, I measured the amount of cement that I needed for this first batch of pavement tiles. Note that the cement used did not contain aggregate, so it was not necessary using a sieve to remove the aggregate before I measured it.

To be honest, I was not sure of the amount of cement I needed to fill the two urethane rubber molds. I though that 200 grams would be enough. If otherwise, I could always make a bit more. 

It turned out that 200 grams was the perfect amount. Pure beginner's luck I would say! 


![](https://www.dropbox.com/s/qqdkob1zcg0ahyz/casting-the-tile-from-the-urethane-rubber-mold-04.jpg?dl=1)


Based on the cement/water ratio information extracted from the Kerakoll Geolite 40’s technical sheet the night before, I calculated that the amount of water needed for 200 grams of Geolite 40 was 36 milliliters. As the relation milliliters/grams is 1:1, I needed 36 grams of water.

I poured water little by little in the container until I reached the amount of water needed. It looked a very small amount of water to me (and I even did the cement/water ratio math again) but if the technical sheet was saying so… it had to be truth!


![](https://www.dropbox.com/s/o15o28t3w5a9gvr/casting-the-tile-from-the-urethane-rubber-mold-05.jpg?dl=1)


**Adding the release agent**

Using a release agent before pouring the concrete mix into the mold seemed a good idea, as this would make removing the concrete from the mold after it had cured very easy. 

Note that there are special concrete release agents available in the market, but it seems that ordinary cooking spray works just as well. 

As I did not have special concrete release agent or cooking spray handy, I searched online for alternatives and I found it that I could also add mineral or vegetable oil to the mold using a brush. 

I took risks, and I used a brush for applying a thin layer of lube oil to the mold.


![](https://www.dropbox.com/s/psynsbueyizr1hb/casting-the-tile-from-the-urethane-rubber-mold-06.jpg?dl=1)


**Mixing the concrete**

At that point, I had the amount of cement needed ready, the amount of water needed ready, and the molds with a thin layer of “my creative mold release agent” ready. It was time to make the mix!!

I started the mixing process by adding the concrete into the plastic bucket

Then, I added the water in one go, and I grabbed the trowel to vigorously mix the content of the bucket until it looked good enough as to be thrown down into the urethane rubber molds.


![](https://www.dropbox.com/s/61dsx3oryrduwrs/casting-the-tile-from-the-urethane-rubber-mold-07.jpg?dl=1)


**Pouring the concrete**

Once the concrete mix was ready, I casted the content of the bucket into the urethane rubber molds using the trowel that I had previously hacked for making it smaller.

Then, I smoothed and leveled the cement in the mold (also with the trowel), I removed any excess concrete, and I cleaned up the edges.


![](https://www.dropbox.com/s/hyub3936gxu4af7/casting-the-tile-from-the-urethane-rubber-mold-08.jpg?dl=1)


**Removing the air bubbles**

Once the urethane rubber molds were filled with concrete, it was time to extract the bubbles.

I first placed the two molds inside the tupperware I had bought before. 

Then, I shaken and taped the tupperware for a while. 

Finally, I placed the container with the two molds inside on top of a running compressor, so the compressor’s vibration would help to release any air bubbles and allow the mix to settle in the voids of the mold.


![](https://www.dropbox.com/s/bg30316tqtpbhbb/casting-the-tile-from-the-urethane-rubber-mold-09.jpg?dl=1)


**Curing the concrete**

After 5-10 minutes with the molds on top of the compressor, I covered the tupperware’s with the lid and retired it from the compressor to place it in a safe location until the next day.

Note that the reason why I covered the molds while curing is because it prevents the water to evaporate too quickly from the concrete. In this way, the hydration process will have enough time to react with the water that was mixed in.


![](https://www.dropbox.com/s/pchozztg3s1e5dl/casting-the-tile-from-the-urethane-rubber-mold-10.jpg?dl=1)


When I went to pick up the tupperware the next day, I could see the condensation formed inside the container. If I had not covered it, this moisture would have escaped and not been included in the hydration process, weakening the concrete. 


![](https://www.dropbox.com/s/lwaiswwq0j95y4p/casting-the-tile-from-the-urethane-rubber-mold-11.jpg?dl=1)


In any case, I opened the container and took out the molds. They looked quite good! 

Apparently the cement was hard enough as to be handled with confidence, although I could see a few moisture marks over the cement surface.


![](https://www.dropbox.com/s/novuz46tunjb900/casting-the-tile-from-the-urethane-rubber-mold-12.jpg?dl=1)


**Demolding the cured concrete**

With the molds outside the container, I carefully removed both concrete parts from the urethane rubber molds and… unfortunately for me, the entire surface of the tiles was full of holes left there by trapped bubbles! 

Obviously, it seemed like I had not performed the bubbles extraction properly and big air bubbles had remained in the bottom of the mold. 

On the other hand, the surface of the tiles presented dark spots. Probably produced by the use of lubricant oil as release agent. But I am not totally sure of this, as tiles with no release agent subsequently casted presented dark spots too.


![](https://www.dropbox.com/s/lpu4t86h3wosgqk/casting-the-tile-from-the-urethane-rubber-mold-13.jpg?dl=1)



![](https://www.dropbox.com/s/0vsx2x4zy3448zq/casting-the-tile-from-the-urethane-rubber-mold-14.jpg?dl=1)



**Improving the result**

As my first attempt to cast concrete was not as good as I expected, I felt the need to repeat the whole concrete casting process until I until the result satisfied me.

I was much more careful during the air bubble extraction stage and, since the molds were rubber made, I also stretched and warped them in order to help getting the air bubbles out (in addition to the vibration methods previously used).

On the other hand, I also cast concrete using the other cement leftovers available.

This time the results were far better!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_32A817ACBC02AB8E5FEF60710902B72863A17472CEACEC753F83C8254EBAC5B9_1527765121393_IMG-3587.jpg)

## **What I have learned**

3D model CNC milling basics

types of milling waxes

How to use Modela Player 4

How to use Virtual MODELA

using urethane rubber 

working with concrete



## **Issues that I had**

extracting bubbles from the concrete cast




## **Tools used** 


- For modelling
  - MacOS High Sierra v10.13.3
  - Rhinoceros for Mac OS X v5.3.2 
  - Measuring tape
  - Caliper


- For preparing the milling file
  - Windows 7 Ultimate Service Pack 1
  - MODELA Player 4 v2.12
  - Virtual MODELA v1.71


- For milling the positive mold
  - Windows 7 Ultimate Service Pack 1
  - Roland VPanel for SRM-20
  - Roland Modela SRM-20
  - 1/8” (3.175 mm) 2 flute square milling bit
  - 1/32” (0.793 mm) 2 flute square milling bit
  - Machinable wax 
  - Double-sided cloth tape
  - Vacuum cleaner
  - Brush
  - Zip bag


- For making the silicone negative mold
  - Smoth-On VytaFlex40 urethane liquid rubber
  - Universal mold release agent
  - Machinable wax mold
  - ABS 3D printed mold
  - Mixing container
  - Wooden stick stirrers
  - Brush
  - Security glasses
  - Latex gloves
  - Apron
  - Scale
  - Vacuum chamber
  - Marker
  - Scraper


- For casting the object
  - Microcement
  - Water
  - Scale
  - Mixing container
  - Trowel
  - Security glasses
  - Latex gloves
  - Apron
  - Brush
  - Tupperware
## 
## **Files to download**









Links
https://www.smooth-on.com/products/universal-mold-release/
https://www.smooth-on.com/product-line/universal-mold-release/
https://www.smooth-on.com/products/universal-mold-release/
https://www.smooth-on.com/product-line/vytaflex/
https://www.smooth-on.com/products/vytaflex-40/
https://www.smooth-on.com/products/inandoutii/





Two part mold
To be completed anytime soon…

Duplicating vinyl records by casting
To be completed anytime soon…


# 

