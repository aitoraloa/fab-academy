# Mechanical Design

Shown how your team planned and executed the project
Described problems and how the team solved them
Listed future development opportunities for this project
Included your design files, 1 min video (1920x1080 HTML5 MP4) + slide (1920x1080 PNG)