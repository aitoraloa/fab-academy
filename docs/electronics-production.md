# Electronics Production

As individual assigment for the [Electronics Production week](http://academy.cba.mit.edu/classes/electronics_production/index.html) I had to fabricate one of the FabISP programmers available at the Fab Academy archives.

Besides, as group project for this week's assignment we had to describe the PCB production process by milling a test pattern. The group project would also serve to check that the mini CNC machines were working well.



## **Deciding which FabISP to make**

A FabISP is an in-system programmer for AVR micro-controllers, designed for production within a Fab Lab that allows to program the micro-controllers on other boards. And fabricating one of these programmers meant [CNC](https://en.wikipedia.org/wiki/Numerical_control) [milling](https://bit.ly/2GNcs09) the [printed circuit board](https://en.wikipedia.org/wiki/Printed_circuit_board) (PCB), [soldering](https://en.wikipedia.org/wiki/Soldering) the [electronic components](https://en.wikipedia.org/wiki/Electronic_component), [programing](https://en.wikipedia.org/wiki/Computer_programming) the board, and testing it. 

Among the several versions of the FabISP available in [Fab Academy’s Electronics Production page](http://academy.cba.mit.edu/classes/electronics_production/index.html), we could choose any of them to complete this assignment. 

The truth is that I would have loved to make more than one, but I finally opted for one of Neil’s versions (which was based on David Mellis's [FabISP](http://fab.cba.mit.edu/content/projects/fabisp/), which was based on Limor's [USBTinyISP](http://www.ladyada.net/make/usbtinyisp/index.html), which was based on Dick Streefland's [USBTiny](http://dicks.home.xs4all.nl/avr/usbtiny/)).

On the other hand, from the two versions available of Neil’s FabISP (hello.ISP.44 vs hello.ISP.44.res), I chosen the hello.ISP.44 board. 

Note that he main difference between these two boards was that the hello.ISP.44 uses a 20MHz crystal oscillator and two 10pF capacitors for the clock signal, while the hello.ISP.44.res uses just one 20MHz resonator. Apart from that, everything else was completely the same!


![](img/electronics-production/deciding-which-fabisp-to-make/deciding-which-fabisp-to-make-01.png)


The very first thing I did after deciding which FabISP to fabricate, was to download all the PNG files related to the hello.ISP.44 board available at the [Fab Academy’s “Electronics Production” week page](http://academy.cba.mit.edu/classes/electronics_production/index.html) ([board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png), [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.components.png), [traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png), and [interior](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.interior.png)).  

Then, I saved all these files in my Fab Academy student folder at [IAAC’s cloud](https://cloud.iaac.net:5001/index.cgi). 

Note that I was going to need the [traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png) and [interior](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.interior.png) PNG files for preparing the CNC milling files first, and then I was going to need also the [board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png) and [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.components.png) PNG files for pinpointing and assembling the components, so start getting used to save the files in the cloud seemed like a good idea, as this would allow me to quickly access them from any of the computers that I would have to use during the whole PCB manufacturing process.


![](img/electronics-production/deciding-which-fabisp-to-make/deciding-which-fabisp-to-make-02.png)


![](img/electronics-production/deciding-which-fabisp-to-make/deciding-which-fabisp-to-make-03.png)


In theory, the next step should have been preparing the milling files. I already had the PNG files for the traces and cutting the outline of the PCB that I was going to mill, so I should have been able to proceed.

Anyway, as the milling files had to be prepared taking into account the machine/tools that I was going to use, I needed to figure this out before going further in this assignment.



## **Reviewing the PCB milling machines**

Needless to say, learning how to properly use a PCB prototyping machine was essential in order to complete this assignment successfully. 

On the other hand, I already knew that we were going to use these little CNC machines in subsequent assignments ([Electronics Design](http://academy.cba.mit.edu/classes/electronics_design/index.html), [Molding and Casting](http://academy.cba.mit.edu/classes/molding_casting/index.html), [Input Devices](http://academy.cba.mit.edu/classes/input_devices/index.html), [Output Devices](http://academy.cba.mit.edu/classes/output_devices/index.html), and [Networking and Communications](http://academy.cba.mit.edu/classes/networking_communications/index.html)), so learning as much as possible about them from the very beginning seemed a really good idea to me.

At the time I was writing this review, there were two different models of desktop CNC machine available at Fab Lab Barcelona’s electronics room:


### **Roland** [**Modela MDX-20**](http://support.rolanddga.com/_layouts/rolanddga/productdetail.aspx?pm=mdx-20)


![](img/electronics-production/reviewing-the-pcb-milling-machines/reviewing-the-pcb-milling-machines-01.png)


The MDX-20 was the oldest of the PCB prototyping mini CNC machines available at the lab. 

Nonetheless, it was not only capable of milling material such as ABS, acrylic, woods, plaster, styrene foam, chemical wood, modeling wax, and light metals such as aluminum and brass, but it could be also used for precise 3D scanning using the “Roland Active Piezo Sensor” accessory.

The maximum work area for this machine was 203,2 (X) x 152,4 (Y) x 60,5 (Z) mm. And while the software resolution was 0.025 mm/step, the mechanical resolution was 0,00625mm/step. On the other hand, the feed rate during milling operations could go from 0.1 to 15 mm/sec.

This machine included a 10W DC spindle motor that could reach a maximum revolution speed of 6500 rpm, with a tool chuck for 3.175 mm (1/8") shank diameter milling bits.

It was connected via RS-232C (using a Serial to USB adapter) to a dual-boot Windows7/Ubuntu computer. And as far I could find out, it used to be run on Windows for 3D scanning operations, using Dr. PICZA software application. And for milling jobs, it used to be operated using the backend installation of [Fab Modules](http://kokompe.cba.mit.edu/) in Ubuntu.

At the time of this writing, this PCB prototyping machine had been discontinued by Roland, and replaced by the fancy MonoFab SRM-20.



### **Roland** [**MonoFab SRM-20**](https://www.rolanddgi.com/en-es/products/modelling-machines/monofab-srm-20) 


![](img/electronics-production/reviewing-the-pcb-milling-machines/reviewing-the-pcb-milling-machines-02.png)


The monoFab SRM-20 was the latest desktop milling machine by Roland at that time. 

As mentioned before, one might said that it was an updated version of the Modela MDX-20, although this new version was intended just for CNC milling (modeling wax, wood, chemical wood, foam, cork, plaster, acrylic, poly-acetate, ABS, or PC boards), and did not include the 3D scanning capabilities of its predecessor. 

The operation strokes were also 203,2 (X) x 152,4 (Y) x 60,5 (Z) mm, but the spindle motor and the tool chuck system were completely different. 

It incorporated a DC Motor Type 380 with a maximum rotation speed of 7000 rpm, and the collet system for 3.175 mm (1/8") shank diameter milling bits included by default was independent, so it could be easily replaced by another collet for cutting tools with bigger or smaller shank diameter.

The software resolution was variable depending on the control commands set used, being 0.01 mm/step for RML-1 commands, and 0.001 mm/step for NC code commands.  While the maximum mechanical resolution was 0.000998594 mm/step.

Unlike the Modela MDX-20, the monoFab SRM-20 machines had a fully enclosed cabinet, reducing dust and noise. The cover also featured an intelligent safety interlock that automatically paused the machine when the cover was opened. 

Each of the two monoFab SRM-20 PCB prototyping machines available at Fab Lab Barcelona was connected via USB to a dedicated dual-boot Windows7/Ubuntu computer.

Under windows, the software installed was VPanel (control panel),  MODELA Player 4 (CAM), Virtual MODELA (preview tool), iModela Creator (design to production tool), ClickMILL software (direct control), and SFEdit2 software (TrueType to Stroke font converter).



## **Getting to know machine and tools**

I would have loved to learn how to use both models during this assignment, but the computer connected to the Modela MDX-20 was not booting when I was completing this assignment, so I had to focus only on learning how to use the monoFab SRM-20. 

Maybe in the future (if the computer attached to the Modela MDX-20 is fixed), I would have the chance to spend some time playing with it. I would really love that! But for the time being, I downloaded the [user manual for the Roland SRM-20](http://support.rolanddga.com/docs/documents/departments/technical%20services/manuals%20and%20guides/srm-20_use_en.pdf), and I carefully read it. 


### **Reading the user manual**

Reading the 152 pages of the manual described the machine’s features further (including part names and functions), and gave me a general idea of the very basics when working with a Roland SRM-20 desktop milling machine.

On the other hand, it also explained the workflows when using the different software applications provided by Roland (SRP Player, MODELA Player, etc).

Anyway, as I was going to use Roland’s VPanel software application and the web-browser version of [Fab Modules](http://kokompe.cba.mit.edu/) for milling the PCB, most of the software related information in the user manual was not very relevant at that moment, but surely it would be very useful later.


![](img/electronics-production/getting-to-know-machine-and-tools/getting-to-know-machine-and-tools-01.png)


### **The cutting tools**

The cutting tools were a relevant element to have in mind when generating the milling files.

In that sense, the user manual included a quick view of the different shapes of milling bits that could be used (flat tip, round tip, pointed tip, etc). And it also explained a extremely important information, which was how to properly attach these cutting tools to the existing collet.

In any case, as explained by Neil during the lecture, among the vast amount of milling bits that could be used with this machine, we were going to use mainly two flat tip bits for milling the PCBs. A 1/64” (0.396 mm) milling bit for milling the traces, and a 1/32” (0.793 mm) bit for the outcuts. 

A third 0.01” (0.254 mm) milling bit would be also available for milling traces, but only if the distance between the traces/pads of the circuit to be milled was so small that we were forced to use this extra-small milling bit instead of the 1/64” (0.396 mm).

In my particular case, using the 1/64” (0.396 mm) and 1/32” (0.793 mm) bits was enough to mill the hello.ISP.44 PCB successfully.


![](img/electronics-production/getting-to-know-machine-and-tools/getting-to-know-machine-and-tools-02.png)


### **The machining material**

Of all the materials that could be used to make PCBs with the Roland SRM-20, the one we were going to use was phenolic paper or [FR-1.](https://support.bantamtools.com/hc/en-us/articles/115001671734-FR-1-PCB-Blanks-) 

FR1 is a material often used to make printed circuit board substrates, that is made of [wood fibre](https://en.wikipedia.org/wiki/Wood_fibre) and [phenolic polymers](https://en.wikipedia.org/wiki/Phenol_formaldehyde_resin). It is a fibre reinforced plastic, most commonly brown in color, and can have a copper foil lamination on one or both sides. 

The circuit board stock that I was going to use for making the hello.ISP.44 board was a 1.6 mm thick 76,2 x 50,8 mm (3x2”) single sided FR1 PCB. Actually, with only one of these FR1 PCBs it could be possible to mill up to three hello.ISP.44, as long as the space was well seized.


![](img/electronics-production/getting-to-know-machine-and-tools/getting-to-know-machine-and-tools-03.png)


Once I knew the machine, milling bits and material I was going to use, I was completely ready to prepare the milling files.


## **Generating the milling files**

A file with the [PNG file extension](https://en.wikipedia.org/wiki/Portable_Network_Graphics) is very useful to store graphics on websites but, unfortunately, it is not a file extension that the Roland SRM-20 milling machines (or any other CNC machine in the Fab Lab) can understand. 

In order to make the Roland SRM-20 to understand the images of the circuit that I had in PNG file format, it was necessary to convert them to [G-code](https://en.wikipedia.org/wiki/G-code).


### **Understanding G-Code**

[G-code](https://en.wikipedia.org/wiki/G-code) is the most widely used programming language for controlling industrial machines such as [mills](https://en.wikipedia.org/wiki/Milling_(machining)), [lathes](https://en.wikipedia.org/wiki/Lathe) and [cutters](https://en.wikipedia.org/wiki/Plasma_cutting), as well as [3D-printers](https://en.wikipedia.org/wiki/3D_printing). 

It has many variants, but most (or all) adhere to certain common rules. In the case of the SRM-20 milling machine, it interprets both [Roland RML code (Roland Machine Language) code](https://itp.nyu.edu/classes/cdp-spring2014/files/2014/09/RML1_Manual.pdf) as well as industry standard [NC code](http://support.rolanddga.com/docs/documents/departments/technical%20services/downloads/mdx-nc_code_en.pdf) (G-code).

Fab Modules can easily generate RML code so using Fab Modules would be the right way to go for this exercise (although using alternative software was also very tempting). 

Besides, the browser-based version of Fab Modules can be accessed from any internet-connected computer, which makes learning how to use it and preparing the files very handy.


### **Generating the RML code for the traces**

In order to start converting the PNG files to RML code using the web browser version of Fab Modules, the first thing I did was opening a new browser window in Google Chrome. 

Then, I wrote “fabmodules.org” in the address bar.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-01.png)


Once the “fabmodules.org” page was loaded, I left-clicked the “input format” button at the upper left-hand corner of the webpage, and I selected “image (.png)” from the drop-down list that appeared. 


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-02.png)


As soon as I clicked “image (.png)” in the “input format” drop-down menu, a navigation window opened, and I used that navigation window to search the “hello.ISP.44.traces.png” file that I had previously saved in my Fab Academy student folder at IAAC’s cloud. 

Once I found the “hello.ISP.44.traces.png” file, I selected and opened it by clicking on the “Open” button at the lower-right corner of the navigation window. 

When I clicked the “Open” button, the navigation window closed, and the image with the traces of the hello.ISP.44 board appeared in the screen.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-03.png)


Simultaneously, a new button labeled as “output format” appeared right next to the former “input format” button (converted to “image (.png)” after selecting that option in the drop-down menu).


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-04.png)


On the other hand, an “input” parameters menu with details of the opened file (name, dpi, size) appeared at the upper right-hand side of the fabmodules.org web page.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-05.png)


Just in case, I checked the size and resolution of the board. Everything seemed to be correct.

Next, I left-clicked the “output format” button that had just appeared, and I selected the “Roland mill (.rml)” option from the drop-down list that appeared.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-06.png)


As soon as I selected the “Roland mill (.rml)” option, a new button labeled as “process” appeared right next to the former “output format” button (converted to “Roland mill (.rml)” after selecting that option from the drop-down menu).  

Furthermore, an “output” parameters menu appeared at the right-hand side of the webpage, just below the “input” parameters menu hat had appeared in the previous step.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-07.png)


Next, I clicked the “process” button, and I selected the option “PCB traces (1/64)” from the drop-down list that appeared. 

Note that, as mentioned before, the right milling bit for machining the traces of the PCB was the 1/64” bit, so it was very important to select this option correctly. 


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-08.png)


As it had happened before, when I selected “PCB traces (1/64)” from the “process” drop-down menu, a “process” parameters menu appeared at the right-hand side of the web page.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-09.png)


Once I had selected the right “input”, “output” and “process” in the corresponding drop-down menus at the top of the fabmodules.org page, it was time to modify the values in the “output” and “process” parameters menus at the right-hand of the screen.

I started this stage of the preparation by revealing the drop-down menu under “output > machine” and selecting “SRM-20” from it. Selecting this option, automatically filled in the rest of the output parameters with default values. 

Note that I left the “speed (mm/s)” parameter as it appeared by default (4), but I changed the “x0(mm)”, "y0(mm)”, and “z0(mm)” parameters to 0, so there would be no offset from the origin point that I was going to set in the machine later. Note also that I changed the “zjog (mm)” parameter form its default value to 12, so the tool would rise 12 mm over the material every time it had to move from one point to the other of the PCB during the milling process.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-10.png)


On the other hand, note that I did not change any of the values in the “process” parameters menu, so all the parameters in that menu were left as they had appeared by default when I had selected the “PCB traces (1/64)” option in the “process” drop-down menu before:


- direction: climb  
- cut depth (mm): 0.1
- tool diameter (mm): 0.4
- number of offsets (-1 to fill): 4
- offset overlap (%): 50
- path error (pixels): 1.1
- image threshold (0-1): 0.5
- sort path:  yes
- sort merge diameter multiple: 1.5
- sort order weight: -1
- sort sequence weight: -1

Next, I left-clicked the “calculate” button available under “process”, and the image of the hello.ISP.44 traces completely disappeared from the screen. 

A few second later the different tool paths started to appear one by one (in blue color), up to a total number of 4 (as selected in the “number of offsets” parameter). Along with the four tool paths, it was possible to also see also the tool’s travel movements. (in red color).


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-11.png)


Finally, I left-clicked the “save” button under “process”, and a new navigation window opened. 

I used that navigation window to search for my Fab Academy student folder at IAAC’s cloud, and once I found it, I saved the milling job as “hello.ISP.44.traces.rml”

The RML file for machining my hello.ISP.44’s traces in the monoFab SRM-20 was ready, but I still needed to prepare the RML for the outline cut, so I basically needed to repeat the very same process again, but changing the source file, some options, and few parameter values.


### **Generating  the RML code for the outline**

In order to prepare the RML file for the hello.ISP.44 board’s outline cut, I started by refreshing Google Chrome (cmd+5) , meaning that all the changes made while preparing the previous RML file disappeared from the window browser. 

I was back in the beginning, and the “input format” button was again waiting to be clicked at the upper-left corner of the fabmodules.org page.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-12.png)


I left-clicked the “input format” button and selected “image (.png)” from the drop-down menu.

Using the navigation window that opened, I searched for the “hello.ISP.44.interior.png” file in my Fab Academy student folder at IAAC’s cloud, and I opened it.

As it had happened before, the image of the hello.ISP.44.interior appeared on the screen, the “Output format” button appeared right next to the former “input format” button, and the “input” parameters menu appeared at the upper right of the fabmodules.org page.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-13.png)


Next, I clicked on the “output format” button, and I selected the “Roland mill (.rml)”.

The “process” button appeared right next to the former “output format” button, and the “output” parameters menu appeared at the right side-hand of the webpage. 

Note that until this point, the whole RML file preparation process was identical to the previous one (except for the PNG used). Some things would be different from now on!


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-14.png)


As mentioned before, the 1/32” milling bit was the right tool for milling the PCB outcut, and once again, it was very important to select it correctly in order to obtain a proper calculation later.

Therefore, I selected “PCB outline (1/32)” in the drop-down menu under the “process” button.

Once the “PCB outline (1/32)” option was selected from the “process” drop-down menu, the “process” parameters menu appeared at the right-hand side of the page, just below the “input” and “output” parameter menus that had appeared before.  


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-15.png)


It was time to edit the “output” and “process” parameter menus at the right-hand side.

Under “Output > machine”, I selected “SRM-20”. 

Then, I changed the “speed (mm/s)” parameter to 0.5; the “x0(mm)”, "y0(mm)”, and “z0(mm)” parameters to 0; and the “zjog (mm)” parameter to 12.

Note that (as I found out later) a speed of 4 under “speed (mm/s)” would work correctly, but (as I was not on a hurry to complete this assignment) I preferred to be a bit conservative.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-16.png)


Furthermore, I left all the values in the “process” parameters menu as they were by default:


- direction: climb  
- cut depth (mm): 0.6
- stock thickness (mm): 1.7
- tool diameter (mm): 0.79
- number of offsets (-1 to fill): 1
- offset overlap (%): 50
- path error (pixels): 1.1
- image threshold (0-1): 0.5
- sort path:  yes
- sort merge diameter multiple: 1.5
- sort order weight: -1
- sort sequence weight: -1

 
Then, I clicked the “calculate” button under “process”, and the image of the hello.ISP.44 interior completely disappeared from the screen,.

A few seconds later the tool paths appeared (in blue color). 


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-17.png)


Note that the “stock thickness” was 1.7 mmm, and the “cut depth” was set to 0.6 by default, meaning that the tool would make the passes in order to cut the whole stock thickness. 

In order to see the aforementioned multiple tool passes, I rotated the view by keeping the mouse’s right button pressed and moving the mouse over the tool paths image. 


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-18.png)


Finally, I clicked the “save” button under “process”, and I used the navigation window to save the milling job as “hello.ISP.44.traces.rml” in my Fab Academy student folder at IAAC’s cloud.

At that moment, the two RML files that I needed for milling the hello.ISP.44 PCB were ready.

It was time to get close to one of the SRM-20 milling machines! 😉 


## **Milling the hello.isp PCB**

### **Initializing the machine**

As nobody had been using the machines when I started this part of the assignment, the first thing when I entered Fab’s electronics area I did was to turn on the computer connected to the machine.

Immediately after, and while the computer was still booting, I turned on the Roland SRM-20 CNC milling machine by pressing the power button located at the top of the fairing.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-01.png)


As soon as I pressed the power button, the power button’s LED started to flash, and the machine’s initialization process started. During the initialization process, the spindle head moved up (Z axis) and left (X axis), and the machine’s table/bed moved to the front (Y axis). 

Once the initialization was completed, the power button’s LED stayed lit.


### **Setting up the machine**

With the SRM-20 initialized and the computer turned on, I launched the “VPanel for SRM-20” software application by clicking in the corresponding icon.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-02.png)


The very first thing I did after the “VPanel for SRM-20” program opened was checking the machine’s configuration by clicking the “Setup” button at the lower right of the program window.

This opened a “Setup” dialog window that included 2 tabs: “Modeling Machine” and “Correction”.

In the “Modeling Machine” tab, I checked that “RML-1/NC Code” was selected under “Command Set”, and that “Millimeters” was selected under “Unit”. These two were the most relevant settings.

Although it was not very relevant, I also checked that the preferred option “Move cutting tool to desired location” was selected under “Direction of the Y axis moving keypad”.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-03.png)


Then, I selected the “Correction” tab, and I checked that the correction values for the three axes available in that dialog window were set to 100%.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-04.png)


Finally, I closed the “Setup” dialog window by clicking the “OK” button at the lower right corner.


### **Attaching the stock material (and warming up the spindle)**

After checking that the machine parameters were correctly set up, I forgot about the “VPanel for SRM-20” application for a few seconds, and I started the preparations for attaching the stock material to the machine’s table.

On that basis, I opened the machine’s lid and un-tightened four screws that keep the table coupled to the Y axis. Then, I gently slid the table forward so it could be uncoupled from the screws and extracted from the machine.

Note that it is also possible to attach the stock material without removing the table from the machine, but it seemed much more convenient to me to remove it.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-05.png)


On the other hand, I though that it would be a good idea to take advantage of the 3-5 minutes that I was going to spend attaching the stock material to the table for for warming up the spindle.

In order to warm up the spindle, I closed the machine’s lid, and I turned the spindle on by clicking the “ON” button under “Spindle” in the VPanel program. As I just wanted to warm the spindle up for a few minutes, I also set the slide control under “Spindle Speed” to the lowest value.

Note that arming up the spindle was not mandatory, but as far as I understood while reading the SRM-20’s user manual, it is a recommended practice


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-06.png)


While the spindle was warming up, I thoroughly cleaned the surface of the MDF sacrificial board that was taped down on top of the machine’s metallic bed. There were not adhesive or double tape residues over the MDF, so I basically focused on removing the dust particles using a soft brush.

Right after cleaning the sacrificial board, I checked that it was firmly attached to the metal table, and also that its surface of the MDF board was flat. Apparently, it was! 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-07.png)


The next step was attaching the FR1 PCB to the sacrificial board.

In order to do so, I cut a couple of pieces of double sided tape, and I applied them all the way across the bottom side of the FR1 PCB, being careful not to leave folds or overlaps.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-08.png)


Then, I removed the paper liner from the double sided tape, and I sticked the PCB on the MDF sacrificial board, so that the PCB’s front side was completely parallel to the table's front side.

Next, I pressed the PCB against the MDF using a piece of paper so the PCB would be properly sticked, but without leaving fingerprint marks on the copper surface. 

Note that leaving fingerprints on the copper surface makes it harder to solder the components to the board, so cleaning any fingerprint or spot before milling is highly recommended.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-09.png)


With the PCB correctly attached to the MDF, it was time to put the table back in the machine.

As the spindle was still spinning at low speed, I set the “spindle speed” parameter back to 100% first, and I stopped the spindle head by clicking the “OFF” button in the VPanel application

Once the spindle head stopped, I opened the machine’s lid and relocated the table in place.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-10.png)



### **Inserting the milling bit**

With the table in position and the screws correctly tighten, I used the [X][Y] feed buttons of the VPanel program to move the spindle head until it was over the PCB. 

Once the spindle was over the PCB, I moved the spindle head up using the [Z] feed button in order to have enough space as to insert the milling bit, but also as for the hole of the mounting screw to be accessible.

Then, I grabbed the 1/64” milling bit and, after checking that the tip was in good condition, I inserted approximately 1/3 of the shank into the spindle's collet.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-11.png)



Once the milling bit was attached to the collet, I needed to set the origin point!


### **Setting the origin point**

In order to set the origin point for the PCB milling job that I was about to perform, I started by bringing the tip of the milling bit near the front-left corner of the PCB, using VPanel’s [X][Y] feed buttons. 

Then, I used the [Z] feed button to slowly move the spindle head down, until the tip of the milling bit was 3 or 4 mm above the left-front corner of the PCB.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-12.png)


Once I the milling bit was over the front-left corner of the PCB, and 3-4 mm away from its surface, I pressed the “X/Y” button under “Set Origin Point” in the “VPanel for SRM-20” program window, so the X and Y coordinate values in the left side of the program window were set to “0.00 mm”. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-13.png)


The origin point for the X and Y axes was set, but I still had to set the origin point for the Z axis. 

In order to do that, I used an hex key to loosen the collet's set screw and, while holding the milling bit from the shank with two fingers, I allowed the bit to gently land on the PCB's surface.

Once the tip of the milling bit was in contact with the surface of the PCB, I used the hex key again to tighten the collet screw.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-14.png)


Then, I clicked the “Z” button under “Set Origin Point” in the “VPanel for SRM-20” program window, so the Z coordinate in the left side of the program window was also set to 0. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-15.png)


At that point, the PCB was attached to the machine, the 1/64” milling bit was inserted in the collet, and the origin point was set. Therefore, it was time to send the corresponding milling job!


### **Sending the milling job**

In order to send the milling job to the machine, I clicked on the “Cut “ button at the lower right of the “VPanel for SRM-20” program window, and the “Cut” dialog window appeared on the screen.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-16.png)


Then, I clicked on the “Add” button at the lower left of the “Cut” dialog window, and a new navigation window opened. Using that navigation window called “Open”, I searched for the “hello.ISP.44.traces.rml” file that I had previously saved.

Once I found the “hello.ISP.44.traces.rml” file, I opened it. 

The navigation window closed, and the hello.ISP.44.traces.rml file appeared in the “Output File List” of the “Cut” dialog window that was still open behind.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-17.png)


With the “hello.ISP.44.traces.rml” file in the “Output File List”, I clicked the “Output” button at the lower right of the “Cut” dialog window, and the machine started milling.

After a few minutes the milling job had finished, so I opened the machine’s lid, and I cleaned the PCB’s surface with a brush so I could check the outcome. Oh, nooooooooo!!

Unfortunately, the copper had not been completely removed in some areas of the PCB, and few traces had not been correctly milled, meaning that the surface of the sacrificial board was not flat.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-18.png)



### **The sacrificial board is not flat!?**

The only way to fix this eventuality was to prepare a milling job to flatten the whole surface.

And the easiest way to do that was using Roland’s proprietary Click Mill software, and one of the 1/8” 2FL flat-end milling bits available in Fab Lab Barcelona’s inventory.

I removed the PCB from the sacrificial board, and I replaced the 1/64” milling bit for a 1/8” milling bit. Then, I followed the zeroing process for setting the new origin point in the scrap board, and I launched the Click Mill application. 

Once Click Mill was open, I selected the “Surface” tab. I introduced the details for the “Lower left” and the ”Upper right” corners of the sacrificial board. And I also introduced, the cutting “Depth” and “Tool-up Height” values.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-19.png)


Then, I clicked in the the “Cut” button in lower-left of the program window, and a new dialog window appeared were I selected the “Tool” to be used and the “Material” to be machined. 

Before going any further, I reviewed the “Cutting parameters” added by default when selecting the “Tool” and “Material” were reasonable. 

Finally, I clicked the “Cut” button in the lower left corner, and the SRM-20 started!


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-20.png)


A while later, the MDF sacrificial board’s surface was completely machined and, allegedly, absolutely flat, so… I could re-start the PCB milling process again!


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-21.png)


### **Sending the milling job… again!**

The first I did in order to sending the milling job again was to remove the old pieces of double sided tape that I have used to stick the FR1 PCB to the MDF on my first try, and I added new ones. 

Then, I sticked the FR1 PCB on the freshly milled MDF board so the PCB’s front side was completely parallel to the table's front side, and making sure that there were no fingerprints or spots on the copper surface. 

Next, I removed the 1/8” milling bit that was still in the collet, and I replaced it with the 1/64” milling bit that I had used before as its tip was still in good condition. 

And finally, I set a new origin point just at the right of my previous failed attempt.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-22.png)


Then, I clicked on the “Cut “ button of the VPanel program again. 

The “Cut” dialog window appeared and the “hello.ISP.44.traces.rml” file was still in the “Output File List”, so I just had to click the “Output” button and the milling machine started.

After a few minutes of “uneasy” wait, the machining job had finished. As soon as the Spindle stopped, I opened the machines lid, moved the table to the front, and cleaned the PCB’s surface.

This time the outcome was much more satisfying! No perfect… but better! 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-23.png)


Considering the previous issues, I must admit that I was quite happy that I had reached this point, but the PCB milling process was not finished yet… I still had to machine the PCB’s outline!

The first thing I did in order to proceed with the PCB’s outline machining was changing the tool. 

As mentioned before, the right milling bit for milling the outcut is the 1/32” bit, so I moved the spindle up, removed the 1/64” bit that was still in the collet, and I replaced it with the 1/32” bit. 

Once I changed the milling bit, I clicked the “X/Y“ button under “Move > To Origin” in the VPanel program window, and the spindle head moved to the origin point that I had set before for milling the traces. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-24.png)


Then, I slowly moved down the spindle head until the tip of the milling bit was around 3-4 mm above the PCB plate, and I repeated the same procedure for setting the origin point in the Z axis that I have used before.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-25.png)


After zeroing the Z axis, I clicked on the “Cut “ button of the VPanel program.

In the “Cut” dialog window that openened it was still the “hello.ISP.44.traces.rml” file listed under “Output File List”. That was the file I had already used for milling the traces and I do not needed anymore, so I click the “Delete” button in order to remove it form the “Output File List”.

Then, I clicked on the “Add” button at the lower left of the “Cut” dialog window, and I searched for the “hello.ISP.44.interior.rml” file using the navigation window that appeared.

Once I opened the “hello.ISP.44.interior.rml”, the “Open” navigation window closed, and the “hello.ISP.44.interior.rml” file appeared in the “Cut” dialog window, under “Output File List”.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-26.png)


I clicked the “Output” button, and the milling process started again

A few minutes later, when the  machining job had finished, I opened the machines lid, moved the table to the front, and cleaned the PCB’s surface with a brush. 

The whole FR1 PCB was still attached to the scrap board, but it could be clearly seen that the machine had cut the whole PCB thickness surrounding the traces. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-27.png)


With the help of a spatula, I gently removed both the hello.ISP.44 PCB and the rest of the FR1 PCB
from the scrap board.

Then, I used a small piece of steel wood and a bit of cleaning alcohol to clean the PCb’s surface.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-28.png)


I have already milled my first PCB and it was not all bad, but I was also totally sure that the outcome could be improved with a little more practice. 

Since I already had the whole process clear, I repeated it a few times more, but changing some of the parameters in Fab Modules’ “process” menu, until I got a handful of hello.ISP.44 PCBs with a much more better finish.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-29.png)


Note that I even etched a hello.ISP.44 board (it can be seen along with the others in the picture above) using photosensitive positive PCBs, transfer paper, a UV-exposure unit, Hydrogen peroxide, Hydrochloric acid and a bunch of utensils, but I guess that is a long story for another time.



## **Assembling the Hello.ISP board**

Once the PCB for the Hello.ISP was ready, it was time to for me to identify and collect the electronic components that I needed for assembling my FabISP board. 


### **Identifying and collecting the electronic components**

With the help of the [hello.ISP.44 labeled board diagram](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png) and the [image of an assembled hello.ISP board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.components.png) that I had downloaded at the very beginning of this assignment, I made a list indicating the name of the parts and the quantity needed of each of them:


- 1x Mini-B USB connector
- 1x ATTiny 44 microcontroller
- 1x 6 pin header
- 1x 1uF capacitor
- 1x 10pF capacitor
- 2x 0 Ohm resistor
- 2x 100 Ohm resistor
- 1x 499 Ohm resistor
- 1x 1k Ohm resistor
- 1x 10k Ohm resistor
- 1x 20MHz crystal
- 2x 3.3V Zener diode

Besides, in order find out more information about the parts needed, I searched for them in the [Fab Lab Inventory](https://docs.google.com/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html), where I obtained the [Digi-Key](https://en.wikipedia.org/wiki/Digi-Key) Part Number for each of these components.

Knowing the Digi-Key Part Number, allowed me to visit their product pages and, in that way, I was able to see what they looked like, and I could also learn more about them.


- Mini-B USB connector → [Digi-key Part Number: H2961CT-ND](https://www.digikey.com/products/en?keywords=H2961CT-ND%09)
- ATTiny 44 microcontroller → [Digi-key Part Number: ATTINY44A-SSU-ND](https://www.digikey.com/products/en?keywords=ATTINY44A-SSU-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 1uF capacitor → [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND%09)
- 10pF capacitor → [Digi-key Part Number: 311-1150-1-ND](https://www.digikey.com/products/en?keywords=311-1150-1-ND%09)
- 0 Ohm resistor → [Digi-key Part Number: 311-0.0ERCT-ND](https://www.digikey.com/products/en?keywords=311-0.0ERCT-ND%09)
- 100 Ohm resistor → [Digi-key Part Number: 311-100FRCT-ND](https://www.digikey.com/products/en?keywords=311-100FRCT-ND%09)
- 499 Ohm resistor → [Digi-key Part Number: 311-499FRCT-ND](https://www.digikey.com/products/en?keywords=311-499FRCT-ND%09)
- 1k Ohm resistor → [Digi-key Part Number: 311-1.00KFRCT-ND](https://www.digikey.com/products/en?keywords=311-1.00KFRCT-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- 20MHz Crystal→ [Digi-key Part Number: 644-1039-1-ND](https://www.digikey.com/products/en?keywords=644-1039-1-ND%09)
- 3.3V Zener diode → [Digi-key Part Number: BZT52C3V3-FDICT-ND](https://www.digikey.com/products/en?keywords=BZT52C3V3-FDICT-ND%09)

After reviewing the the product pages of the parts I was going to use, I headed to the storage cabinets at Fab Lab Barcelona’s electronics room, and I started collecting everything I needed.

Note that reviewing the specs of the parts is not mandatory, but it made much more easier for me to find out what I had to look for. And i t also help me to familirice with the parts’ specfications.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509032697654_assembling-the-hello-isp-board-01.jpg)


I spent a few minutes in front of the cabinets considering the labels and opening drawer after drawer, until I had collected the right amount of every component on the list.

As I was planning to assembly the board using the soldering tools that I had at home instead of the Fab Lab’s equipment, I put all the collected components in as mall plastic box. 

Note that most of these parts were very small, so not keeping them inside a box, would have probably meant losing more than one on the way home.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1526633547834_IMG-3412.jpg)


### **Gathering the tools needed**

The first thing I did when I arrived home with the PCB’s and the parts, was to gather the tools that I was going to need in order to solder all that small SMD components to the Hello.ISP PCB:


- Soldering iron with a 0.5mm conical fine-point tip
- Soldering iron stand with sponge
- Distilled water
- 0.35 mm soldering wire
- Desoldering wick
- Tweezers
- Vacuum base PanaVise PCB holder
- Solder fume extractor
- Magnifying glass


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509034159343_assembling-the-hello-isp-board-02.jpg)


Luckily, I had experience soldering both [THT (Through-hole technology)](https://en.wikipedia.org/wiki/Through-hole_technology) and [SMD (surface-mount technology)](https://en.wikipedia.org/wiki/Surface-mount_technology) components, so I could start soldering without any intermediary steps. 

I plugged in the soldering iron into the mains, and while the soldering iron was heating up, I took all the electronic components out from the blisters in which they came.

Then, I ordered all of these parts near the Hello.ISP PCB.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509032729931_assembling-the-hello-isp-board-03.jpg)


### **Soldering the parts**

After double checking that I had all the right parts for completing the Hello.ISP assembly, I held the PCB in the [PanaVise](http://panavise.com/index.html?pageID=1&id1=1&startat=1&--woSECTIONSdatarq=1&--SECTIONSword=ww) tool, and I checked if soldering iron was already hot enough as to melt the ø0.35 mm soldering wire that I had chosen to use.

I placed the solder fume extractor close to the PanaVise, I turned it on, and I started soldering.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509035827076_assembling-the-hello-isp-board-04.jpg)


Following the [FabISP: Electronics Production tutorial](http://archive.fabacademy.org/archives/2017/doc/electronics_production_FabISP.html) available in the [Fab Academy tutorials page](http://archive.fabacademy.org/archives/2017/doc/index.html), the first component that I solder was the USB connector. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509035849704_assembling-the-hello-isp-board-05.jpg)


The next component to be soldered was the ATTiny 44 micro controller. 

In any case, it was very important to have in mind when soldering the ATTiny 44 chip that it had to be positioned with a specific orientation. The little circle on the chip indicating the VCC pin needed to be facing the right direction before soldering it.

Although this component may seem much more hard to solder than the USB connector, using the [“flood and wick” soldering technique](https://www.youtube.com/watch?v=8yyUlABj29o), it ended up being quite easy.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509035861318_assembling-the-hello-isp-board-06.jpg)


According to the [FabISP: Electronics Production tutorial](http://archive.fabacademy.org/archives/2017/doc/electronics_production_FabISP.html), the next component to be soldered was supposed to be the 20MHz Crystal. Then the diodes, the resistors, the capacitors, and the 6 pin header, in that order. 

The orientation of the crystal, resistors, capacitors, and the pin header did not matter because they do not have polarity. Instead, the diodes orientation is important because they do have polarity.

Knowing this, I preferred to skip the order recommended in the tutorial, and I soldered the two diodes first, ensuring that the tiny white line on the surface of the package indicating the cathode was facing the right direction.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509038681665_assembling-the-hello-isp-board-07.jpg)


Then I put solder in one of the pads where the crystal, resistors, capacitors and header were going to be placed, and I soldered all these components being careful of placing them in the right spot, but without paying close attention to the orientation, as I already knew that none of them would be problematic in that sense.

I am not totally sure if this was there best way to do it, but it worked pretty well for me. And after a few minutes of soldering, the Hello.ISP board was assembled, and it seemed to be ok!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509038142728_assembling-the-hello-isp-board-08.jpg)


Anyway, before doing anything else, I used the magnifying glass to double check that there were not cold points, bridges, or any other common soldering problems in the board. 

Everything appeared to be in order, so I venture myself to execute the last step of the FabISP: Electronics Production tutorial: the “smoke test”. 

Thus, I plugged the Hello.ISP board into my laptop using a mini-USB to USB-A cable, and I did not see smoke, or notice any weird smell coming out from the board. I did not receive any message about the board is drawing too much power from my laptop either, meaning that there were not shorts.

Apparently, I could proceed to the next stage. Programming the board!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509038120621_assembling-the-hello-isp-board-09.jpg)




## **Programming the Hello.ISP board**

In order to program the Hello.ISP board, I followed the [FabISP: Programming tutorial](http://archive.fabacademy.org/archives/2017/doc/programming_FabISP.html#mac). And according to this tutorial, the first thing I needed to do was installing the corresponding software. 

### **Installing the software**

As I was using a macOS laptop for programming the Hello.ISP board, the software that I needed in order to compile and upload the code to the ATtiny44A was [CrossPack AVR](https://www.obdev.at/products/crosspack/index.html). 

CrossPack AVR is a development environment for Atmel’s AVR micro controllers running on Apple’s Mac OS X, similar to AVR Studio on Windows. It consists of the GNU compiler suite, a C library for the AVR, the AVRDUDE uploader and several other useful tools.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509059210530_programming-the-hello-isp-board-01.jpg)


From the CrossPack downloads page, I downloaded the [latest version available at that moment](https://www.obdev.at/products/crosspack/download.html). 

Once the image file was completely dowloaded, I opened it by double clicking the image file, and a new window appeared with a Readme file and the installer package CrossPack-AVR.pkg.

I launched the installer file by double clicking on it, and I followed the installation instructions until the installation process was completed.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509060481721_programming-the-hello-isp-board-02.jpg)


According to the [FabISP: Programming tutorial](http://archive.fabacademy.org/archives/2017/doc/programming_FabISP.html#mac), the next step was “getting Make via [XCode](https://itunes.apple.com/us/app/xcode/id497799835)”. 

I already had XCode installed in my computer, so I directly downloaded the [FabISP Firmware for MacOS 10.8.2](http://www.as220.org/fabacademy/downloads/fabISP_mac.0.8.2_firmware.zip) to my desktop, using the download link available at the FabISP: Programming tutorial page.

Once the download was completed, I had to unzip the “fabISP_mac.0.8.2_firmware.zip” file that I have just downloaded. In order to do so, I followed the tutorial and used the Terminal program.

I launched the Terminal program, and I navigated to the desktop directory by typing:


    $ cd ~/Desktop/


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509368319425_programming-the-hello-isp-board-03.jpg)


Once in the Desktop directory, I used the “**unzip fabISP_mac.0.8.2_firmware.zip**” command to decompress the “fabISP_mac.0.8.2_firmware.zip” file. 

As soon as I hit the Enter key on my keyboard, the console spit out a few lines of code indicating that the **fabISP_mac.0.8.2_firmware.zip** file had been uncompressed.


    MacBook-Pro-de-Aitor:Desktop aitoraloa$ unzip fabISP_mac.0.8.2_firmware.zip
    Archive:  fabISP_mac.0.8.2_firmware.zip
       creating: fabISP_mac.0.8.2_firmware/
      inflating: fabISP_mac.0.8.2_firmware/.DS_Store  
       creating: __MACOSX/
       creating: __MACOSX/fabISP_mac.0.8.2_firmware/
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._.DS_Store  
      inflating: fabISP_mac.0.8.2_firmware/main.c  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._main.c  
      inflating: fabISP_mac.0.8.2_firmware/main.elf  
      inflating: fabISP_mac.0.8.2_firmware/main.hex  
      inflating: fabISP_mac.0.8.2_firmware/main.o  
      inflating: fabISP_mac.0.8.2_firmware/Makefile  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._Makefile  
      inflating: fabISP_mac.0.8.2_firmware/usbconfig.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._usbconfig.h  
       creating: fabISP_mac.0.8.2_firmware/usbdrv/
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/.DS_Store  
       creating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._.DS_Store  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/asmcommon.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._asmcommon.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/Changelog.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._Changelog.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/CommercialLicense.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._CommercialLicense.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/License.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._License.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/oddebug.c  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._oddebug.c  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/oddebug.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._oddebug.h  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/oddebug.o  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/Readme.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._Readme.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/USB-ID-FAQ.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._USB-ID-FAQ.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/USB-IDs-for-free.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._USB-IDs-for-free.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbconfig-prototype.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbconfig-prototype.h  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrv.c  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrv.c  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrv.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrv.h  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrv.o  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm.asm  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm.asm  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm.o  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm.S  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm.S  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm12.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm12.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm128.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm128.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm15.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm15.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm16.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm16.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm165.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm165.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm18-crc.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm18-crc.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm20.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm20.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/USBID-License.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._USBID-License.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbportability.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbportability.h  
    MacBook-Pro-de-Aitor:Desktop aitoraloa$

Once the “fabISP_mac.0.8.2_firmware” directory had been created, I navigated to this newly created directory by typing:


    $ cd fabISP_mac.0.8.2_firmware

Then, in order to see the files inside the fabISP_mac.0.8.2_firmware director, I typed:


    ls

And the console prompted:


    Makefile        main.c                main.elf        main.hex        main.o                usbconfig.h        usbdrv

At this point, I had CrossPack AVR and XCode installed, and I had the FabISP firmware for Mac downloaded too. The next step (according to the FabISP: Programming tutorial that I was still following) was powering the Hello.ISP board. 

### **Powering the Hello.ISP board**

It would be impossible to program the FabISP without powering it first.

The right way to power the Hello.ISP board was to connect it to the computer via USB, and to use a separated programmer plugged into the 6-pin programming header. 

The programmer could be a FabISP board configured as programmer, or any other commercially available programmers, such the [Atmel AVRISP MkII](http://ww1.microchip.com/downloads/en/devicedoc/atmel-42093-avr-isp-mkii_userguide.pdf) or the [Atmel-ICE](http://www.microchip.com/DevelopmentTools/ProductDetails.aspx?PartNO=atatmel-ice).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509450269998_programming-the-hello-isp-board-06.jpg)


I did not have any other working FabISP yet, but I had [Atmel AVRISP MkII](http://ww1.microchip.com/downloads/en/devicedoc/atmel-42093-avr-isp-mkii_userguide.pdf) at home.  At that moment, my only option was using the AVRISP MkII programmer to power (and eventually to program) my FabISP board. 

On the other hand, I had already read in the “FabISP: Programming” tutorial that the “Makefile” document present in the “fabISP_mac.0.8.2_firmware” directory was set up to work with the AVRISP MkII by default, so using the AVRISP MkII would also absolve me temporarily from having to edit the “Makefile” document.

Knowing this, I connected both the Hello.ISP board and the AVRISP MkII programmer to my laptop using the corresponding USB cables in each case. 

The next step was connecting the AVRISP MkII programmer’s ISP female connector to the 6 pin male header of the Hello.ISP board. But it was determinant to connect these two devices correctly in order to avoid any issue, so a decided to make a little research.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1526151403940_6pin.jpg)


With the help of the [hello.ISP.44 labeled board diagram](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png), I figured out the pin layout for the 6 pin male header in the hello.ISP.44 board. And visiting [Atmel’s AVRISP MkII Introduction page](http://www.atmel.com/webdoc/avrispmkii/avrispmkii.intro_connecting.html) at [Atmel’s documentation web site](http://www.atmel.com/webdoc/index.html), I found out the AVR ISP  connector pinout too.

Once I discovered the right orientation for hooking up the AVRISP MkII programmer’s ISP connector into the Hello.ISP.44 board’s header, I connected these to devices.

And as soon as I connected them, the light indicator of the AVR ISP MkII changed from red to green, meaning that the hello.ISP.44 board was powered.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509450280813_programming-the-hello-isp-board-07.jpg)


### **And the Makefile!?**

According the [FabISP: Programming tutorial](http://archive.fabacademy.org/archives/2017/doc/programming_FabISP.html#mac), the next step after powering the FabISP was to edit the “Makefile” document inside the “fabISP_mac.0.8.2_firmware” directory. 

Anyway,  the “Makefile” was set up to work with the AVRISP MkII by default, and as I was going to use a AVRISP MkII to program the FabISP, I did not need to change it this time. 

Finally, it was time to program the board! 


**Compiling the firmware**

The first thing I did in order to program the Hello.ISP board was to open a new Terminal window.

Then, I moved to the fabISP_mac.0.8.2_firmware directory:


    $ cd Desktop/fabISP_mac.0.8.2_firmware

And I compiled the firmware:


    $ make clean 

According to the tutorial, the response that I received from the system was the right one:


    rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex 
    main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s

Meaning that the firmware compilation had been successful.


### **Creating the hex file**

Immediately after, I used the “make hex” command in order to create the main.hex file (or output binary) that the AVR micro controller needs to run.

    $ make hex

And according the tutorial, the response received from the system was also successful.

    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -c usbdrv/usbdrv.c -o usbdrv/usbdrv.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -x assembler-with-cpp -c usbdrv/usbdrvasm.S -o usbdrv/usbdrvasm.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -c usbdrv/oddebug.c -o usbdrv/oddebug.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -c main.c -o main.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -o main.elf usbdrv/usbdrv.o usbdrv/usbdrvasm.o usbdrv/oddebug.o 
    main.o
    rm -f main.hex main.eep.hex
    avr-objcopy -j .text -j .data -O ihex main.elf main.hex
    avr-size main.hex
       text           data            bss            dec            hex        filename
          0           2020              0           2020            7e4        main.hex 

Once the firmware was compiled and the main.hex file created, it was time to set the fuses for the programmer to use the external clock (provided by the 20Mhz crystal). 


### **Setting the fuses**

In order to set the fuses in the micro controller, I typed:

    make fuse

And the response in the console was:

    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.01s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK
    
    avrdude done.  Thank you.

Meaning that the fuses had been successfully set.


### **Flashing the hello.ISP.44 board**

Finally, I programmed the board. 

It was as simple as typing:

    $ make program

The response that I received from the system was:

    avrdude -c usbtiny -p attiny44  -U flash:w:main.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: FLASH memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "main.hex"
    avrdude: writing flash (2020 bytes):
    
    Writing | ################################################## | 100% 5.68s
    
    avrdude: 2020 bytes of flash written
    avrdude: verifying flash memory against main.hex:
    avrdude: load data flash data from input file main.hex:
    avrdude: input file main.hex contains 2020 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 3.36s
    
    avrdude: verifying ...
    avrdude: 2020 bytes of flash verified
    
    avrdude: safemode: Fuses OK
    
    avrdude done.  Thank you.
    
    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK
    
    avrdude done.  Thank you.

Which seemed to be ok, so my hello.ISP.44 board had been correctly programmed.

Anyway, I needed to verify the the board was working.


### **Verifying my first FabISP**

The first thing I did to verify mi FabISP board was opening my laptop’s [system profiler.](https://en.wikipedia.org/wiki/System_Information_(Mac))

Then, I navigated to “Hardware > USB > Hub”. 

Under the USB devices three, I could see one “FabISP” listed. If the board could be recognized by my laptop, the hello.ISP.44 board had been correctly programmed.



![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509456613454_programming-the-hello-isp-board-13.jpg)



### **Setting the FabISP as a programmer**

The only thing that I had left to do in order to set the hello.ISP.44 board as a programmer, was removing the two zero Ohm resistors that I had placed in the board (SJ1 and SJ2).

So I plugged the soldering iron to the mains again! And while waiting for it to get hot, I placed the hello.ISP.44 board back in the PanaVise. 

Once the soldering iron was hot enough, I alternately heated each side of the resistor and removed the resistors with the help of some tweezers. 

After the two resistors were removed from the board, I used soldering wick to clean off the pads.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1518974172292_programming-the-hello-isp-board-14.jpg)


I could then say that my hello.ISP.44 programmer was finished, and allegedly working.

In any case, the best way to see if it was really working was using it for programming any other device. So I decided that it would be nice to make a second hello.ISP.44 board, and to use the FabISP programmer that I already owned to program it. 

As I already knew the process, it should not be hard for me to repeat it, right!?  


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1526633576325_IMG-3414.jpg)



## **Making the IDC ISP cable**

Once I had a second hello.ISP.44 board assembled and ready to be programmed, I found that I needed to make a ISP cable in order to connect the two hello.ISP.44 boards via their 6 pin headers.

After reviewing some [former Fab Academy students’ documentation](http://fab.academany.org/2018/), I understood that for making the suitable ISP cable to connect the hello.ISP.44 boards to each other, I just needed a small piece of [ribbon cable](https://www.digikey.com/products/en?keywords=MC10M-50-ND%09) and two [6 pin (2x3) crimp-on IDC connectors](https://www.digikey.com/products/en?keywords=609-2841-ND%09).


### **Gathering the parts that I needed**

Luckily, when I was checking the drawers looking for the hello.ISO.44 parts, I saw a drawer with the “2x3 FEMALE CONNECTOR” label on it. I went back to the Fab’s electronics room, and took a few 6 pin IDC connectors (including the strain reliefs).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509645217774_making-the-idc-isp-cable-01.jpg)


At Fab Lab’s electronics room, there was also a big spool of 10 way ribbon cable. I asked if there was also 6 way ribbon cable available, but the answer was NO. I was also informed that I would have to use that 10 way ribbon spool in order to get the 6 way ribbon cable that I needed.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509645232008_making-the-idc-isp-cable-02.jpg)



### **Converting a 10 way ribbon cable into a 6 way ribbon**

The truth is that converting a 10 way ribbon cable into a 6 way ribbon cable is not a big issue. It was as simple as removing 4 of the wires from the 10 way ribbon cable. And that was what I did! 

I started by cutting an approximately 20 mm piece of cable from the 10 way ribbon cable spool. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509645280439_making-the-idc-isp-cable-03.jpg)


Then, I made a small cut between the yellow and the green wires with the help of cutting pliers.

Note that I could have cut between the blue and the purple wires, but I preferred to keep the black wire at one side of the resulting 6 way flat cable, in order to use it as ground reference later.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509645477397_making-the-idc-isp-cable-04.jpg)


Once the cut was done, I separated the 4 wires that I wanted to pull apart from the other 6 wires by gently stretching both sets of wires in different directions.

The clear jacket joining the 10 wires together was thinner between wires, so pulling apart the wires happened to be very easy, since the wires separation itself was acting as a guide.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509646980158_making-the-idc-isp-cable-05.jpg)



### **Adding the IDC connectors**

First, I checked that the resulting ribbon cable had not defects.

Then, I started by passing one of the 6 way ribbon cable’s end through the IDC connector. 

And when the ribbon cable’s end was more or less aligned with the opposite side of the connector, I pressed the connector onto the ribbon cable.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509647041457_making-the-idc-isp-cable-06.jpg)


Normally, when making a regular IDC cable, we have to take into account the polarity. 

However, for this ISP cable that I was making, the polarity was not relevant. It was just important to keep in mind for later that the connection between the headers of the two hello.ISP.44 boards had to be “straight-through”, this is pin 1 to pin 1, pin 2 to pin 2, pin 3 to pin 3, pin 4 to pin 4, etc.

![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1526640717722_rect5939.png)


Punching the connector completely onto the ribbon cable by hand was impossible for me, and I did not have a proper IDC connector crimp tool on hand, so I had to find another way to crimp the connector onto the ribbon cable. 

After watching [a couple of videos online about flat ribbon cable assemblies](https://www.youtube.com/watch?v=p1yZKT3Yock), I found out that the best way to crimp the 6 pin IDC connector was using one vise.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509647041521_making-the-idc-isp-cable-07.jpg)


Once I had the two IDC connectors crimped onto the ribbon cable, I attached the strain reliefs. 

Attaching the strain reliefs was not really necessary, as the ISP cable connection was not very susceptible of being accidentally pulled off, but I preferred to complete the IDC connector set.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509647447510_making-the-idc-isp-cable-08.jpg)



### **Checking the cable connections**

In order to check the cable connections, I grabbed a [digital multimeter](https://en.wikipedia.org/wiki/Multimeter).

I selected the multimeter's continuity test mode, and I tried to reach the contacts of the IDC connectors with the multimeter’s probes. Unfortunately, reaching the contacts was not possible because the probes’ tips were not small enough.

To solve this problem, I inserted two 0.35 mm solder solder pieces in the connectors’ holes, and I checked the connections by touching the end of the solder pieces that jutted out.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509647381535_making-the-idc-isp-cable-08.jpg)


All the cable connections turned out to be fine, meaning that the IDC ISP cable was ready.

At that point, I could finally use my FabISP as a programer. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1509663361366_making-the-idc-isp-cable-10.jpg)



## **Using the FabISP as a programer**

In order to program the new Hello.ISP.44 board using the FabISP that I had previously programmed with the AVRISP MkII, I needed to replicate the previous programing process. 

As I had all the software dependencies installed, the first thing I did in order to complete this last part of the assignment was powering the boards.


### **Connecting the boards**

I started by connecting both the FabISP programmer and the hello.ISP.44 board to my laptop, using two USB-A to mini-USB cables. 

Once the two boars were being powered by the computer, I connected the them together using the IDC ISP cable that I had just made.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1518976700262_using-the-hello-isp-44-board-as-a-programer-00.jpg)


Before doing anything else, I double checked that the IDC ISP cable was connecting the headers of the two hello.ISP.44 boards correctly. 

As mentioned before, the connection between headers had to be “straight-through”,  meaning that the pin 1 of the hello.ISP.44 board acting as programmer had to be connected to the pin 1 of the hello.ISP.44 board to be programmed, the pin 2 of the hello.ISP.44 board acting as programmer had to be connected to the pin 2 of the hello.ISP.44 board to be programmed, and so on.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1519069732555_connections.jpg)


### **Editing the Makefile**

Retaking the “FabISP: Programming” tutorial, the next step after powering the FabISP  was to edit the “Makefile” document inside the “fabISP_mac.0.8.2_firmware” directory. 

As the “Makefile” was set up to work with the AVRISP MkII by default, but I was going to use a FabISP programer this time, I needed to edit it.

I opened the “Makefile” document using TextEdit, and I searched for these lines:


    #AVRDUDE = avrdude -c usbtiny -p $(DEVICE) # edit this line for your programmer
    AVRDUDE = avrdude -c avrisp2 -P usb -p $(DEVICE) # edit this line for your programmer

 

![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1518975047332_using-the-hello-isp-44-board-as-a-programer-02.jpg)


 
Once I found them, I removed the "hash" symbol (#) in front of the line “**AVRDUDE = avrdude -c usbtiny -p $(DEVICE)**” , and I added a "hash" symbol (#) to the beginning of the line with the “**AVRDUDE = avrdude -c avrisp2 -P usb -p $(DEVICE)**” to comment it out.


    AVRDUDE = avrdude -c usbtiny -p $(DEVICE) # edit this line for your programmer
    #AVRDUDE = avrdude -c avrisp2 -P usb -p $(DEVICE) # edit this line for your programmer

And I saved the “Makefile” document, overwriting the original one.


### **Programing the new hello.ISP.44**

Then, I opened a new Terminal window, and I moved to the fabISP_mac.0.8.2_firmware directory: 

    $ cd ~/Desktop/fabISP_mac.0.8.2_firmware

Once in the fabISP_mac.0.8.2_firmware directory, I compiled the firmware, created the main.hex file, set the fuses and flashed the program on the new hello.ISP.44 board, using the same commands that I had used before whit the AVRISP MkII. 

Everything went fine and the response received from the system was the right one in every step.

    Last login: Mon Nov  6 16:31:20 on ttys000
    MacBook-Pro-de-Aitor:~ aitoraloa$ cd ~/Desktop/fabISP_mac.0.8.2_firmware
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make clean
    rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make hex
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c usbdrv/usbdrv.c -o usbdrv/usbdrv.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -x assembler-with-cpp -c usbdrv/usbdrvasm.S -o usbdrv/usbdrvasm.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c usbdrv/oddebug.c -o usbdrv/oddebug.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c main.c -o main.o
    main.c:88:13: warning: always_inline function might not be inlinable [-Wattributes]
     static void delay ( void )
                 ^
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -o main.elf usbdrv/usbdrv.o usbdrv/usbdrvasm.o usbdrv/oddebug.o main.o
    rm -f main.hex main.eep.hex
    avr-objcopy -j .text -j .data -O ihex main.elf main.hex
    avr-size main.hex
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make fuse
    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FF)
    
    avrdude done.  Thank you.
    
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make program
    avrdude -c usbtiny -p attiny44  -U flash:w:main.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "main.hex"
    avrdude: writing flash (2002 bytes):
    
    Writing | ################################################## | 100% 2.03s
    
    avrdude: 2002 bytes of flash written
    avrdude: verifying flash memory against main.hex:
    avrdude: load data flash data from input file main.hex:
    avrdude: input file main.hex contains 2002 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 2.40s
    
    avrdude: verifying ...
    avrdude: 2002 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FF)
    
    avrdude done.  Thank you.
    
    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FF)
    
    avrdude done.  Thank you.
    
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ 


### **Verifying my new FabISP**

Finally, I verified that the new FabISP was working correctly

The two FabISP still connected to my laptop, so I just opened the System Profiler again, and I could see two “FabISP” listed under the USB devices three. 

The newly flashed hello.ISP.44 board had been correctly programmed.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1526246305918_Captura+de+pantalla+2018-05-13+a+las+23.17.09+2.png)



## **Completing the group project** 

[Electronics production](http://academy.cba.mit.edu/classes/electronics_production/index.html) assignment also included a group project. 

This group project was both to describe the PCB production process and to check that everything in the mini CNC machines was working well. In order to do that, we needed to mill a test pattern.

In my particular case, I had already completed the whole PCB production process before, so this part of the assignment was mainly useful for checking that the mini CNC machines in the lab were still in good condition, and for helping my group members learn how the PCB production process was.

We started by downloading the “trace-width” test files ([traces](http://academy.cba.mit.edu/classes/electronics_production/linetest.png) and [interior](http://academy.cba.mit.edu/classes/electronics_production/linetest.interior.png)) available at the [Electronics Production assignment page](http://academy.cba.mit.edu/classes/electronics_production/index.html).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1523207663139_linetest.png)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1523207662768_linetest.interior.png)


Then, we used [Fab Modules](http://fabmodules.org/) for setting the milling files, having into account that we would mill the PCB traces using the 1/64 milling bit, and the PCB outline using the 1/32 milling bit.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1523208431596_Captura+de+pantalla+2018-04-08+a+las+19.24.54.png)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1523208431573_Captura+de+pantalla+2018-04-08+a+las+19.26.29.png)


Once the milling files were ready, and the circuit board stock properly attached to the mini CNC machine’s bed, we launched the milling jobs using the VPanel software.

A few moments later the test file PCB was done… and the outcome was quite nice!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_56C236B37D587AD276A6BC9F4DA940B88951E3C2CFDE4917FB4AE018BA92ECAC_1523210384267_IMG-2948.png)



## **What I have learned**

I have learned what is a programer and how to make one.

While making the FabISP, I have learned:

how to use fab modules

how to prepare RML files with fabmodules

how to use a desktop milling machine

machining PCBs with a desktop milling machine

techniques to solder SMD components

programing a board



## **Issues that I had**

the Roland MDX-20 was not working

the mini CNC machine’s sacrificial board was not flat

crimping the IDC connectors

testing the IDC cable


## **Tools used** 

- **For preparing the RML files:**
    - MacOS High Sierra v10.13.3
    - Fab modules


- **For milling the Hello.ISP board:**
    - Windows 7 Ultimate Service Pack 1
    - Roland VPanel for SRM-20
    - Roland SRM-20
    - Hex key
    - FR1 PCBs
    - 1/64” SE 2FL milling bit 
    - 1/32” SE 2FL milling bit
    - Double-sided tape
    - Spatula
    - Paper towels
    - Rubbing alcohol
    - Steel wool


- **For assembling the Hello.ISP board:**
    - Soldering iron with a 0.5mm conical fine-point tip
    - Soldering iron stand with sponge
    - Distilled water
    - 0.35 mm soldering wire
    - Desoldering wick
    - Tweezers
    - Vacuum base PanaVise PCB holder
    - Solder fume extractor
    - Magnifying glass


- **For programming the Hello.ISP board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - CrossPack-AVR v20131216
    - Xcode v9.2
    - Terminal v2.8.2
    - Atmel AVRISP MkII
    - USB-A to mini USB cable
    - USB-A to USB-B cable


- **For making the IDC ISP cable:**
    - 10 wires ribbon cable
    - 6 pin female crimp-on IDC connectors.
    - Scissors
    - Wire-cutter
    - PanaVise
    - Multimeter
    - 0.35 mm soldering wire


## **Files to download**
