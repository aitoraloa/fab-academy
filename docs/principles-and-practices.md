# Principles and Practices

As assignment for the first week of the Fab Academy we just had to propose and communicate a concept for our final project.

## **Motivation**

There were many things that I wanted to make as Fab Academy project that it was hard for me to decide which one I liked best.

In any case, I had it clear that I wanted to make something that I could use at a regular basis. And at that time I really needed help cleaning my vinyl record collection.

I have a big vinyl record collection (~1000), so cleaning all these records by hand would be a lot of work. Having a cleaning machine would be really helpful!

## **Concept**
When it comes to cleaning vinyl records there are many options. You can clean them by hand or you can use a vinyl record cleaning device/machine.

8 easy and affordable ways to clean your vinyl records-by-hand

The 8 best record cleaning machines for the true vinyl connossieur

The bad news is that the vinyl record machines available in the market are quite expensive. And unfortunately, the machines with the best vinyl record cleaning technology (which is ultrasonic cleaning) are the most expensive of all.

Anyway the ultrasonic bath machines are not so expensive, so it could be great to make an adaptor to clean Vinyl records in one of these machines.

## **References**

### Vacuum > Manual

Revinylizer

The Vinyl Vac

Squeaky Clean Vinyl

Record Doctor

### **Vacuum > Automated**

Vinyl Cleaner VC-S

Music Hall WCS-2

MKII Moth

Okki Nokki

nittygritty

VPU Industries HW 16.5

VPU Industries MW1 Cyclone

Sota LPC

Loricraft Audio PRC

### **Ultrasonic**

Degritter

Klaudio's KD-CLN-LP200

Vinyl Cleaner Pro

iSonic

### **Third Way**

Groove Clean

Cleaner Vinyl

### **Notes**

Upgrade one of the existing devices

Adaptable to any ultrasonic bath -> clamp

Add speed control

Add timer

Add buzzer alarm

3D printed body

## **Sketch**

TBC