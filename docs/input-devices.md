# Input Devices

Individual assignment: measure something: add a sensor to a microcontroller board that you have designed and read it

Group assignment: measure the analog levels and digital signals in an input device


## **Which sensor board to make!?**

From the very beginning, my plan was to complete all the boards showed during the lecture. 

I had enjoyed making the FabISB and the Echo Hello-World boards during the previous assinments, and I really wanted to keep on practicing the whole board manufacturing and programming processes.

Anyway, I though that the right way to do it was to start from the very beginning. And the first sensor board in the input devices list was the hello.button.45 board.

Thus, I downloaded all the images (PNG) and programming (TXT) files available for the hello.button.45 board in the the Fab Academy’s Input Devices page.

![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527333846257_hello.button.45.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527333858126_hello.button.45.traces.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527333872481_hello.button.45.interior.png)



## **Redrawing the hello.button.45 board**

Repeated all the steps learned while Redrawing the Hello Echo-World board

Created a new project:

EAGLE → Projects > eagle > Inputs > hello.button.45

Designed the hello.button.45 schematic → components added from fab library


- 1x ATTINY45SI (Microcontroller)
- 1x FTDI-SMD-HEADER (FTDI header)
- 1x AVRISPSMD (ISP header)
- 1x 6MM_SWITCH6MM_SWITCH (push-button)
- 1x CAP-UAS1206FAB (Capacitor)
- 2x RES-US1206FAB (Resistor)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499073843_Captura+de+pantalla+2018-05-26+a+las+19.13.48.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499073853_Captura+de+pantalla+2018-05-26+a+las+19.14.24.png)


Laid out the board


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499113630_Captura+de+pantalla+2018-05-26+a+las+19.16.04.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499137694_Captura+de+pantalla+2018-05-26+a+las+19.44.58.png)


Checked the design rules → Minimum clearance between objects in signal layers = 16mil

Everything ok!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499177638_Captura+de+pantalla+2018-05-26+a+las+19.45.33.png)


Exported the hello.button.45 board

In the “Visible Layers” dialog Window, hide all layers except “Top” and “Dimension”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499243495_Captura+de+pantalla+2018-05-26+a+las+19.46.00.png)


File > Export > Image 

In the “Export Image” dialog window:

Monochrome
Resolution = 500 dpi
rea = Full


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499272072_hello.button.45.png)


Used Gimp for creating a white border around the image:

opened the exported PNG file
selected the PCB using the inner white line
“Image > “Crop to selection”
“Image > canvas size”

  Width = +20 
  Height = +20 
  Under “Offset”, click “Center” button

Clicked “Resize” button
Created a new layer with white background and place in under the traces layer
“Layer > Merge down”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499324709_hello.button.45_traces.png)


And I also used Gimp to the outline cut PNG file

selected the traces
“Edit > Fill with BG color” or “Edit > Fill with FG color”, depending which one was black
“Colors > Invert”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527499356331_hello.button.45_outline.png)



## **Making the hello.button.45 board**

### **Milling the hello.button.45 board**

Fab Modules
Roland SRM-20 Desktop Milling Machine
Roland VPanel for SRM-20
one sided FR1 PCB
1/64” milling bit for the traces
1/32” milling bit for the outline


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527501511455_IMG-3475.jpg)


### **Assembling the hello.button.45 board**

Identified and collected the electronic components


- 1x AVR ATTiny 45 Microcontroller IC 8-Bit 10MHz 4KB (2K x 16) FLASH 8-SOIC
- 1x Tactile Switch SPST-NO Top Actuated Surface Mount
- 1x 6 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x 6 Positions Header Breakaway Connector 0.100" (2.54mm) Surface Mount, Right Angle
- 1x 1µF ±10% 50V Ceramic Capacitor X7R 1206 SMD
- 1x 10k Ohm 1/4W 1% Resistor 1206 SMD


- ATTiny 45 microcontroller → [Digi-key Part Number: ATTINY45V-10SU-ND](https://www.digikey.es/products/es?keywords=ATTINY45V-10SU-ND%09)
- Tactile switch → [Digi-key Part Number: SW262CT-ND](https://www.digikey.com/products/en?keywords=SW262CT-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 6 pin FTDI header → [Digi-key Part Number: S1143E-36-ND](https://www.digikey.com/products/en?keywords=S1143E-36-ND)
- 1uF capacitor → [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527501567808_IMG-3477.jpg)


Soldered the components to the board


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527501631423_IMG-3478.jpg)



## **Understanding the C code**

tbc


## **Programming the hello.button.45 board** 

Created a new directory

Copied the hello.button.45.c and hello.button.45.make files in the directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527588781554_Captura+de+pantalla+2018-05-29+a+las+12.12.36+2.png)


Connected the hello.button.45 to the computer using a FTDI cable

Connected the FabISP to the computer using a miniUSB-A to USB-B cable

Connected the hello.button.45 and the FabISP using the IDC cable


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527504935186_IMG-3496.jpg)


Checked that everything was ok using the system profiler


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527590071238_Captura+de+pantalla+2018-05-29+a+las+12.33.24.png)


Opened terminal and navigated to the directory where the hello.button.45.c and hello.button.45.make files had been saved

Typed


    make -f hello.button.45.make

Terminal response


    avr-gcc -mmcu=attiny45 -Wall -Os -DF_CPU=8000000 -I./ -o hello.button.45.out hello.button.45.c
    avr-objcopy -O ihex hello.button.45.out hello.button.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.button.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     364 bytes (8.9% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)

Meaning that the  “hello.button.45.c.hex” and  “hello.button.45.out” files had been created


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527593579935_Captura+de+pantalla+2018-05-29+a+las+13.21.21+2.png)


Then, I typed:

    make -f hello.button.45.make program-usbtiny

And the console prompted:


    avr-objcopy -O ihex hello.button.45.out hello.button.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.button.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     364 bytes (8.9% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)
    
    
    avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.button.45.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9206
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.button.45.c.hex"
    avrdude: input file hello.button.45.c.hex auto detected as Intel Hex
    avrdude: writing flash (364 bytes):
    
    Writing | ################################################## | 100% 0.38s
    
    avrdude: 364 bytes of flash written
    avrdude: verifying flash memory against hello.button.45.c.hex:
    avrdude: load data flash data from input file hello.button.45.c.hex:
    avrdude: input file hello.button.45.c.hex auto detected as Intel Hex
    avrdude: input file hello.button.45.c.hex contains 364 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 0.45s
    
    avrdude: verifying ...
    avrdude: 364 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:62)
    
    avrdude done.  Thank you.

Meaning that hello.button.45 board had been programed.


## **Testing the hello.button.45 board**

Used terminal to navigate to the folder were the program term.py program was located

Checked the USB port were the board was connected.

I typed:


    $ ls /dev/cu.*

wich prompted:


    /dev/tty.Bluetooth-Incoming-Port /dev/tty.usbserial-FTEZU7NB

Meaning that the hello.button.45 board was connected to /dev/tty.usbserial-FTEZU7NB.


Then, I typed:


    $ python term.py  /dev/tty.usbserial-FTEZU7NB 9600

As soon as I hit enter, a new dialog window called “term.py” opened:


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527503695816_Captura+de+pantalla+2018-05-28+a+las+12.33.45.png)


And every time I clicked the switch button in the hello.button.45 board the letters “d” and “u” appeared in the “term.py” window.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_ACF65A3B68336EBD5C00B28910FD938174625A4274E55CE5AEA8DD3E7D0A3FD6_1527503793103_Captura+de+pantalla+2018-05-28+a+las+12.33.58.png)


Meaning that the hello.button.45 board was working well.



## **Completing the group project**


measure the analog levels and digital signals in an input device



## **What I have learned**




## **Issues that I had**

Understanding the C code


## **Tools used**


- **For redrawing the board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - EAGLE v8.0.2


- **For preparing the milling files**
    - MacBook Pro with macOS High Sierra v10.13.3
    - GIMP v2.8.22


- **For making the board:**
    - Milling 
        - Windows 7 Ultimate Service Pack 1
        - Roland VPanel for SRM-20
        - Roland SRM-20
        - Hex key
        - FR1 PCBs
        - 1/64” SE 2FL milling bit 
        - 1/32” SE 2FL milling bit
        - Double-sided tape
        - Spatula
        - Paper towels
        - Rubbing alcohol
        - Steel wool
    - Assembling
        - Soldering iron with a 0.5mm conical fine-point tip
        - Soldering iron stand with sponge
        - Distilled water
        - 0.35 mm soldering wire
        - Desoldering wick
        - Tweezers
        - Vacuum base PanaVise PCB holder
        - Solder fume extractor
        - Magnifying glass


- **For programming the board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - Terminal v2.8.2 (404)
    - FabISP programmer
    - USB-A to mini USB cable
    - FTDI Serial TTL-232 USB cable
    - 6 wire IDC ribbon cable


- **For testing the board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - Python v2.7.10
    - term.py program
    - Multimeter
    - Osciloscope



## **Files to download**