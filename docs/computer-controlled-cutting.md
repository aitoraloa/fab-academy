# Computer-Controlled Cutting
Unlike the two first Fab Academy assignments, the [Computer-controlled cutting](http://academy.cba.mit.edu/classes/computer_cutting/index.html) week’s assignment included a group project. 

The aim of this group project was testing the [laser-cutters](https://en.wikipedia.org/wiki/Laser_cutting) with the materials available, and coming up with the right cutting settings for each machine/material used. This group project also implied playing around with [the different types of joints presented during this week’s lecture](http://academy.cba.mit.edu/classes/computer_cutting/joints.jpg).


## **Planning the group project**

As I was not present at Fab Lab Barcelona the week in which this assignment took place, I had to search for these laser-cutting settings on my own. Luckily, I had access to the Fab Lab’s laser cutting machines the first two weeks of August, when there was almost nobody using them except me. This turned out to be very good opportunity to learn how to use them, and find the right settings for them by making all kinds of laser cutting tests.

Furthermore, at that time Fab Lab Barcelona was filled with leftovers of materials that were susceptible of being [engraved](https://en.wikipedia.org/wiki/Engraving) or cut with a laser-cutter: [PMMA](https://en.wikipedia.org/wiki/Poly(methyl_methacrylate)) of different colors and thicknesses; [cardboard](https://en.wikipedia.org/wiki/Cardboard) and [cork](https://en.wikipedia.org/wiki/Cork_(material)); various types of laminates; [leather](https://en.wikipedia.org/wiki/Leather) and other [textiles](https://en.wikipedia.org/wiki/Textile). 

Using some of these leftovers available seemed to me the best idea!

In any case, as I was going to use leftovers, it was very important to make sure before choosing one material over the others that there was going to be enough leftovers of that particular material as to complete all the laser-cutting exercises that I wanted to perform. Using leftovers might be highly satisfying unless you reach a point where there still things to be tested or fabricated, and you do not have enough material left.

Trying to make the testing process as challenging as possible, I decided that I was going to use three types of materials: 3 mm balsa wood, 3 mm PMMA, and 4 mm (one layer) cardboard. 

Obviously, I took this decision after having clear that there were enough leftovers of these 3 materials in the lab as to make all the tests, and as to complete the press-fit construction kit that I would have to make later for the individual assignment.


![~ 600x400 mm pieces of the 3 materials selected for the laser cutting and engraving exercises.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503419638025_making-laser-cutting-test-parts-01.jpg)


## **Designing the laser-cutting test parts**

Once I had all the materials selected, it was time to start designing the test parts that I was going to use to find the right laser cutting and engraving settings for each machine and material. 

My initial idea was to design test parts that would help me out to determine the laser kerf of the laser cutting machine, and finding out the right size of the slots for a perfect press-fit joint. 

Anyway, understanding the difference between vector and raster engraving, practicing with halftones, or learning as much as possible about living-hinges was also on my plans, so it was pretty clear that I was going to be designing quite some time.


### **Which CAD software to use!?**

Deciding which software to use was a important decision. 

The available options were many, as virtually any CAD software could be used for designing the test parts. Anyway, taking into account that the press-fit construction kit to be designed later had to be [parametric](https://en.wikipedia.org/wiki/Parametric), I thought that using a parametric CAD software would be the right choice.

On the other hand, I already knew that [Rhinoceros](https://www.rhino3d.com/en/) was the CAD software installed in the computers connected to the laser cutting machines at Fab Lab Barcelona. And I also knew that, although Rhinoceros was not a parametric program by itself, it could be used with [Grasshopper](http://www.grasshopper3d.com/) if parametric capabilities were required. 

In any case, I was also quite interested in trying any other of the parametric CAD softwares presented during the lecture, especially [OpenSCAD](http://www.openscad.org/) or [Fusion360](https://www.autodesk.com/products/fusion-360/overview). Exporting the files created with any of the aforementioned parametric software programs so they could be opened with Rhinoceros later for sending them to the laser cutter was also a possibility.  

Finally, as there were many test parts to design and I was much more familiarized with Rhino (although I had not used it for a long time) than with OpenSCAD or Fusion360, I though that it would be much more convenient to complete the laser-cutting test parts design stage using Rhinoceros.


### **The Laser kerf test part**

After watching the lecture for the second time, I understood that the initial point when working with any laser-cutting machine for the first time is to find out the laser kerf. 

The laser kerf is the portion of material that the laser burns away when it cuts through.

Laser kerf is determined by material properties and thickness. But other factors also have an impact on how much the laser takes away. The focal length of the lens, the cutting power, the cutting speed and/or the cutting frequency could determine the ranges of kerf for each material. 


![Schematic diagram showing what is laser kerf](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505340671152_drawing.jpg)


Kerf widths can vary even on the same material sheet, whether cutting a straight line or a curve line, or from laser cutting in the X or Y dimension. Even the manufacturing tolerance of the material can also impact the laser kerf. 

When cutting parts on a laser machine to produce accurate cut parts, with final dimensions as close as possible to the programmed shape, the actual path has to be compensated.

Testing the laser kerf of a laser-cutter could be as simple as cutting a 100 x 100 mm square and measuring the resulting part with a digital caliper. The resulting part could be a 99 x 99 mm. square, meaning that the laser kerf is 0.5 mm.

In any case, after exploring the web for more information on laser kerf, I discovered a few interesting methods for finding out the laser kerf of any laser-cutting machine. Among all these methods, there were also different laser kerf test patterns. The laser kerf test pattern that I decided to replicate was even [available for download](https://cdn.instructables.com/ORIG/F10/RIEU/HNY22JJ0/F10RIEUHNY22JJ0.ai), but I thought that making a new design from scratch would be quite interesting in order to practice with Rhinoceros. 

Accordingly, I opened a new file in Rhinoceros, and I used the `Rectangle`, `Polyline`, `TextObject` and `Hatch` commands to design a custom laser kerf test pattern. 

The test pattern that I drawn was basically a rectangle divided in 9 blocks. After cutting the blocks, these could be pushed to one side in order to measure the gap created on the other side with a digital caliper. Then dividing that gap by the 10 cuts would let me know the laser kerf. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1518813317912_Captura+de+pantalla+2018-02-16+a+las+21.33.51.png)


Another useful information that I had at that time, was that the different processes of a laser-cutting job (engraving vs cutting) should be separated by color in order to differentiate them. 

Based on that information, I also modified the document’s layers set up, leaving just three layer that I named “Engrave”, “Cut_01” and “Cut_02” . Then, I colored the objects of the “Engrave” layer in black, the objects of the “Cut_01” layer in red and the objects of the “Cut_02” layer in blue, using Rhinocero’s layer edition capabilities.


![Custom replica of one laser kerf test parts found online made in Rhinoceros.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504254848105_image.png)


Note that I used two “Cut” layers because I wanted that the outline of the kerf test part to be cut at the end of the process. Adding a second “Cut“ layer would allow me to cut all the inner curves in RED first, leaving the outlines in BLUE for the end of the cutting stage.

Once the laser kerf test pattern design was finished, I moved on to the next test part design.


### **The press-fit joint test**

In theory, a [press-fit joint](https://en.wikipedia.org/wiki/Interference_fit) (also known as interference fit or friction fit) is a [fastening](https://en.wikipedia.org/wiki/Fastener) between two parts which is achieved by [friction](https://en.wikipedia.org/wiki/Friction) after the parts are pushed together, rather than by any other means of fastening.

In practice, I needed to design a test part to find out the right slot size to achieve this perfect friction joint with the three different materials that I had chosen. 

As with the laser kerf test part, I found many examples of press-fit test parts online. Most of these shared by former Fab Academy students and available in the [Fab Academy archives](http://archive.fabacademy.org/). Anyway, I was still eager to keep on training myself with Rhino, so I decided to design my own test parts again. 

As I had already decided to use 3 mm thick balsa and acrylic, and 4 mm thick cardboard, I needed two different designs of press-fit test parts. Using materials with different thicknesses forced me to design one test part for the balsa and the acrylic, and second test part for the cardboard.

So I opened a new file in Rhinoceros, and I drawn a rectangle with several slots in one of the long sides, making the slot in the middle to be 3 mm thick (as the balsa and the acrylic).

Then, I placed five more slots in both sides of this pivotal slot.  And I consecutively varied up and down the sizes of these slots, +0.05 mm to the right and -0.05 to the left. 

And finally, I used the `Chamfer` command in the corners on the outside of the slots. 


![Custom press-fit test part for 3 mm thick materials made in Rhinoceros](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503499892760_making-laser-cutting-test-parts-04.jpg)


As with the previous test part, I modified the layers set up, leaving only two layers. “Engrave” colored in black and “Cut” colored in red. And I also ensured that the different objects of the drawing were correctly placed in their corresponding layers. 

The press-fit test for the 3 mm materials was finally ready! 

Next, I duplicated the document of the “press-fit test for 3 mm thick material”, and I varied the size of all the slots. I changed the size of the slot in the middle to 4 mm (which was thickness of the cardboard). And I also modified the sizes of the slots on both sides of the 4 mm pivotal slot. But this time, I consecutively made the slot size +0.1 mm to the right and -0.1 to the left.

As far as I knew at that point (and was confirmed later), the cardboard’s laser kerf tends to be bigger than the laser kerf of the balsa wood or the acrylic, so the tolerances should be also a bit bigger when using cardboard. 0.1 mm (for cardboard) vs 0.05 mm (for acrylic and balsa wood).


![Custom press-fit test part for 4 mm thick materials made in Rhinoceros](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503500722686_making-laser-cutting-test-parts-05.jpg)


The two press-fit test that I needed were ready! 

At that point, I though that making these designs parametric would be a great idea. Especially if they had to be changed later. Anyway, I needed to learn Grasshopper first, and I was eager to see how these tests looked in real life, so I decided that for the moment I would move on with the other designs that I had to do.


### **The engraving test part**

Laser engraving is the practice of using lasers to engrave an object. And relying on the information gathered about laser engraving, I understood that it was necessary to make tests, tests, test… and more test, in order to mastering laser engraving. 

I also found out that there was a good amount of laser engraving test images available online, so I picked up ideas here and there, and completed my custom laser engraving test part.

On a new Rhinoceros document, I drawn a greyscale wheel with nine-teen sectors that ranged from 100% black to 5% black. The main difference from the other test part designed before was that I had to create different layers in order to apply the different shades of gray to the objects, so the resulting document had 20 layers.  I was not sure if other options for this action were available on Rhinoceros… maybe yes! But, at that time, this was the best way I found to do it.


![Custom vector formed engraving test part made in Rhinoceros](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503568099616_making-laser-cutting-test-parts-06.jpg)


In theory, the darker the color, the stronger the engraving should be. So I hoped this test would allow me to understand how the laser cutter behaves when different shades of grey are present.

On the other hand, this engraving test part was made entirely from vectors, but I also wanted to see how different it would be sending the same greyscale wheel but using an image (raster) instead of only vectors. 

In order to get an image of the greyscale wheel, I temporally removed the grid and I changed the Rhinoceros background to white. Then, I screen-captured the design and created a new file in Rhinoceros, where I placed the captured image by using the `PictureFrame` command.


![Custom JPG based engraving test part made in Rhinoceros](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503568644543_making-laser-cutting-test-parts-06_01.jpg)


To be honest, I was not sure if there would be any difference at all. In theory, all the fills in the vector-formed test part should be consider as an engraving job, even if you sent the laser job as “a vector job”. Probably both test parts would give a similar result but… I would see that later! 


### **The halftone test part**

Continuing with the laser engraving tests, I though that it would be quite interesting to see how does it work to laser engrave a halftone image. 

As seen in the wikipedia, a [halftone](https://en.wikipedia.org/wiki/Halftone) is the reprographic technique that simulates continuous tone imagery through the use of dots, varying either in size or in spacing, thus generating a gradient-like effect.

The internet is full of beatiful halftone images, but I still wanted to make it more personal, so I used a picture of one of my cats for this test part.


![Color picture of one of my beloved cats.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503509758934_mandu.jpg)


Converting an image to halftones is something that could be done with Inkscape, Gimp, or any other image manipulation programs. Besides, the halftone conversion process seemed to be a pretty simple operation with all of them. In any case, I decided to use Gimp.

So, I started by launching Gimp, and I opening the JPG file containing my cat’s picture. 

Then, I converted the imported color image to grayscale by simply choosing “Image” in the main menu, and selecting “Mode > Greyscale” from the drop-down menu.


![The picture of my cat converted to a grayscale image using Gimp.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503507818831_making-laser-cutting-test-parts-07.jpg)


Once the image was converted to grayscale, I just needed to apply a filter, so I clicked “Filters” on the main menu, and I selected “Distortions > Newsprint” from the drop-down menu. 

The “Newsprint” dialog window appeared, and I played around with the parameters available there in order to find the best result. Then, I clicked “OK”.

Note that I had to play around for a few minutes with the parameters available in the “Newsprint” dialog window until I understood how they affected the image. After several attempts, I reached somewhere closed to what I was looking for.


![The grayscale image of my cat converted to halftone using Gimp's "Newsprint" feature.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503509353760_making-laser-cutting-test-parts-08.jpg)


Probably this picture of my cat was not the best image that I could have chosen. An image with white/clear background and higher contrast would have been a much better option. 

Anyway, I saved the image as a PNG file, and I inserted this PNG image in a new Rhinoceros document using the `PictureFrame` command. 

Then, I modified the image size, and I made myself sure that the layers were correctly arranged. 

Finally, I save the file as 3DM.


![Halftone test part made in Rhinoceros using the halftone image prepared with Gimp.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503511530415_making-laser-cutting-test-parts-09.jpg)


### **The living hinges**

As seen in the wikipedia, a [living hinge](https://en.wikipedia.org/wiki/Living_hinge) is a thin flexible hinge made from the same material as the two rigid pieces it connects. It is typically thinned or cut to allow the rigid pieces to bend along the line of the hinge. 

As in the preceding cases, I was able to find several examples of [living hinge patterns online](https://www.google.es/search?q=living+hinge+patterns&safe=off&rls=com.microsoft:es:%7Breferrer:source%3F%7D&rlz=1I7ACAW_esES322&dcr=0&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjxiLXZ1KraAhVK3KQKHUdQDXsQ_AUICigB&biw=1920&bih=1035). 

As at this point I really wanted to start laser cutting, instead of designing my own living hinges (which would have taken me a while), I tried to collect as many examples as possible, and I prepared al these examples with Rhinoceros.


![Super flexible plywood living-hinge test part prepared with Rhinoceros.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503590776975_making-laser-cutting-test-parts-10.jpg)


Preparing these files with Rhinoceros was not quick either. 

Most of them were only available in DXF or PDF file formats, and when I opened them with Rhinoceros I encountered some strange things (open curves, exploded text). 

All this bugs had to be fixed before sending the jobs to the laser-cutting machine, meaning that I had to expend also quite some time doing these preparations. 


![Living hinges pattern collection prepared with Rhinoceros.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503590534044_making-laser-cutting-test-parts-11.jpg)


On the other hand, I arranged the objects in the layers of the document so the outlines were the last vectors to be cut by adding a second cut layer (Cut 02), and coloring it in blue. 


![Another living hinges pattern collection prepared with Rhinoceros.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503591051651_making-laser-cutting-test-parts-12.jpg)


As with the laser kerf test part, this would allow me later to cut the inner red curves first. And the blue outlines would be cut at the end of the process.


![Lattice hinge test parts for 3 mm acrylic prepared with Rhinoceros.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503590534001_making-laser-cutting-test-parts-13.jpg)


At this point, I had all the files for the test parts ready. And despite I assumed that some changes could be needed later… it was time to move on to the machines! 

But before that, I uploaded all these files I made to my student folder inside [IAAC’s cloud](https://cloud.iaac.net:5001/index.cgi), as this would allow me to access them directly from the computers connected to the machines, instead of using a USB drive (which is always less productive, IMHO).


## **Reviewing the laser-cutters** 

The test files that I had prepared could be used in any of the laser-cutting machines available at Fab Lab Barcelona, but nevertheless I though that it would worth the time making a quick review of these machines before choosing which one to use.

By the time I was writing this, there were 4 different laser-cutting machines at Fab Lab Barcelona:

### [**Speedy 100**](https://www.troteclaser.com/en/laser-machines/laser-engravers-speedy-series/) **by** [**Trotec**](https://www.troteclaser.com/)

![Trotec Speedy 100R laser cutting and engraving machine available at Fab Lab Barcelona](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503593550606_reviewing-the-laser-cutters-available-01.JPG)


This was the smallest of the laser-cutters available.

It was a 30 watt compact CO2 laser-cutting and engraving machine for entry level users with a working area of 610 x 305 mm, and a maximum workpiece height of 170 mm. 

In order to make it work, it had to be connected via USB to a Windows OS laptop with the printer drivers, Rhinoceros design software, and [Trotec’s dedicated JobControl laser software](https://www.troteclaser.com/en/laser-machines/laser-software/jobcontrol/) installed. 

It could be used to engrave acrylic, glass, laminates, leather, paper, plastic, stone, textiles, or wood. But it could only cut acrylic, laminates, leather, paper, plastic, textiles, or wood.


### [**Speedy 400**](https://www.troteclaser.com/en/laser-machines/laser-engravers-speedy-series/) **by** [**Trotec**](https://www.troteclaser.com/)

![Trotec Speedy 400 laser cutting and engraving machine available at Fab Lab Barcelona](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503593611007_reviewing-the-laser-cutters-available-02.JPG)


I guess I could say that this machine was the “big brother” of the Speedy 100.

It was a 120 watt high productivity CO2 laser-cutter with a working area of 1000 x 610 mm, and a maximum height of workpiece of 305 mm. Noteworthy, this machine’s maximum processing speed was 3.55 m/sec with an acceleration of up to 5g. That’s crazy! 

Unlike the speedy 100, this machine was permanently connected via USB to a Windows OS desktop computer with the corresponding printer drivers, the Rhinoceros design software, and the JobControl laser software installed on it.  

As its smaller companion in the Speedy series, it could engrave acrylic, glass, laminates, leather, paper, plastic, stone, textiles, or wood. But it could only cut acrylic, laminates, leather, paper, plastic, textiles, or wood.


### [**Legend 36EXT**](https://www.epiloglaser.com/products/l36ext-techspecs.htm) **by** [**Epilog**](https://www.epiloglaser.com/index.htm)

![Epilog Legend 36EXT laser cutting and engraving machine available at Fab Lab Barcelona](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503593638223_reviewing-the-laser-cutters-available-03.JPG)


This “legendary” machine was a 75 watt air-cooled CO2 laser-cutter with a working area of 914 x 610 mm, and a maximum height of workpiece of 305 mm. 

It was also permanently connected via USB to a Windows OS desktop computer with the corresponding printer drivers installed on it. But unlike the Trotec laser cutters, the laser jobs were directly sent from Rhinoceros (via the Epilog laser driver interface) and saved in a memory buffer. So after sending the jobs from the computer, it was necessary to launch them using the control panel in the machine.

Wood, Acrylic, Fabrics, Delrin, Cloth, Leather, Matboard, Melamine, Paper, Mylar, Pressboard, Rubber, Wood veneer, Fiberglas, Plastic, Cork or Corian were among the materials that could be laser-cut with this machine. And if that was not cool enough, it could also laser engrave all of the aforementioned materials, plus Glass, Ceramic, Marble, Coated and Painted metals, Anodized aluminum, Stainless steel, Brass and Titanium. OMG! 


### **2000 Series Laser by** [**MultiCam**](http://multicam.ca/)

![MultiCam 2000 series laser cutting and engraving machine available at Fab Lab Barcelona](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1503593832782_reviewing-the-laser-cutters-available-04.JPG)


This was the biggest laser-cutting machine available at Fab Lab Barcelona. A 400 watt CO2 workhorse with a working area of 1500mm x 3000mm, and a maximum height of workpiece of 110 mm.

It was permanently connected via USB to a dedicated Windows OS desktop computer. But, in contrast to the PC used with the Epilog or the Trotec, this desktop computer had EnRoute CAD/CAM software installed. So after importing previously designed DXF files, the cutting files could be prepared and sent to a particular files folder using EnRoute. Then, using the machine’s remote control, the cutting settings could be set and the jobs launched.

It was perfect for cutting and/or engraving a wide variety of materials including thick plastic sheet, different types of wood such as MDF or Plywood, leather, fabric, etc. And it also could cut light gauge ferrous metals (up to 8 mm).  

Unfortunately, both the Multicam 2000 and the Epilog XT Legend 36 were out of service at the moment I was completing this assignment. And the access of the Trotec Speedy 100 was restricted to few staff personnel. So my only option was using the Trotec Speedy 400! 


## **Laser-cutting with the Trotec Speedy 400**

Once I knew which materials I was going to use, and which machine was available for completing this assignment, it was time to get to know all these elements better. 

Regarding the materials, there was not to much to find out. All of them were commonly used materials for laser-cutting operations, and their characteristics should not be an issue for any decent laser cutter. Far less for a 120 watt laser-cutting machine as the Trotec Speedy 400. 

However, I had never used a Trotec laser cutter before, so the first stop for me was downloading and thoughtfully reading both the [Trotec Speedy 400 Operation Manual](https://www.troteclaser.com/fileadmin/content/images/Contact_Support/Manuals/Speedy-400-Manual-EN.pdf) and the [Trotec JobControl Operation Manual](https://www.troteclaser.com/fileadmin/content/images/Contact_Support/Manuals/JobControl-Manual-EN.pdf). It was not a quick task, as they were almost 200 pages of very detailed information, including all aspects of the machine and the software set up, but it happened to be a very revealing lecture at the end… so it was worth the effort!

After entirely reading both manuals, I had a general idea about how to operate this laser-cutter and manage the JobControl application in order to launch any laser job. In addition, it was also quite clear to me how the Trotec laser cutter workflow was. 

The Trotec driver installed in the computer created a printing job directly from the graphics program (containing the graphic data and other relevant information such layers color, engraving/cutting job, size, resolution, etc). And this printing job was automatically accessed by the JobControl Manager Software later in order to manage, precise position, transfer, launch and continuously control the job.


![Schematic diagram showing the workflow with a Trotec laser cutter](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505343674203_drawing2.png)


At this point, I already had the test files prepared, and I felt confident enough to get my hands on the Speedy 400 laser cutter, so I did not wait a second longer. It was time to start putting into practice all the theoretical knowledge acquired until then!

### **Cutting the kerf test parts (and learning how to work with the laser cutter)**

As mentioned before, I was already convinced that the starting point when working with a laser-cutter for the first time would be finding out the laser kerf. And this was the first operation that I performed! At the same time, this laser kerf exploration would also help me to learn how to really use both the laser-cutter and the laser-cutter’s management software.

I guess it is importatn to mention that any laser job requires preparation on the machine side, and on the management software side. On the machine side, the material has to be placed, the laser focused, and the origin point selected. And on the management software side, the files need to be created, the settings configured, etc. 

Although I had also seen people waiting until the very last moment (just before launching the laser job) to complete the preparation of the material in the machine, it seemed to me much more appropriate completing the machine preparation side first, and then concentrating on the management software side. 

### **Preparing the material in the machine**

Based on my previous comments, the first thing I did was getting closer to the machine with the sheet of balsa wood that I had reserved. As nobody was using it before me, it was switched off, meaning that had to switch the laser-cutter on! 

With the machine’s lid closed (otherwise the work table would not reference), I rotated the ON/OFF key switch from the “O” position to the “I” position first. And then, I rotated the ON/OFF key switch again so it momentary reached the “On” position. 


![ON/OFF switch and emergency stop button of the Trotec Speedy 400](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504034738134_laser-cutting-with-the-trotec-speedy-400-01.jpg)


As soon as the key reached the “On” position, the laser cutter and the exhaust system started. 

Simultaneously, the working table begun to move very slowly until it reached the bottom of the machine, where it stoped.

The next thing I did was placing the balsa wood sheet over the working table, making sure that it was completely flat and tightly positioned against the horizontal/vertical rulers into the upper left-hand corner of the working table.


![Balsa wood sheet correctly placed over the Trotec Speedy 400's working table.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504034758766_laser-cutting-with-the-trotec-speedy-400-02.jpg)


Immediately after, I moved the working table upwards by pressing the Z positioning key, until the surface of the material was approximately  5 cm away from the tip of the processing head. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506441993537_laser-cutting-with-the-trotec-speedy-400-04.jpg)


As I was going to perform a manual focusing (a software focusing is also possible with this machine), I hung the focus tool on the external ring of the head so that the focus tool could move unhindered, and I positioned the head over the material using the X/Y positioning keys.


![Trotec Speedy 400's focus tool hung on the external ring of the laser head for manual focusing.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504034775456_laser-cutting-with-the-trotec-speedy-400-03.jpg)


Then, I moved the working table upwards again by pressing the Z positioning key, and carefully observing the focus tool. And just before the material reached the tip of the focus tool, I started moving the working table upwards very slowly by briefly tapping the positioning key, until the focus tool tilted to the side.


![Trotec Speedy 400's focus tool tilted to the side, meaning that the lens is focused.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504034786486_laser-cutting-with-the-trotec-speedy-400-04.jpg)


At that point, the lens was properly focused onto the surface of the material and I took the focus tool out from the machine, so I could continue. 

The next thing I did was moving the processing head by using the Y and Y positioning keys until the red dot was in the very upper-left hand corner of the material, determining where the origin/starting point for the laser job was going to be located.


![Speedy 400's laser head placed in the very upper-left hand corner of the balsa wood sheet.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504034853681_laser-cutting-with-the-trotec-speedy-400-05.jpg)


Then, I closed the laser-cutting machine’s lid, and the preparation on the machine side was completely finished. It was time to move on to the computers for dealing with the software side! 


### **Preparing the test files**

From the two computers available to work with the laser-cutters at Fab Lab Barcelona’s laser machines room, just one was connected to the Trotec Speedy 400 (and also the Epilog). The second computer was only available for working with the MultiCam 2000. These computers were labeled, so it was easy to figure out in front of which one of them did I have to sit. 


![Computers available to work with the laser-cutters at Fab Lab Barcelona's laser machines room.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504536284246_laser-cutting-with-the-trotec-speedy-400-06.jpg)


Something very important to have in mind when working with a laser-cutter is that even when a preset for cutting a material is available or known, is always recommended to make a test cut before running the full job. There is nothing worse when working with a laser cutter than taking the material out of the machine and finding that it did not cut all the way through. A small circle or square cut in a corner of the material should work. 

In that sense, I had already found a [recommended machine settings cheat-sheet for the Trotec Speedy 400 in Fab Lab Berlin’s wiki](https://wiki.fablab.berlin/index.php/Laser_cutter_Trotec_Speedy400#Recommended_machine_settings), but I still needed to verify that these settings could be transposed to the same machine model at Fab Lab Barcelona.

Thus, the first thing I did once I was sit in front of the corresponding computer was using Rhinoceros to quickly create a 10 x 10 mm little square using the Rhino’s Rectangle command. 

Then, I modified the Rhinoceros document’s layers set up, leaving only one layer called “Cut”, and I colored this layer in red.


![10x10 mm rectangle drawn in Rhinoceros for performing the test cut.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504035243103_laser-cutting-with-the-trotec-speedy-400-07.jpg)


The next thing I did was placing the lower-left corner of the square in the 0, 0, 0 point of the Rhinoceros’s document, although this was not entirely necessary because the print job’s window would be defined later. Then, I saved the document as “10x10mm_square.3dm”

Once the file was saved, I used the `Print` command, and the “Print Setup” dialog window appeared. The 10 x 10 mm red square could be seen as a small object in the middle of the print window. It was so small because the size of the print job at that moment was much more bigger than the size of the square… something that I was going to change a few steps later!


![“Print Setup” dialog window showing the 10x10mm square after using the "print" command.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504035375035_laser-cutting-with-the-trotec-speedy-400-08.jpg)


In the “Print Setup” dialog window, I double checked that the corresponding driver was selected under “Destination” at the upper-left hand of the screen. It had to be “Trotec Engraver v10.3.0”. 

Note that as this computer was also used for sending laser jobs to the Epilog laser cutter (and also to the disappeared Spirit GE laser cutter), so the drivers for these two machines were also installed. The point is that the installed drivers could only be used with their corresponding laser cutter, so that is the reason why it was very important to select the right one!

Then, I also check that the other parameters under “Destination” were ok. 


- Size: “MC 9430”
- Orientation: “Portrait”
- Output Type: “Vector Output”
- Output Color: “Print Color”

And I pressed the “Properties” button (also under “Destination”), so the “Trotec Engraver v10.3.0 Properties” dialog window opened. 


![“Trotec Engraver v10.3.0 Properties” dialog window with the settings used in a previous laser job.](https://www.dropbox.com/s/sioecd79mfgnd19/laser-cutting-with-the-trotec-speedy-400-09.jpg?dl=1)


Under “Size Settings” in the “Trotec Engraver v10.3.0 Properties” dialog window, I introduced the “Width” and “Height” values, based on the size of the laser job that I was going to perform.

- Width: 11.00 mm
-  Height: 11.00 mm

Then, I clicked on the “Material Templates” button under “Material Settings”, and a new dialog window with the materials database appeared. 

This “Material database” dialog window showed the material, processes and settings used in the last laser job that was going to be sent to the machine. As the laser job that I was preparing had nothing to do with the previous laser job, all these details needed to be reviewed and updated.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1525896419529_laser-cutting-with-the-trotec-speedy-400-10.jpg)


The first thing I changed was the material. I selected “Wood > Balsa” from the materials list available at the left hand of the “Material database” dialog window. And then, I also changed the “Material thickness” to 3.00 mm. 

Then, I determined the process that I was going to perform in the table at the right of the dialog window. As I was just going to send a cut job, and the object to be cut was colored in red, I selected “Skip” for every line of the “Process” column, except for line “2”  (as this line was labelled in red color).  


![“Material database” dialog window with the settings for laser-cutting the 10x10mm square.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506345162616_laser-cutting-with-the-trotec-speedy-400-13.jpg)


And finally,  I configured the cutting parameters for that line based on the information collected from Fab Lab Berlin’s recommended machine settings cheat-sheet:


- Power:  100
- Speed: 2.5
- PPI/Hz: 1000
- Auto: _
- Passes: 1
- Air Assist: On
- Z-Offset: 0,00
- Advanced: Custom

And I clicked on “OK”, so the “Material database” dialog window closed.

Then, I pressed the “Store Settings” button at the lower-right hand corner of the “Trotec Engraver v10.3.0 Properties” dialog window that still opened behind, and the “Trotec Engraver v10.3.0 Properties” dialog window closed too. 


![“Trotec Engraver v10.3.0 Properties” dialog window with the settings for laser-cutting the 10x10mm square.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506345179116_laser-cutting-with-the-trotec-speedy-400-14.jpg)


At this point, the material, processes and settings determined for the new laser job were saved!

Back in the “Print Setup” dialog window, I selected “Window” under “View and Output Scale”, and I clicked the “Set” button (under “View and Output Scale”).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1525897671556_Capture32.png)


 
This took be back to the drawing in the Rhinoceros workspace, where I dragged a square to define the print window that I wanted for my “print job”. 


![Print window defined in Rhinoceros.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505985504815_laser-cutting-with-the-trotec-speedy-400-16.jpg)


Then, I pressed the “Enter” key and small dialog window appeared asking me if I wanted to change the scale of the object to fit the window area. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506442413997_laser-cutting-with-the-trotec-speedy-400-16-1.jpg)


I clicked “No” and the small dialog window closed, taking me back to the drawing. 

Then, I pressed the “Enter” key again, and the “Print Setup” dialog window re-appeared.

As the “Width” and “Height” values introduced in the “Trotec Engraver v10.3.0 Properties” dialog window were only 1 mm bigger than the actual size of the square, it could be seen occupying the most part of the printing job area. However, the square size still was 10 x 10 mm.


![“Trotec Engraver v10.3.0 Properties” dialog window showing the red square after defining the print window.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504536395270_Capture31.PNG)


Then, I pressed the “Print” button at the lower-right corner of the “Print Setup” dialog window and the “Print Setup” dialog window closed. At the same time, the laser cutting job was sent to the JobControl application, but as it had not been opened before, it was automatically launched.

Once the JobControl application was open, the “10x10mm_square.3dm” cutting job sent from Rhinoceros as a print job could be seen in the job queue at the right-hand of the program window. 


![10x10mm_square.3dm laser job in JobControl's job queue.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506077858761_laser-cutting-with-the-trotec-speedy-400-18.jpg)


On the other hand,  as the JobControl application that just opened, it was not connected to the laser cutter yet. So the next thing I did was pressing the “Connect” button at the bottom-right of the program window (the button with the image of an USB cable). 


![JobControl's "connect" button.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506077377356_laser-cutting-with-the-trotec-speedy-400-19.jpg)


After 10-15 seconds, a blue cross appeared at the upper-left corner of the white workspace (also known as “Plate”), meaning that the connection was completed, and showing where was the laser head had been located in the machine (during the machine preparation stage) .


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506346707390_laser-cutting-with-the-trotec-speedy-400-20.jpg)


I made zoom in on the plate so the blue cross could be seen better, and I selected the “10x10mm_square.3dm” cutting job from job queue. Then, I dragged and dropped it over the plate, placing the upper-left corner of the job in the center of the blue cross. 

![10x10mm_square.3dm laser job placed over the plate.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504178954356_image.png)


Before sending the job to the machine, I opened the “Material database” dialog window again and I double checked that the material, thickness, processes, power, speed, PPI/Hz, and all the other settings configured in the previous stage, were still correctly set. Note that this was not totally necessary, but I think that is always nice to double check it before definitively submit the laser job.

Note that I also pressed the “WYSYWYG” button, so I could see the red square on the plate instead of a black square. And that I clicked the “Update” button at the bottom of the process time calculation window, so the time that cutting the square will take would be updated.


![10x10mm_square.3dm laser job placed over the plate after pressing the “WYSYWYG” and the "Update" buttons.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506347102749_laser-cutting-with-the-trotec-speedy-400-22.jpg)


Then I clicked the “Start Engraving Process” button at the lower-right hand corner of the program window (the one that looks like a “Play button”), and the laser job started in the Trotec Speedy 400 laser-cutter almost immediately. 


![JobControl's "Start engraving process" button.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506078288103_laser-cutting-with-the-trotec-speedy-400-23.jpg)


After a few seconds the laser cut job was completed! 

I waited until all the fumes in the machine were extracted by the exhaust system, and only then I opened the laser cutter’s lid in order to see the result. 

At the upper-left hand of the balsa wood sheet a small square could be seen!


![10x10mm square test cut performed in balsa wood.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504180079718_image.png)


Then, I used the tip of a flat cutter to check if the part had been completely cut. The little square was cleanly cut and it could be easily separated from the balsa wood sheet, meaning that the cut settings obtained from Fab Lab Berlin’s recommended machine settings cheat-sheet were valid.


![10x10mm square test cut after being separated from the balsa wood sheet.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504180067439_image.png)


At that point, I knew which cutting settings I was going to use for the balsa wood. And I also knew that the settings available in Fab Lab Berlin’s wiki for the Trotec Speedy 400 were pretty trustworthy, so I move on to the next laser job. It was time to run the balsa wood kerf tests!

First of all, I ensured that the balsa wood sheet was still completely flat and correctly positioned into the upper-left corner of the working table against the horizontal/vertical rulers. As the material characteristics have not changed, I assumed that the laser was still focused.

Then I moved the processing head by using the positioning keys, until the red dot was in the upper-left corner of the material, just under the previous test cut. That was where the origin/starting point was going to be located for my next laser job!


![Laser cutter's head placed under the previous cut in order to launch the next laser job.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504535837390_laser-cutting-with-the-trotec-speedy-400-19.jpg)


I closed the laser-cutter’s lid and I moved myself near the computers again, where I opened the laser kerf test part using Rhinoceros and I replicated all the steps followed during the test cut. 

One of the differences with the previous set up was that the size of the laser kerf test was 115 x 125 mm, so the “Width” and “Height” values under “Size Settings” in the “Trotec Engraver v10.3.0 Properties” dialog window had to be changed accordingly (120 x 130 mm for my liking).

Another difference was that this laser job included an engraving process and a second cutting process, so in the processes table of the “Material database” dialog window, I had to select “Engrave” for line 1 (black), “Cut” for line 2 (red), and “Cut” for line 3 (blue). 


![“Material database” dialog window showing the material, process and settings that will be used for laser cutting the laser kerf test part in Balsa wood.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504536427715_Capture33.PNG)


I also determined the engraving and cutting settings based on both the previous test cut exercise, and the information at Fab Lab Berlin’s recommended machine settings cheat-sheet:

LINE 1 - Color BLACK


- Process: Engrave
- Power:  60
- Speed: 100
- PPI/Hz: 500
- Auto: _
- Passes: 1
- Air Assist: On
- Z-Offset: 0,00
- Advanced: Custom

LINE 2 - Color RED


- Process: Cut
- Power:  100
- Speed: 2.5
- PPI/Hz: 1000
- Auto: _
- Passes: 1
- Air Assist: On
- Z-Offset: 0,00
- Advanced: Custom

LINE 3 - Color BLUE


- Process: Cut
- Power:  100
- Speed: 2.5
- PPI/Hz: 1000
- Auto: _
- Passes: 1
- Air Assist: On
- Z-Offset: 0,00
- Advanced: Custom

And I sent the laser job to JobControl, so it also appeared in the job queue. 


![kerf_test_v2.3dm laser job in JobControl's job queue.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506079045955_laser-cutting-with-the-trotec-speedy-400-28.jpg)


Then, I removed the previous “10x10mm_square.3dm” job from the plate, and I dragged and dropped to the plate the “kerf-test_v2.3dm” job, placing the upper-left corner of this new job in the center of the blue cross, as I have done before with the tiny square. 


![kerf_test_v2.3dm laser job placed over the plate.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504536473590_Capture36.PNG)


Lastly, I clicked the “Start Engraving Process” button, so the laser job started.

After a couple of minutes the machine stopped. I waited again until all the fumes were gone, and the I opened the laser-cutter’s lid in order to remove the laser kerf test part from the bed. 

Everything seemed to have turned out marvelously well! 😛 


![Kerf test part laser job performed in balsa wood.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504535871132_laser-cutting-with-the-trotec-speedy-400-22.jpg)


As I wanted to see how the laser cutting settings affect the laser kerf, I performed two more laser jobs of the very same laser kerf test part, but changing the speed to 2.0 and 1.5, respectively.

Once the three tests were cut, I measured the laser kerf for them all using a digital caliper.


![Laser kerf for 3mm balsa wood with the cutting speed set to 2.5 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993032177_laser-cutting-with-the-trotec-speedy-400-31.jpg)

![Laser kerf for 3mm balsa wood with the cutting speed set to 2.0 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993031593_laser-cutting-with-the-trotec-speedy-400-32.jpg)

![Laser kerf for 3mm balsa wood with the cutting speed set to 1.5 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993031039_laser-cutting-with-the-trotec-speedy-400-33.jpg)


Effectively, the slower the cutting speed, the bigger the laser kerf! 

I guess that increasing the power would had the same effect but as I was already cutting with the Power setting at 100, this parameter could not be increased.

The laser kerf test experiment with balsa wood was completed. I have learnt how to use the laser cutter and the management software. And I also knew the best settings for cutting and engraving the 3 mm balsa wood. I could use these settings for all the other laser jobs with 3 mm balsa wood.

Anyway, I still did not know about the acrylic or the cardboard… so I repeated the entire experiment using the 3mm acrylic sheet first, and the 4mm cardboard sheet later.

As with the balsa wood, I did a small test cut in both materials using the same laser job that I have used before. And the initial cutting settings for the acrylic and the cardboard were also obtained from Fab Lab Berlin’s recommended machine settings cheat-sheet.

In the case of the 3 mm acrylic, I was able to increase the initial speed (from 0.75 to 1.10), and the material was still being correctly cut, so this new cutting speed was used as reference.

However, I made three laser kerf test parts in acrylic using the recommended Power and PPI/Hz settings but setting the cutting speed at 1.10, 0.75 and 0.30, respectively. 


![Laser kerf for 3mm acrylic with the cutting speed set to 1.1 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993125488_laser-cutting-with-the-trotec-speedy-400-34.jpg)

![Laser kerf for 3mm acrylic with the cutting speed set to 0.75 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993134960_laser-cutting-with-the-trotec-speedy-400-35.jpg)

![Laser kerf for 3mm acrylic with the cutting speed set to 0.3 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993148940_laser-cutting-with-the-trotec-speedy-400-36.jpg)


And finally, I also made three laser kerf test parts in cardboard using the recommended Power and PPI/Hz settings, but setting the cutting speed at 2.5, 2.0 and 1.5, respectively.


![Laser kerf for 4mm (1 layer) cardboard with the cutting speed set to 2.5 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993166543_laser-cutting-with-the-trotec-speedy-400-37.jpg)

![Laser kerf for 4mm (1 layer) cardboard with the cutting speed set to 2.0 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993174095_laser-cutting-with-the-trotec-speedy-400-38.jpg)

![Laser kerf for 4mm (1 layer) cardboard with the cutting speed set to 1.5 being measured with a digital caliper.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505993209560_laser-cutting-with-the-trotec-speedy-400-39.jpg)


At the end of this part of the assignment, I had three laser kerf test parts made with three different cutting speeds for each of the three materials selected. And in all cases, the relation between the material, the cutting settings and the laser kerf was proven.


### **Cutting the press-fit test parts**

Once I knew the right settings to get the best results (for both engraving and cutting) with the three materials that I had planned to use during this exercise, it was time to move ahead on the assignment and make the press-fit test parts. I basically put into practice the concepts that I had previously learned, and I completed the whole (machine/software) preparation process again.

In few minutes, I had ready the press-fit test parts for each material. 

The only difference with the previous laser jobs that I could point out, is that a second “Cut” process was not necessary this time, as in the press-fit test parts there were no “inner cuts” to be performed between the engraving and the outline cutting processes.

For the 3 mm balsa wood, the best press-fit joint was achieved using the 2.95 mm slots. 


![3mm balsa wood press-fit test parts joined using the 2.95 mm slots.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505208477296_IMG_1548.jpg)


Instead, for meeting the most tight press-fit joint with the 3 mm acrylic, the 3.15 mm slots had to be used. Although, the 3.20 mm slots also procured a pretty consistent joint.


![3mm acrylic press-fit test parts joined using the 3.15 mm slots.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505208497125_IMG_1550.jpg)


And for the 4 mm cardboard, the 3.80 mm slots demonstrated to be the most adequate ones.


![4mm (1 layer) cardboard press-fit test parts joined using the 3.80 mm slots.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505208515047_IMG_1551.jpg)


At this point, I was confident enough to perform any kind of job with the laser cutter, as I already knew the steps that I had to follow on both the machine and the software side. 

In addition, I had learned the right cutting/engraving settings, and I had found out the suitable slot sizes for the three materials that I had selected for this part of the assignment. I could well move on to the parametric press-fit construction kit, but there were a few more laser cutting and engraving tests I wanted to make before reaching that stage.


### **Making the engraving test parts**

As for the engraving test parts, it did not make too much sense to me to use cardboard. 

Of course I could have used it, but the kind of cardboard that I had selected for this assignment did not appear to be the most suitable. The cardboard’s superficial layers were pretty thin and it seemed to me that from a certain engraving depth, the engraving would be turn into a hole. Thus, I only used the balsa wood and the acrylic for this part.

To begin with, I prepared a new laser job for the engraving test that was entirely vector formed, using the same recommended settings for the 3 mm balsa wood that I have used before. 

Note that in this case a second “Cut” process was not necessary either, as there were no “inner cuts” in the engraving test part that I had designed.


![Outcome of the vector formed engraving test in balsa wood.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504725239662_IMG_1506.jpg)


Immediately after (and using the same material), I repositioned the laser head and launched the very same laser job two more times, but changing the “Power” to 70 and 50, respectively.

In this way, it was possible for me to appreciate how a slight change in the “Power” parameter (the speed and PPI remained completely the same) affected every sector in the greyscale wheel.


![Outcome of the vector formed balsa wood engraving tests varying the engraving speed.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504778031709_IMG_1511.jpg)


Continuing with the engraving test parts in 3mm balsa wood, I launched a new laser job for the engraving test part with the embedded JPG image of the greyscale wheel that I had prepared in the “Making laser-cutting test parts” stage. 

I used  the recommended engraving settings for balsa wood again. But in the “Print Setup” dialog window, I had to select “Raster Output” under “Output Type” (instead of “Vector Output”) so the whole job was sent as a raster image. 


![“Raster Output” selected under “Output Type” so the laser job is sent a raster image.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506349544100_laser-cutting-with-the-trotec-speedy-400-45.jpg)


Note that, as I selected “Raster Output”, the red outline for the cutting process was also batched as a raster image, meaning that it was not recognized as a cutting line, no matter whether the process for the RED color row in the materials database dialog window was set to “Cut” or not. 

In order to perform the cutting once the engraving was finished, I went back to Rhino’s interface and I used the “Hide” command to get temporarily rid of the greyscale wheel’s image. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1505985003633_laser-cutting-with-the-trotec-speedy-400-46.jpg)


Then, I sent a new ”print” for a second laser job, but selecting “Vector Output” under “Output Type” in the “Print Setup” dialog window. In this way, a new laser job appeared in JobControl’s queue containing only the red cutting line that I needed for the cutting process.

At that point, I just had to make sure that the RED color row in the materials database dialog window was set to “Cut”, and that the cutting settings for the material being used were still ok. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506442621416_laser-cutting-with-the-trotec-speedy-400-47.jpg)


Another interesting utilities that I had to use in order to complete this “raster” engraving job successfully, were JobControl’s “Marker to Job” and “Move head to marker” features.  

Once the engraving process was completed, the laser head went back to the machine’s zero point. This meant that I had lost the origin point for the laser job that I was performing.

Anyway, as the engraving job that I had just sent was still in JobControl’s plate, I selected it and clicked “Plate” in the main menu. From the dropdown menu, I selected the “Marker to Job” option, and a marker was set in the upper-left corner of the engraving job that was still on the plate.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506442634256_laser-cutting-with-the-trotec-speedy-400-48.jpg)


Then I clicked on “Engraver” in the main menu, and I selected “Move to Marker” from the dropdown menu. The laser head moved to the recently added marker, and the laser job’s origin for the cutting process was set in the very same point where it was set for the previous engraving job.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506442649077_laser-cutting-with-the-trotec-speedy-400-49.jpg)


Then I removed the “raster” engraving laser job from the plate, and I drag-and-dropped the laser job with the red cutting line. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506442676820_laser-cutting-with-the-trotec-speedy-400-50.jpg)


Finally, I clicked the “Start Engraving Process” button and the cutting was finish in seconds.


![Outcome of the image formed engraving test using the recommended settings for balsa wood.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504778265692_IMG_1508.jpg)


Surprisingly, and despite using the very same settings in both the “vector” and the “raster” engraving test jobs, the darker sectors of the part sent as “Raster Output” happened to be much more strong than the very same sectors of the test sent as “Vector Output”, while the lighter sectors were much more similar.


![Difference between the vector formed and the image formed engraving tests using the same settings.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504778435956_IMG_1509.jpg)


Finally, I removed the balsa wood sheet from the laser cutter and I replaced it  with the clear acrylic sheet. Then I launched the laser job of the engraving test part made entirely with vectors, using the same recommended settings for the acrylic that I have used before.

As expected, it turned out that the darker the color the whiter engraved sector!


![Outcome of the vector formed engraving test in 3mm clear acrylic.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504779211015_IMG_1505.jpg)


### **Making the halftone test**

For the halftone test, I repeated the very same steps that I followed before on the engraving/cutting process of the greyscale wheel with the JPG image embedded.

I started with the balsa wood once more. And I sent the laser job as “Raster Output” first , so image and text were engraved. Immediately after, I sent a new laser job as “Vector Output” with just the RED line, so the outline was cut. 


![Outcome of the halftone test in 3mm balsa wood .](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504792923484_IMG_1513.jpg)


Then I removed the balsa wood sheet, and I replaced it with the clear acrylic sheet. And I repeated the very same process but changing the engraving/cutting settings for the acrylic.


![Outcome of the halftone test in 3mm clear acrylic.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1504795910267_IMG_1514.jpg)


On both cases, I used the same recommended settings for each material that I have used before. 

It was time to go for the living hinges!  


### **Making the living hinges**

Cutting the living hinges was nothing extraordinary in terms of preparation. I had already experienced the need of configuring a laser job with two different laser cutting processes before, when I needed to cut the inner blocks of the kerf test part first, leaving the kerf test part’s outline for the end. It was as simple as selecting “Cut” for line 2 (red), and “Cut” for line 3 (blue) in the processes table of the JobControl’s “Material database” dialog window.

Anyway, it was a really mesmerizing process visually speaking! Watching the laser head crazily coming and going from end to end of the machine’s workbench until the whole inner patterns of each living hinge test part was completed was a worth seeing process. 

And it was even more amazing to check the results once the cutting was finished!


![Super flexible plywood living-hinge test part being tested](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506007359511_laser-cutting-with-the-trotec-speedy-400-56.jpg)


I used balsa wood for the “super flexible plywood” living-hinge test part, and also for the living hinges pattern collections. I still had a lot of 3mm balsa wood left, but not so much 3 mm acrylic, so I only used the 3 mm acrylic for the lattice hinge test parts.


![3 mm acrylic lattice hinge test parts being tested](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506007384793_laser-cutting-with-the-trotec-speedy-400-59.jpg)


For both the balsa wood and the acrylic, I used the same recommended settings that I have used before. In any case, is quite interesting to mention that for some of the living hinges, mostly those that are created using only cuts (and not holes), the bigger the laser kerf the more flexible they are. This means that forcing a bigger laser kerf could be interesting in some cases!


![A laser-cut living hinges pattern collection.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506007377501_laser-cutting-with-the-trotec-speedy-400-58.jpg)


By the end, I had a few representative examples of the existing living hinge patterns, that allowed me to get a clearer picture of the characteristics and the possibilities provided by this amazing technique for bending rigid materials.


![Another laser-cut living hinges pattern collection.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506007371108_laser-cutting-with-the-trotec-speedy-400-57.jpg)


Reached that point, I had made all the tests that I wanted to make and I guessed that the “group assignment” part for that week was completed. It was time to move ahead onto the individual assignment and address the parametric press-fit construction kit!


## **Designing a parametric press-fit construction kit**

Designing a parametric part was totally new for me. I had heard the “parametric design” term many times before but never made one, so I was quite excited about it.

At that time, there were a few software alternatives that I could have used. And I imagine that the right choice would have been to use Rhinoceros + Grasshopper. Anyway, the only version of Rhinoceros for Mac OS X that allowed to use Grasshopper at that time was [RhinoWIP](https://discourse.mcneel.com/t/goodbye-rhinowip-for-rhino-5-for-mac/56242), and I had Rhinoceros v5.3.2 already installed in my computer. As I did no want to have two versions of the same software installed in my laptop (it was already quite overloaded), I decided to use Fusion 360 instead.

Fusion 360 is a cloud-based 3D CAD/CAM tool for product development that combines industrial and mechanical design, collaboration, and machining in a single package. It is available for free to students, and there is an amazing amount of resources to quickly learn how to use it.

After watching [a few online video tutorials](https://www.google.com/search?q=fusion+360+parametric&safe=off&rls=com.microsoft:es:%7Breferrer:source%3F%7D&rlz=1I7ACAW_esES322&source=lnms&tbm=vid&sa=X&ved=0ahUKEwjmzP7_5PnaAhWGa1AKHcQlBfIQ_AUICigB&biw=1440&bih=735) about how to make parametric designs using variables (called "user parameters”) and constrains with Fusion360, I felt confident enough as to start designing the parametric press-fit construction kit. 

The first thing I did was to open a “New Design” in Fusion 360. And then, I drawn the entire shape of the individual part that I wanted for my press-fit construction kit.


![Shape of the press-fit construction part.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506333270318_Captura+de+pantalla+2017-09-25+a+las+11.30.06.png)


With the press-fit construction kit completed, I opened the “Parameters” dialog window by clicking on “MODIFY” in the main toolbar, and selecting “Change Parameters” from the dropdown menu.


![“Change Parameters” selected from the “MODIFY” dropdown menu.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506336999215_designing-a-parametric-press-fit-construction-kit-02.jpg)


Next, I created 2 new “User Parameters” by clicking the “+” symbol located on the right of the “User Parameters” term. 

Clicking the “+” symbol opened a new dialog window called “Add User Parameter” where I completed the “Name” and “Expression” details. 


!["Add User Parameter" dialog window open after clicking the “+” symbol next to the “User Parameters” term.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506337017556_designing-a-parametric-press-fit-construction-kit-03.jpg)


For greater clarity, I named these “User Parameters” as “lado” (side) and “grosor” (thickness).


!["lado" and " "grosor" parameters created in the “User Parameters” dialog window.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506337035032_designing-a-parametric-press-fit-construction-kit-04.jpg)


Then, I started applying dimensions and constrains but it did not work as I expected.

Honestly, I though that making the design first and then start adding dimensions/constrains would be the best way to complete a parametric design. But it did not work for me (or at least I was not capable of making it work this way), so I had to start from the very beginning again.

As I though that the “User Parameters” that I had created before were still correct, I did not change or remove them. Anyway, I deleted the previously drawn shape from Fusion360’s workspace, and I drawn a simple square, modifying this square’s shape step by step while adding the corresponding dimensions/constrains at every turn. 

After a good while I ended up with a drawing full of dimensions and constrains, but it seemed to work fine. When the values of “lado” or “grosor” in the “Parameters” dialog window were changed, the design changed accordingly. 


![Shape of the press-fit construction part with all the dimensions and constrains added.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506334555658_Captura+de+pantalla+2017-09-25+a+las+12.15.28.png)


Finally, I set the “lado” parameter to 150 mm and the “grosor” parameter to 3.80 mm. 

My parametric press-fit construction part was ready in Fusion 360, but I still needed to save the design in a format that could be opened with Rhinoceros later. 

[Fusion 360 can export files in two different ways](https://knowledge.autodesk.com/support/fusion-360/learn-explore/caas/sfdcarticles/sfdcarticles/How-to-export-a-design-in-Fusion-360.html), directly from Fusion 360 by selecting “Export” from the “File” drop down menu, or from the A360 Project via the web browser. I decided to use the second option, and I exported the shape as DXF file via Autodesk’s A360 Project hub.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506337053088_designing-a-parametric-press-fit-construction-kit-06.jpg)


When exporting from Fusion 360 file using the A360 Project via the web browser, you receive an email with the selected file attached. Once I had received the aforementioned email, I saved the attached DXF file in my computer.

Then, I opened this DXF file in Rhinoceros, and I checked that the curves were closed. I also changed the units to millimeters, and I modified the Rhinoceros document’s layers set up (leaving only one layer called “Cut”). Finally, I colored the “Cut” in red, and I saved the file as 3DM.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506335387855_Captura+de+pantalla+2017-09-25+a+las+12.29.04.png)


The parametric design that I had created was ready to be cut! Next stop… laser cutting enough parametric press-fit construction parts to make a kit!


## **Laser-cutting the press-fit construction kit**

Making the press-fit construction kit in different materials would have been great. Anyway, I wanted the parts to be 150x150mm, so though that making it using only the 4 mm cardboard should be less material consuming, and enough as to complete the assignment successfully.

I placed one the 4 mm cardboard sheets that I still have available over the laser-cutter working table, and I followed all the steps that I have learnt from the previous exercises: 


- positioning the material, 
- focusing the lens
- setting the origin point. 

Then, I closed the laser-cutter’s lid and moved towards the computer.

Once in front of the computer, I sent an initial laser job for the 10x10mm square in order to check that the previously used settings were still appropriate. 

The little square test was correctly cut, so I prepared a new laser job with a single unit of the parametric part and I sent to the machine using JobControl.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506361623880_IMG_1608.jpg)


The resulting part was correctly cut, so I went ahead and cut a second unit. 

I repositioned the laser head by using the laser cutter’s X/Y positioning keys, and I launched the very same laser job with the single parametric part again. 

Once the second parametric part was laser cut, I removed it from the laser cutter’s working table.

When I checked, the slots in both parts fit perfectly into each other and the joint was strong enough as to hold them together, meaning that the 3.80 mm slot size was still appropriate


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506361645443_IMG_1611.jpg)


As the cutting settings and the slot size were proven to be fine, I used Rhinocero’s `Rectangular Array` command for preparing a new file with a larger amount of the part, awolling me to laser cut several pieces in one go, instead of sending the parts one by one. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506362625715_Captura+de+pantalla+2017-09-25+a+las+20.03.22.png)


I used all the cardboard sheets that I have left, ending up having ~40 pieces of my part. 

Finally, I had my press-fit construction kit, and I could built a structure with it.


![Cardboard structure built using the parametric press- fit construction kit.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506366064607_FullSizeRender.jpg)



## **Cutting something on the vinyl-cutter**

****When I started with this part of the assignment I was not totally sure about what to cut. I knew that stickers for laptops, signs for the lab or even a mask for screen printing t-shirts were some of the options but I did not know which one to choose (or if I wanted to make something different). 

As I needed to get familiar first with the [CAMM-1 GX-24 vinyl cutter](http://support.rolanddga.com/_layouts/rolanddga/productdetail.aspx?pm=gx-24) available at Fab Lab Barcelona, and also find out the material available for this assignment, I left the decision for later.


![Roland CAMM-1 GX-24 vinyl cutting machine available at Fab Lab Barcelona.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500566820528_IMG_1100.JPG)


### **Selecting the cutting material** 

When I was starting with this assignment, there was only one kind of material at hand in the Lab to be used with the vinyl-cutter and it was a 500 mm wide matte black adhesive film. The quality of this adhesive film was quite poor but good enough as to complete the exercise.

Although it could appear not to be relevant, the size of the material available was a detail to have in mind from the very beginning, as the width of the material (and also the length) would be determinant later when setting the cutting range and preparing the job.


![500 mm wide matte black adhesive film roll.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500905500081_IMG_1307.jpg)


### **Reading the user manual**

Once I knew which material was available, it was time for me to get to know the machine a little bit better, so I [downloaded the CAMM-1 GX-24 vinyl cutter user manual](https://metalab.at/wiki/images/2/2b/Cuttr_Manual.pdf) and I made a general reading, with particular attention to Chapter 1: Getting Started, Chapter 2: Preparing the GX, and Chapter 4: Performing Cutting.

As the [Roland CutStudio software](https://www.rolanddga.com/products/software/roland-cutstudio-software) was already installed and working in the computer near the vinyl-cutter and this software was supposed to be only suitable for Windows machines (meaning I could not install it in my macOS laptop), I skipped Chapter 3: Installing/Uninstalling Software.


### **Loading the material**

After I finished reading the user manual, I felt confident enough to get my hands on the vinyl-cutter, so I placed the vinyl cutter on top of a leveled table, and I inserted the roller base into the machines's lower back groove. 

As the material I was going to use was a roll, the roller base accessory seemed to be pretty convenient (although not essential at all).


![Roller base accessory inserted into the machines's lower back groove.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500567014988_IMG_1148.jpg)


Then, with the vinyl-cutter still turned off, I placed the black vinyl roll in the roller base and unrolled it sliding the leading edge through the machine, until it came out from the front side. 

Note that before loading the adhesive film, I pressed the sheet load leveler at the back of the machine. This made the two pinch rollers at the front to go up, allowing me to slide the material’s leading edge through the machine without any resistance. 


![Sheet load leveler at the back of the machine pressed, so the material can be slide through.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500629239524_IMG_1228.jpg)


If the sheet load leveler would had been raised (meaning that the pitch rollers would have been down) it would not have been possible to slide the sheet through the machine. 

After the adhesive film roll was loaded into the machine, I used the guide line marks at the front and the back of the vinyl-cutter’s surface to align the material.


![Material aligned using the guide line marks at the front and the back of the vinyl-cutter’s surface.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500629270860_IMG_1255.jpg)


Once the roll was correctly aligned, I placed the two pinch rollers (right and left) on the edges of the material. These two pinch rollers had to be also within the white spaces available, known as “pinch roller position verification marks”. 

The position of the pinch rollers could be adjusted either from the front or the back of the machine, although they seemed much more easy to move from the back. 


![Pinch roller placed on the edge of the material and within the position-verification mark.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500629527387_FullSizeRender.jpg)


As the pinch roller position-verification mark at the left of the vinyl-cutter was bigger, I placed and aligned the vinyl roll adapting the right edge of the material to the left edge.

With the material aligned, and the pinch rollers at the edges of the material within the position verification marks, I raised the sheet load leveler at the back of the vinyl-cutter, so the two pinch rollers pressed the adhesive film sheet and the material was secured in place.


![Material aligned and secured in place.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506610999738_cutting-something-on-the-vinyl-cutter-07.jpg)


It was time to turn the vinyl-cutter on! Yeeees! 😛 

When I turned the machine on by pressing the “ON/OFF” button at the lower-right corner of the operation panel, the carriage moved slowly to the right end. 

Simultaneously, the “SELECT SHEET” menu popped up in the operation panel display.

Then, I had to choose the type of material I was going to use between “ROLL”, “PIECE” or “EDGE” by pressing the UP/DOWN arrows in the operation panel. By default, “ROLL” was the first option. 

As I was going to use a vinyl roll, I selected “ROLL” by pressing the “ENTER” key.


!["ENTER" key being pressed in order to select "ROLL"  from the "SELECT SHEET" menu.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500571787785_IMG_1203.jpg)


I guess it is interesting to mention that when playing around with the “SELECT SHEET” options, I also noticed that there was no big difference when selecting “EDGE” instead of “ROLL”. 

When selecting “ROLL”, the carriage only moved from right to left measuring the width of the material, so the cutting started at the location where the material is secured in place. But when selecting “EDGE”, along with the carriage movement, the vinyl-cutter also moved the material to the rear until the leading edge was aligned with the carriage, so the cutting started at the leading edge of the roll.  

As I had selected “ROLL”, after the carriage moved from right to left measuring the width of the material, I had to move the material to the rear using the UP ARROW button in the operation panel in order to start cutting at the edge of the roll.

In any case, as soon as I pressed the “ENTER” button to select “ROLL”, the message “NOW LOADING...” appeared in the operation panel's display. And as mentioned before, the carriage started to move smoothly from right to left, measuring the cuttable width of the material.


![Vinyl cutter's display showing "NOW LOADING..." after choosing the type of material.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500905521269_IMG_1301.jpg)


When the carriage reached the left end, the cuttable width (W:) of the material could be seen in the operation panel's display (in millimeters). But this number was a bit lower than the real width of the material: 478 mm vs 500 mm.

Consequently, I noticed that the measured length was totally dependent on where the pinch rollers were placed. As the cuttable width was the distance between the inner edges of the pinch rollers, the more at the edge I was placing the pinch rollers the more cuttable length I was getting. 


![Vinyl cutter's display showing the the width of the material.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500632131506_IMG_1213.jpg)


In any case, I could not place the pinch rollers even one millimeter outside the position-verification marks. Otherwise, the “BAD POSITION” message would appear in the operation panel's display.

Another interesting thing to mention is that every time I wanted to adjust any of the two pinch rollers (in order to add up a few millimeters) I had to press a couple of times the “MENU” key in the operation panel, until the “UNSETUP” message appeared in the display. 


![Vinyl cutter's display showing "UNSETUP" after pressing the "MENU" key a couple of times.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500629944390_IMG_1208.jpg)


Then I had to press the “ENTER” key, so the carriage would go back to its original position.

Once the carriage was back in the right end of the machine, the “SELECT SHEET” menu appeared again in the operation panel's display, so I could start the material setting process all over again: lowering the leveler, adjusting the pinch rollers, checking that the material was aligned, raising the leveler, SELECT SHEET > ROLL, etc.

Finally, with the material aligned, the pinch rollers in position, and the information about the material width visible in the operation panel's display, it was time to go ahead. 

The next step was optimizing the cutting quality for the material.


### **Tuning the cutting quality settings**

In order to obtain high-quality cutting results, carrying out a cutting test was a must. 

It was very important to find out the cutting quality of the material that I was about to use, because this quality might be different from one supplier to the other, or even between materials from the same supplier. But also because (as Neil mentioned during the lecture) other external parameters like room relative temperature or humidity when performing the cut could affect the results significantly.

When I turned the vinyl-cutter on, the FORCE was already set to 150 gf (gram-force), the SPEED was set to 15 cm/s (centimeters per second) and the BLADE OFFSET was set to 0.250 mm (millimeters). This settings could be seen by pressing the MENU button.


![Vinyl cutter's display showing  the "SPEED", "FORCE" and "BLADE OFFSET" settings.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500710561401_IMG_1276.jpg)


On the other hand, I knew that other people had been using the vinyl-cutter before me to cut the very same material I was about to use, so those should be the right settings. 

Anyway, in order to make myself sure, I pressed the “TEST” key at the upper-left corner of the operation panel for one second or longer, and a test pattern was cut. 

Then, I fed the material toward the front by holding the DOWN ARROW button and I used a pair of tweezers to peel off the cut shapes. The outer circle peeled off but not the rectangle, meaning that the cutting quality was ok.


![Outer circle of the test cut being peeled off with a pair of tweezers.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500630012241_IMG_1226.jpg)


As I also wanted to learn how to change the machine’s force, speed, and offset in case I needed to do so in the future, I spent some time playing around with the settings.

In order to change the cutting speed, I pressed the MENU button of the operation panel until I saw UNSETUP in the display. Then I pressed the DOWN ARROW button until CONDITION appeared.


![Vinyl cutter's display showing  the "CONDITION" menu.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500905544756_IMG_1295.jpg)


With CONDITION in the display, I pressed the RIGHT ARROW button to enter the “user condition settings” menu (although pressing the ENTER button could have work too), and the option “FORCE 150gf” appeared in the display. Then I hit the DOWN ARROW button again until “SPEED 15cm/s” appeared in the display.


![Vinyl cutter's display showing  the "SPEED" menu.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500710588459_IMG_1279.jpg)


With “SPEED 15cm/s” in the display, I hit the RIGHT ARROW button to enter the “cutting speed edit mode” and I changed the cutting speed within a range of  1 cm/s to 50 cm/s (or 10 mm/s to 500 mm/s) by pressing the TOP/DOWN ARROW buttons. 


![Vinyl cutter's display showing the highest speed available in the "SPEED" menu.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500710609497_IMG_1282.jpg)


After setting the speed back to 15 cm/s, I pressed the ENTER button. Then I pressed the MENU button, so I exit the “user condition settings” and the display showed the original settings screen.

The process for changing blade offset was almost the same as with the speed. But instead of selecting “SPEED” inside the “user condition settings” menu, I had to select “OFFSET”. 

The offset is the distance from the tip of the blade to the center of the blade, and in the Roland GX-24 this offset/distance range was from 0.000 mm to 1.000 mm.  

Choosing the right offset for the blade that it is being used (45º vs 60º) will better negotiate corners and curves. Based on that, I left the OFFSET value in 0.250 mm, which was the right distance for the 45º angle blade installed in the machine.

Note that if the cutting blade angle installed in the vinyl-cutting machine would had been 60º, the OFFSET value should have been set to 0.400 mm.


![Vinyl cutter's display showing the "OFFSET" menu.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500710634958_IMG_1283.jpg)


The last setting I wanted to check was the cutting force. In order to access this setting I could follow the previous steps and select FORCE inside the “user condition settings” menu. 

Or I could just press the “FORCE” button in the operation panel, which seemed much more easy.

In any case, the first thing I did before going further was checking that the “PEN FORCE” slider in the lower side of the operation panel was centered, meaning that it had to be at “0” on the scale.


![“PEN FORCE” slider in the lower side of the operation panel.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500630043994_IMG_1218.jpg)


Once the “PEN FORCE” slider was centered, I pressed the “FORCE” key in the upper-left side of the operation panel, and the “RIGHT ARROW” key to enter the edit mode. 

Next, I used the UP/DOWN ARROW buttons to increase and decrease the “FORCE” number. 

I changed the “FORCE” value to higher and lower values indistinctly. For each of the randomly selected values, I pressed the “ENTER” key to enable the setting, and I pressed the “MENU” key or the “LEFT ARROW” key to go back to the settings screen.

Then, for each of the values selected, I pressed the “TEST” key for one second or longer, so the test patterns were cut. 

Obviously, with a lower force (30 gf) the cutting was not enough as to cut the vinyl, with a medium force (100 gf) both the outer circle and the rectangle were peeled off, and with the FORCE parameter to its maximum value (250 gf) the blade almost went through the material, so both the adhesive film and the support layer could be completely removed.  


![Test cuts made using different "FORCE" values.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500630087108_IMG_1261.jpg)


After performing these 3 test patterns cuts and with the lesson learned, I set the “FORCE” back to 150 gf and went ahead with the assignment. 

The next stop was to create cutting data and… cuuuuuut!

Anyway, before doing anything else, I held down the DOWN ARROW button in the vinyl-cutter’s operation panel in order to move the material toward the front. And I used an utility knife and the machine’s knife guide to cut off the portion of the material that had been used to cut the tests, leaving the adhesive film completely free of cut areas.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500630900904_IMG_1263.jpg)


It was time to start sending cutting jobs to the machine using any of the different software applications suitable for this task: Fab Modules, Mods, Inkscape or CutStudio.


### **Creating cutting data and NOT performing cutting with Inkscape**

Considering that I use it quite often and that there was [a tutorial available in Fab Academy tutorial's website](http://fabacademy.org/archives/2015/doc/vinyl_cutter.html) that explains the whole process, using Inkscape for vinyl-cutting seemed pretty obvious and a very interesting option to me.

In any case, when I was sending simple geometries directly from Inkscape in order to understand the workflow, I found out lots of inconsistencies between what I was sending and the vinyl-cutter’s outcome. From changes in the orientation of the geometries sent, to total losses of the origin. 
It was a very disturbing and frustrating sensation I have to say!

Several attempts later, the situation did not change although I tried all the possible settings combinations. Sometimes it worked fine and the very next time, it did not work at all. All this lead me to think that there might be a software issue. I searched online and I found out that some versions of Windows could be erratic when using Inkscape and a Roland GX-24 vinyl-cutter.

I updated the Inkscape version but this did not fix the issue. I even downloaded an [Inkscape extension called Inkcut](http://inkcut.sourceforge.net/) but it did not work either. And I was not happy with start updating the operating system, drivers or any other thing… so I basically gave up! :(


![My mood after several frustrated attempts of using Inkscape for sending jobs to the vinyl cutter.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500632057574_capture-01.png)



### **Creating cutting data and performing cutting with CutStudio**

Next move in order to go ahead on this part of the assignment without further delays was using Roland’s [CutStudio](http://www.rolanddg.eu/en/applications/cutstudio) software, but I needed to learn how to use it first.

CutStudio is a program specially designed by Roland for being used with their CAMM-1 series vinyl-cutters, that provides the necessary tools for designing custom cut graphics. It supports popular file formats like AI and EPS. And it also includes a vector function that creates cutting lines from BPM, and JPG files.

As mentioned before, installing it on my laptop was not an option because it was only compatible with Windows 7, Vista and XP, but I could learn how to use it straight away by using the copy installed in the fab lab’s computer.


![CutStudio being launched in a Windows machine.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500487958150_capture-02.JPG)


As I did at the beginning of my frustrated intent of using Inkscape, I started playing around with simple geometries before even thinking on sending a custom designed graphic.

The first thing I did after opening CutStudio in the Lab’s computer was setting the cutting range. 

I clicked “File” on the main menu at the top of the program window, and I selected “Cutting Setup”. The “Cutting Setup” dialog window appeared.

Then, in the “Cutting Setup” dialog window, I checked that “Roland GX-24” was chosen under "Printer”, and I clicked the “Properties” button.


![“Cutting Setup” dialog window with “Roland GX-24” chosen under "Printer”.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500484344955_capture-03.JPG)


When I clicked the “Properties” button, a new dialog window named “Roland GX-24 Printing Preferences” opened. And in the “Size” tab of that new dialog window, I clicked the “Get from Machine” button available under “Cutting Area”. The cuttable range was obtained from the machine, and displayed in the corresponding field. 

Note that as I was using a vinyl roll, only the “Width” parameter was automatically changed, and I had to modify the “Length” parameter manually. The “Length” that I introduced was 1000 millimeters, in order to have surplus material. I could have been even more as the vinyl roll was longer, but the true is that I was going to use even less than 1000 mm.

In any case, I also checked in the “Roland GX-24 Printing Preferences” dialog window that “Millimeters” was selected under “Units”, and that “Off” was selected under “Rotate”.


![“Roland GX-24 Printing Preferences” dialog window.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500484393421_capture-04.JPG)


Then, I clicked the “OK” button, and the “Printing Preferences” dialog window closed.

And I also clicked “OK” in the “Cutting Setup” dialog window that still opened at the back, so the cutting range for the material that I was going to use was finally set. 

Once the “Cutting Setup” dialog window closed, the program window behind was showing a white area that matched exactly the size introduced under “Cutting Area” in the “Roland GX-24 Printing Preferences” dialog window. 

I could start adding text, shapes, graphics, etc.


![CutStudio's work space after setting the "Cutting Area".](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500484831160_capture-05.JPG)


At the left side of the program window there was a tools menu with different buttons that I used to create text in vector format, squares, circles and other geometric shapes.

Then, I arranged all these elements I had created at the bottom edge of the cutting range. 

In theory, as the leading edge of the loaded material corresponded to the bottom edge of the cutting range, placing the shapes to be cut there would prevent a wasteful feed of material.


![Text and other geometric figures arranged at the bottom edge of the cutting range.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500574636667_capture-06+copia.JPG)


Then I clicked the “Cutting” button at the top menu bar of the program window. And this opened the “Cutting“ dialog window, where I just had to click “OK” for the cutting process to start. 

A few seconds later the cutting process had finished! 

Then, I held down the DOWN ARROW button in the vinyl-cutter’s operation panel to move the material toward the front, and I used an utility knife to cut off the portion of the material that had been cut by the machine.  

Once the leading edge of the material was cut from the roll, I used a pair of tweezers and my hands to peel off the excess material, leaving only the text and the shapes. 

Nothing extraordinary to be honest! But I was glad to have done my first vinyl cut ever.


![Text and shapes on the adhesive film after the the excess material was peeled off.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500631019007_IMG_1265.jpg)


Continuing with CutStudio, there was an operation that I really wanted to try: “vector function”. This was special Cut Studio feature to create cutting lines directly from BMP and JPG files.

In order to test the “vector function”, I left-clicked on the “Import” button at the top menu bar and the “Import” dialog window appeared on the screen program.


![“Import” dialog window.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500574651753_capture-06_5.JPG)


I used the “Import” dialog window to browse the computer files. And I selected a JPG file with a nice typography that I had previously downloaded from the internet. 

Then, I clicked on the “Open” button of the “Import” dialog window.

When I pressed the “Open” button and the “Import” dialog window closed, the selected JPG file appeared as a background image on the work space. It was behind the grid and also unselected. 

I clicked on top of the image, and four black notes appeared in the corners, meaning that it had been selected.


![Imported JPG file on the program window’s work space.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500486598266_capture-07.JPG)


Next, I left-clicked “Object” in the main menu and a new window dialog called “Image outline” appeared, where I could see a small image of the imported JPG file.

I adjusted the image density by using the slider under “Alignment Image Density”. 

Once I was happy with the image density, I clicked on the “Extract Countour Lines” button, and the outlines appearedin the “Image outline” dialog window.


![“Image outline” dialog window.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500487116101_capture-09a.JPG)


Then, I pressed the “OK” button. The “Image outline” dialog window closed and the contour lines appeared over the background image in the screen program.

I selected the contour lines, and I moved them to the right of the work space.


![Contour lines moved away from the background image.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500487486636_capture-11.JPG)


Next, I selected the JPG image again and I deleted it. Note that I could also have left the image as this would not have affected the cut.  

Immediately after, I moved the contour lines back to its original place in order to perform the cut in the lower-left side of the material, and I created a rectangular box around the contour lines.


![Outlines ready to be sent to the vinyl cutter.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500574960457_capture-12+1.JPG)


Finally, I clicked the “Cutting” button at the top menu bar of the program window, and I clicked “OK” in the “Cutting“ dialog window that appeared. The vinyl-cutter started to cut again! 

A few seconds later the cutting process had finished. As I did before, I held down the DOWN ARROW button in the machine’s operation panel to move the material toward the front, and I used an exacto knife to cut off the portion of the material that had just been cut.

Then, I used a pair of tweezers to peel off the surpass material, leaving only the typography.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1500631047277_IMG_1269.jpg)


After the two tests made with CutStudio, I definitely needed to find out what to make for completing this part of the assignment. I could not postpone this decision for more time!


### **Creating the assignment !?!? with Inkscape** 

Not a long time ago, I went to a sports center where I saw an outdoor paddle tennis court that had black vinyl bird silhouettes sticked to the glasses around the court. After wondering why for a while, I finally figured out that the stickers were there so the birds would not crash against the glasses and hurt themselves. Note that this kind of bird silhouette stickers can also be seen in the windows of tall buildings for the same reason.

In any case, making bird silhouette window stickers would have been nice. But instead of using the shape of a regular bird, I decided to use something a bit more… eccentric!? As I love dinosaurs since I am a child, I thought the making a Pterosaur silhouette window sticker would be fun.

I searched online for “Pterosaur”, and I found tons of images. Anyway, there was a drawing of a Pteranodon from the [Encyclopedia Britannica](https://www.britannica.com/) that would be perfect for being outlined, and that was the image I chosen for this exercise.


![Image of a Pteranodon obtained from the Encyclopedia Britannica.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506619388036_cutting-something-on-the-vinyl-cutter-34.jpg)


I opened Inkscape and imported the JPG file with the image of the Pteranodon that I had previously downloaded from the internet. And I arranged the document’s orientation by clicking on “File > Document Properties” and selecting “Orientation: Landscape”. 

Then, I scaled the image by clicking on “Object > Transform”, and changing the “Width” and “Height” parameters in the “Scale” tab of the “Transform” dialog window that appeared at the upper-right hand of the program window.


![Image of the Pteranodon after being imported and scaled in Inkscape.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506621608280_cutting-something-on-the-vinyl-cutter-35.jpg)


With the image of the Pteranodon scaled and centered, I renamed the existing layer to “Imagen” (image) and I locked it by clicking the lock icon at the left of the layer’s name.

Then, I created a new layer called “Trazado” (outline in spanish), and I drawn the Pteranodon’s contour using the “Draw Bezier curves and straight lines” tool. 

Note that I used red color for the path, so it could be properly seen after it was closed.


![Red outline drawn over the Pteranodon using Inkscape's “Draw Bezier curves and straight lines” tool.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506622188174_cutting-something-on-the-vinyl-cutter-36.jpg)


After the outline of the Pteranodon was closed and carefully adjusted to the Pteranodon image’s 
contour, I changed the path’s color to black and filled the shape (also with black color). 

In this way, I could get a better idea of how would it look later when cut in vinyl.


![Pteranodon's after being filled in black.](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506939680463_cutting-something-on-the-vinyl-cutter-37.jpg)


I liked theway it looked, so I removed the “Imagen” layer by right-clicking over the layer’s name and selecting “Delete the current layer” from the drop-down menu. 

Then, I made a rectangle around the Pterosaur’s silhouette using the “Create rectangles and squares” tool, so the amount of vinyl to be peeled later was bounded.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506939788923_cutting-something-on-the-vinyl-cutter-38.jpg)


Finally, I saved the drawing as “Pteranodon.svg” by clicking on “File > Save as…”, and selecting the right file extension from the dropdown menu at the lower-right corner of the “Select file to save to” dialog window. 

The Pterosaur’s silhouette was ready to be sent to the vinyl cutter!


### **Performing cutting with Fab Modules**

Initially, I was thinking on using CutStudio to send the Pterosaur’s silhouette to the vinyl cutter, but Santi suggested me that it would be great to use Fab Modules instead. As I am always happy to learn how to use new software and practicing with it, I heed his suggestion.

In any case, regardless of the software that was to be used, I had to start by preparing the material in the vinyl-cutter. I grab a roll of proper cutting vinyl that luckily had appeared in the lab, and I placed it in the vinyl-cutter following all the steps that I had learned before. 

Once material was correctly loaded, I made several cut tests until I found out that the right “FORCE” setting for this material was 110.  

With the material and the cutting parameters ready, I restarted the Lab’s computer and selected “Ubuntu” in the pre- boot menu.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506940802409_cutting-something-on-the-vinyl-cutter-39.jpg)


Then, I launched the “Terminal” application by clicking over the “Terminal” icon in the left-hand of the desktop screen. And once the “Terminal” application was opened, I wrote the word “fab” and I pressed the “Enter” key on the keyboard.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506940874203_cutting-something-on-the-vinyl-cutter-40.jpg)


As soon I pressed the “Enter” key, a new dialog window called “fab” appeared on the screen.

I selected “drawing (.svg)” from the drop-down menu under “from input format:”, and “Roland vinylcutter” (.camm) from the drop-down menu under “to output process:”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506940957413_cutting-something-on-the-vinyl-cutter-41.jpg)


Note that I left the “GUI size (pixel)” value at 400, which was the value that it had by default.

Then, I clicked the “make_svg_camm” button at the right hand of the “fab” dialog window, and a new dialog window under the name of  “make_svg_camm” appeared.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506941236599_cutting-something-on-the-vinyl-cutter-42.jpg)


After maximizing the “make_svg_camm” dialog window, I selected “vinyl” from the drop-down menu at the upper-center of the “make_svg_camm” dialog window.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506941325422_cutting-something-on-the-vinyl-cutter-43.jpg)


Next, I clicked the “load.svg” button under the leftmost white square, and I used the window browser that appeared to search and select the “Pteranodon.svg” file that I had previously prepared with Inkscape.

The white square under “from: svg” was filled with several lines of code representing the .svg file.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506941423623_cutting-something-on-the-vinyl-cutter-44.jpg)


Then, I clicked the “make .path” button under the rightmost white square, and the shape of the Pteranodon plus the rectangle that previously drawn using Inkscape appeared, showing also the tool path.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506941601595_cutting-something-on-the-vinyl-cutter-45.jpg)


I did not change any of the values that appeared under the white square with the Pteranodon’s path. But I did change the “force (g)” value under “to: camm” at the upper-right hand of the “make_svg_camm” dialog window to 110, as that was the value that I have previously confirmed to be the right one for the vinyl material that I was going to cut. 

I also changed the “velocity (cm/2)” value to 15. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506943455762_cutting-something-on-the-vinyl-cutter-46.jpg)


Once the “force (g)” and “velocity (cm/2)” values were set, I clicked the “make .camm” button and new button named “send it!” appeared above the “make .camm” button. 

I pressed the “send it!” button and the cutting job was sent to the vinyl cutter!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506943463347_cutting-something-on-the-vinyl-cutter-47.jpg)


Once the vinyl cutting job was finished, I held down the DOWN ARROW button in the vinyl-cutter’s operation panel to move the material towards the front.

Then, I used an utility knife to cut off the portion of the material that had been cut, and I peeled off the surpass material leaving only the Pteranodon’s shape.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506956037039_cutting-something-on-the-vinyl-cutter-48.jpg)



### **Using the transfer film**

The next thing I needed to do was moving the Pteranodon’s silhouette to a transfer film, so it could be later sticked to a glass, window, or any other surface I decided to stick it to.

I found a small roller up piece of transfer film in the lab, so I place it as stretched as I could over a table, and I removed the paper liner. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506957193253_cutting-something-on-the-vinyl-cutter-49.jpg)


Then I placed facing down the vinyl sheet with the Pteranodon’s shape over the transfer film and I made sure that the to parts were correctly sticked by using a plastic squeegee.

Note that I could have proceed al the way around, this is placing the vinyl sheet with the Pteranodon’s shape on the table and laying the transfer on top. But the transfer leftover was so rolled up that it would have been much more difficult to make it right.
 

![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506957281941_cutting-something-on-the-vinyl-cutter-50.jpg)


Then I cut the surpass material using an utility knife, a ruler and the straight sides of the rectangle around the Pteranodon’s silhouette a a guide.

Finally, I had my black vinyl Pteranodon silhouette sticker ready to be used.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_266F5FF124441352CB2D07A6820150AFC5DD0BCE56599ADF292FCF2905FCAD01_1506957418482_cutting-something-on-the-vinyl-cutter-51.jpg)



## **What I have learned**

how to test and use a laser cutter

parametric design

how to use a vinyl-cuter  



## **Issues that I had**

no issues with the design part

Laser cutting stopping all the time. machined needed maintenance.

Parametric design. I need to practice more.

Inkscape for laser cutting



## **Tools used** 


- **For designing the test parts:**
    - Mac OS X Sierra v10.12.6
    - Rhinoceros for Mac OS X v5.3.2


- **For laser-cutting the test parts:**
    - Windows OS !?!?!?!
    - Rhinoceros for Windows version !?!?!?
    - Trotec ControlJob version ?!?!?!
    - Trotec Speedy 400 laser-cutting machine
    - 3mm balsa wood leftovers
    - 3 mm acrylic leftovers
    - 4mm (1 layer) cardboard leftovers
    - Utility knife


- **For designing the parametric  press-fit construction kit:**
    - Mac OS X Sierra v10.12.6
    - Autodesk Fusion 360 v2.0.3410


- **For laser-cutting the parametric  press-fit construction kit:**
    - Windows OS !?!?!?!
    - Rhinoceros for Windows version !?!?!?
    - Trotec ControlJob version ?!?!?!
    - Trotec Speedy 400 laser-cutting machine
    - 4mm (1 layer) cardboard leftovers
    - Utility knife


- **For vinyl-cutting:**
    - Mac OS X Sierra Version 10.12.6
    - Inskcape for Mac OS X Version 0.91
    - Windows 7 Ultimate Service Pack 1 Version
    - RolandCutStudio Version
    - Fab Modules Version
    - Roland CAMM-1 GX-24 vinyl cutter
    - 500 mm wide matte black adhesive film
    - 500 mm wide matte black cutting vinyl
    - Transfer film
    - Utility knife
    - TweezersRuler
    - Cutting mat



## **Files to download**

