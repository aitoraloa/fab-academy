# Output Devices

Individual assignment: add an output device to a microcontroller board you've designed, and program it to do something

Group assignment: measure the power consumption of an output device


## **Which actuator board to make!?**

I still wanted to make all of them, and the first one this time was the hello.RGB.45 board.

Thus, I downloaded all the images (PNG) and programming (TXT) files available for the hello.RGB.45 board in the the Fab Academy’s Output Devices page.

![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527505393516_hello.RGB.45.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527505415027_hello.RGB.45.traces.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527505415023_hello.RGB.45.interior.png)




## **Redrawing the hello.RGB.45 board**

Repeated all the steps learned while Redrawing the Hello Echo-World board

Created a new project:

EAGLE → Projects > eagle > Outputs > hello.RGB.45

Designed the hello.RGB.45 schematic → components added from fab library


- 1x ATTINY45SI (Microcontroller)
- 1x FTDI-SMD-HEADER (FTDI header)
- 1x AVRISPSMD (ISP header)
- 1x PINHD-2X2-SMD (2x2 header)
- 1x LEDRGBNEW (RGB SMD LED)
- 1x REGULATORSOT23 (5V Voltage regulator)
- 1x CAP-UAS1206FAB (capacitor)
- 4x RES-US1206FAB (resistor)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527518074693_Captura+de+pantalla+2018-05-28+a+las+16.33.00+2.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1523989909890_Captura+de+pantalla+2018-04-17+a+las+19.26.49.png)


Laid out the board


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1523989909913_Captura+de+pantalla+2018-04-17+a+las+19.27.02.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527525655534_Captura+de+pantalla+2018-05-28+a+las+18.39.50.png)


Checked the design rules → Minimum clearance between objects in signal layers = 16mil

Everything ok!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527525667248_Captura+de+pantalla+2018-05-28+a+las+18.40.11.png)


Exported the hello.button.45 board

In the “Visible Layers” dialog Window, hide all layers except “Top” and “Dimension”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1523989909870_Captura+de+pantalla+2018-04-17+a+las+20.29.51.png)


File > Export > Image 

In the “Export Image” dialog window


- Monochrome
- Resolution = 500 dpi
- Area = Full


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527525783622_hello.RGB.45.png)


Used Gimp for creating a white border around the image:

opened the exported PNG file
select the PCB using the inner white line
“Image > “Crop to selection”
“Image > canvas size”

  Width = +20 
  Height = +20 
  Under “Offset”, click “Center” button

Click “Resize” button
Create a new layer with white background and place in under the traces layer
“Layer > Merge down”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527526934629_hello.RGB.45_traces.png)


And used also Gimp for creating the outline cut PNG file

selected the traces
“Edit > Fill with BG color” or “Edit > Fill with FG color”, depending which one is black
“Colors > Invert”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527526953886_hello.RGB.45_outline.png)



## **Making the hello.RGB.45 board**

**Milling the hello.RGB.45 pcb**

Fab Modules
Roland SRM-20 Desktop Milling Machine
Roland VPanel for SRM-20
one sided FR1 PCB
1/64” milling bit for the traces
1/32” milling bit for the outline


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527588490959_IMG-3500.jpg)


### **Assembling the hello.RGB.45 board**


- 1x AVR ATTiny 45 Microcontroller IC 8-Bit 10MHz 4KB (2K x 16) FLASH 8-SOIC
- 1x LED RGB Diffused 4PLCC 3.2V SMD 
- 1x Linear Voltage Regulator IC Positive Fixed 1 Output 5V 100mA SOT-23-3
- 1x 6 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x 4 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x 1µF ±10% 50V Ceramic Capacitor X7R 1206 SMD
- 1x 10k Ohm 1/4W 1% Resistor 1206 SMD
- 2x 1k Ohm 1/4W 1% Resistor 1206 SMD
- 1x 499 Ohm 1/4W 1% Resistor 1206 SMD


- ATTiny 45 microcontroller → [Digi-key Part Number: ATTINY45V-10SU-ND](https://www.digikey.es/products/es?keywords=ATTINY45V-10SU-ND%09)
- LED RGB →  [Digi-key Part Number: CLV1A-FKB-CK1N1G1BB7R4S3CT-ND](https://www.digikey.es/products/es?keywords=CLV1A-FKB-CK1N1G1BB7R4S3CT-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 4 pin ISP header → [Digi-key Part Number: 609-5160-1-ND](https://www.digikey.com/products/es?keywords=609-5160-1-ND)
- 5V Regulator  → [Digi-key Part Number: LM3480IM3-5.0/NOPBCT-ND](https://www.digikey.es/products/es?keywords=LM3480IM3-5.0%2FNOPBCT-ND%09)
- 1uF capacitor → [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- 1k Ohm resistor → [Digi-key Part Number: 311-1.00KFRCT-ND](https://www.digikey.com/products/en?keywords=311-1.00KFRCT-ND%09)
- 499 Ohm resistor → [Digi-key Part Number: 311-499FRCT-ND](https://www.digikey.com/products/en?keywords=311-499FRCT-ND%09)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527588523949_IMG-3501.jpg)


Soldered the components to the board


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527588523966_IMG-3502.jpg)




## **Making the power cable**

The hello.RGB.45 needed to be powered in order to program it.

In order to make a cable to supply power to the hello.RGB.45 board I used a [USB-A to 5.5x2.1mm center-positive barrel jack cable](https://www.sparkfun.com/products/8639), and modified [5.5x2.1mm center-positive barrel jack to 2-pin JST cable](https://www.sparkfun.com/products/8734).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527592369942_IMG-3506.jpg)


Alternatively, I also had a [9V battery to 5.5x2.1mm center-positive barrel jack adaptor](https://www.sparkfun.com/products/9518) that I could use to power the board without the need of a USB port.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527592473145_IMG-3510.jpg)



## **Understanding the C code**

tbc


## **Programming the hello.RGB.45 board**

Created a new directory.

Copied the hello.RGB.45.c and hello.RGB.45.make files in the directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527588723235_Captura+de+pantalla+2018-05-29+a+las+12.11.34+2.png)


Connected the hello.RGB.45 to the computer using the power cable

Connected the FabISP to the computer using a miniUSB-A to USB-B cable

Connected the hello.RGB.45 and the FabISP using the IDC cable


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527592612688_IMG-3505.jpg)


Checked that everything was ok using the system profiler


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527593433378_Captura+de+pantalla+2018-05-29+a+las+13.29.39+2.png)


Opened terminal and navigated to the directory where the hello.bus.45.c and hello.bus.45.make files for had been saved

Typed


    make -f hello.RGB.45.make

Terminal response


    avr-gcc -mmcu=attiny45 -Wall -Os -DF_CPU=8000000 -I./ -o hello.RGB.45.out hello.RGB.45.c
    avr-objcopy -O ihex hello.RGB.45.out hello.RGB.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.RGB.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     338 bytes (8.3% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)

Meaning that the  “hello.RGB.45.c.hex” and  “hello.RGB.45.out” files had been created


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527594833861_Captura+de+pantalla+2018-05-29+a+las+13.53.35+2.png)


Then, I typed:


      make -f hello.rgb.45.make program-usbtiny

And the console prompted:


    avr-objcopy -O ihex hello.RGB.45.out hello.RGB.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.RGB.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     338 bytes (8.3% Full)
    (.text + .data + .bootloader)
    
    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)
    
    
    avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.RGB.45.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9206
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.RGB.45.c.hex"
    avrdude: input file hello.RGB.45.c.hex auto detected as Intel Hex
    avrdude: writing flash (338 bytes):
    
    Writing | ################################################## | 100% 0.39s
    
    avrdude: 338 bytes of flash written
    avrdude: verifying flash memory against hello.RGB.45.c.hex:
    avrdude: load data flash data from input file hello.RGB.45.c.hex:
    avrdude: input file hello.RGB.45.c.hex auto detected as Intel Hex
    avrdude: input file hello.RGB.45.c.hex contains 338 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 0.45s
    
    avrdude: verifying ...
    avrdude: 338 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:E2)
    
    avrdude done.  Thank you.

Meaning that hello.RGB.45 board had been programed.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_31368FC9E6EC8FBB285859E3A50C0DAA06F39E163AE17447654FAF722D1E70EA_1527609044338_rgb_mov_optomized.gif)



## **Completing the group project**

measure the power consumption of an output device



## **What I have learned**



## **Issues that I had**



## **Tools used**


- **For redrawing the board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - EAGLE v8.0.2


- **For preparing the milling files:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - GIMP v2.8.22


- **For making the board:**
    - Milling
    - Windows 7 Ultimate Service Pack 1
        - Roland VPanel for SRM-20
        - Roland SRM-20
        - Hex key
        - FR1 PCBs
        - 1/64” SE 2FL milling bit 
        - 1/32” SE 2FL milling bit
        - Double-sided tape
        - Spatula
        - Paper towels
        - Rubbing alcohol
        - Steel wool
    - Assembling
        - Soldering iron with a 0.5mm conical fine-point tip
        - Soldering iron stand with sponge
        - Distilled water
        - 0.35 mm soldering wire
        - Desoldering wick
        - Tweezers
        - Vacuum base PanaVise PCB holder
        - Solder fume extractor
        - Magnifying glass


- **For programming the board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - Terminal v2.8.2 (404)
    - FabISP programmer
    - USB-A to mini USB cable
    - USB-A to 5.5x2.1mm center-positive barrel jack cable,
    - modified 5.5x2.1mm center-positive barrel jack to 2-pin JST cable.
    - 2x2 pin header
    - 6 wire IDC ribbon cable


- **For measuring the power consumption:**
    - Multimeter



## **Files to download**