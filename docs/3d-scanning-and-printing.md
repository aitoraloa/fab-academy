# 3D Scanning and Printing

As the two previous weeks, [3D Scanning and Printing week](http://academy.cba.mit.edu/classes/scanning_printing/index.html) also included a group project. 

The aim of this group project was testing the design rules for the 3D printers available at Fab Lab Barcelona, which basically meant that we have to take these 3D printers to their limit by sending some specially designed prints. 

As individual project, I had to design and [3D print](https://en.wikipedia.org/wiki/3d_print) an object (small, few cm) that could not be made substractively, and I had to print this object in the most suitable 3D printer. 

Finally, I also had to [3D scan](https://en.wikipedia.org/wiki/3D_scanner) an object (and optionally print it)


## **The 3D printing workflow**

Testing the 3D printers was an interesting exercise because, besides letting us know the limitations that a 3d printer has, it would also help us to understand the 3D printing workflow. 

The 3D printing workflow is not exactly the same for every 3D printing machine, as each 3D printer has its own special characteristics and requirements, but in short it could be said that every 3D printing involves: 

### **CAD file development**

In order to produce a 3D object, first it is necessary to create its virtual design of this object by using any CAD software among the several that are available.


### **CAD file conversion** 

Once the CAD file has been developed, it will be necessary converting it into specific file format. 

The file formats would specified based on the 3D printing technology being employed by the particular 3D printer that were at using. For instance, if the printer is based on the [stereolithography technique](https://en.wikipedia.org/wiki/Stereolithography), the CAD file has to be converted into a [STL (standard tessellation language)](https://en.wikipedia.org/wiki/STL_(file_format)) 


### **STL file manipulation**

Once the STL format file is ready,  it will be also necessary to set the orientation and size of the object to be printed. This is similar to the case of a 2D printer.

The STL file manipulation can be done by using any of the 3D printing software available.

### **Preparing the printer**

With everything about the digital file ready, next step is making sure that the 3D printer is ready. 

This mainly means installing the material which is necessary to perform a 3D printing operation, but sometimes it also means doing other preparation operations such as the bed calibration.


### **The building up**

Based on the complexity of the object to be printed, the span of printing varies. 

At this point, all that one has to do is wait and perform random checks to make sure the everything is working flawlessly. Since the entire construction of the object is in form of very thin layers, it definitely is going to take some time.


### **Post Processing** 
 
This is not always necessary. But sometimes, once the building up process is done and the 3D object has been removed from the 3D printer, the part needs to be [post-processed](https://www.3dhubs.com/knowledge-base/post-processing-fdm-printed-parts).

The post-processing method varies based both on the 3D printing technology used and the quality of the results to be obtained.


## **Preparing the group project**

Once I had reviewed the 3d printing workflow, and following the directions given by Neil in order to test the 3D printers, I found out that there are a lot of torture test available online for testing the limits of a 3D printer.

At the time of this writing, [Thingiverse](https://www.thingiverse.com/) (which probably is the most extended digital design files sharing platform in the world) was an amazing source for [these kind of 3d printing utilities](https://www.thingiverse.com/search/page:1?q=Test+your+3D+printer&sa=&dwh=98595b6cc62d323). 

![Search results under "Test your 3D printer" in Thingiverse web site](https://www.dropbox.com/s/1phzmd8wl2f1fr6/learning-how-to-test-a-3d-printer-01.jpg?dl=1)


Along with the torture tests available in Thingiverse, there are also a lot of other interesting 3D printing utilities for testing 3D printers such as: [Temperature Towers tests](http://www.thingiverse.com/thing:2130067); [Dimensional tests](https://www.thingiverse.com/thing:1982686); [Tolerance tests](https://www.thingiverse.com/thing:636363); [Stringing tests](https://www.thingiverse.com/thing:1028352); [Bed Level tests](https://www.thingiverse.com/thing:2188146); [Bolt, Hole and Thread tests](https://www.thingiverse.com/thing:1686322); [Lithophane settings test](https://www.thingiverse.com/thing:2084570); [Flexibility tests](http://www.thingiverse.com/thing:220038); [Cooling tests](http://www.thingiverse.com/thing:2136663); and many more.

As I was not present in Fab Lab Barcelona that week either, I could not participate when they decided which torture test file to use. Anyway, I finally found out that the torture test they chosen and used was the classic “[Test your 3D printer!](https://www.thingiverse.com/thing:704409)” [](https://www.thingiverse.com/thing:704409)torture file by [CtrlV.](https://www.thingiverse.com/ctrlV/about)


!["Test your 3D printer!" torture-test page in Thingiverse](https://www.dropbox.com/s/y7nzdg85kcfc76l/learning-how-to-test-a-3d-printer-02.jpg?dl=1)


Not being present at the fab lab BCN during the 3D scanning and printing week, somehow “forced” me to make my own research. And after browsing through Thingiverse's torture test alternatives, I felt in love with a sweet little 3D print named #3DBenchy! 

3DBenchy is a 3D model specifically designed for testing and benchmarking 3D printers. 

It was developed in 2015 by the [Creative Tools design team in Sweden](https://www.creativetools.se/), and a huge amount of information and documentation has been generated around this cute torture test since then, including a growing enthusiastic community.


!["#3DBenchy" torture-test page in Thingiverse](https://www.dropbox.com/s/uvifkz925itua8c/learning-how-to-test-a-3d-printer-03.jpg?dl=1)


The 3D model (among other utilities) can be found and [downloaded from Thingiverse](http://www.thingiverse.com/thing:763622). But it is also available for download from other (very very interesting) 3D printing resources platforms like [Instructables](http://www.instructables.com/id/3DBenchy-The-tool-to-calibrate-and-test-your-3D-pr/), [Youmagine](https://www.youmagine.com/designs/3dbenchy-the-jolly-3d-printing-torture-test), and [Pinshape](https://pinshape.com/items/4786-3d-printed-3dbenchy-the-jolly-3d-printing-torture-test).

Furthermore, this project was so successful, that [it also has its own dedicated website](http://www.3dbenchy.com/), were you can find all the details about the project, download illustrative PDFs and posters, view other users 3D prints, or share your own 3D prints.


![#3DBenchy's dedicated web site](https://www.dropbox.com/s/rw037pdu0hew8x5/learning-how-to-test-a-3d-printer-04.jpg?dl=1)


For practical purposes, [I downloaded the #3DBenchy files from Thingiverse](http://www.thingiverse.com/thing:763622). 

The process of downloading files from Thingiverse was very simple. I just had to click on the [](https://www.thingiverse.com/thing:763622/zip)“DOWNLOAD ALL FILES” [](https://www.thingiverse.com/thing:763622/zip)(blue) button located in the upper right corner of the thing's page.


![" DOWNLOAD ALL FILES" button in #3DBenchy's Thingiverse web page](https://www.dropbox.com/s/7xmaamjg4fucr91/learning-how-to-test-a-3d-printer-05.jpg?dl=1)


This downloaded a ZIP file with all the files available for this part. 

In that particular case, by clicking the “DOWNLOAD ALL FILES” [](https://www.thingiverse.com/thing:763622/zip)button I downloaded the 3D part, but also an explanatory brochure and all the small parts that make up the piece.

Alternatively, I could also download the files individually by scrolling down the thing's page and clicking on the "Thing files" button, below the 3D printed parts slide show. 

A list of all the files related appeared below the button, and I just needed to select the file(s) that I wanted to download.


!["Thing Files" button with the list of files under it in #3DBenchy's Thingiverse web page](https://www.dropbox.com/s/ad8hm43ewkvuty7/learning-how-to-test-a-3d-printer-06.jpg?dl=1)


As I just wanted to print the entire #3DBenchy model and not the separated parts, I [clicked on “3Dbenchy.stl" and downloaded the single file](https://www.thingiverse.com/download:1223854). Note that I also erased the previously downloaded ZIP file from my computer so I did not end up with duplicated elements.

So far so good! I already had the [#3DBenchy’s STL file](https://en.wikipedia.org/wiki/STL_(file_format)) for testing the 3D printers, and it was about time to put my thoughts on the 3D printing machines.

## **Reviewing the 3D printers available**

At the moment I was conducting this assignment, there were four different 3D printing machine models available at Fab Lab Barcelona:

### [**BCN3D+**](https://www.bcn3dtechnologies.com/es/3d-printer/bcn3d-plus/) **by** [**BCN 3D Technologies**](https://www.bcn3dtechnologies.com/)

![Two of the BCN3D+ FDM printers available at Fab Lab Bcn](https://www.dropbox.com/s/bhne6v7xqd7r1oo/reviewing-the-3d-printers-available-01.jpg?dl=1)


These were open-frame modular [Fused Deposition Modeling (FDM)](https://en.wikipedia.org/wiki/Fused_deposition_modeling) 3D printers designed and developed by BCN3D Technologies in Barcelona. 

With a print volume of 240 x 210 x 200 mm and a hot (glass-build) bed, these machines could be used with 3.00 mm diameter filaments like ABS, PLA, PVA, HIPS, Nylon and/or Filaflex. Their maximum resolution was 100 microns.

They could work connected to a computer via USB (and controlling it with 3D printing host suites as [Repetier Host](https://www.repetier.com/) or [PronterFace](http://www.pronterface.com/)), or as standalone 3D printer using a [SD card](https://es.wikipedia.org/wiki/Secure_Digital). This second option was the way this machine was being used in Fab Lab Barcelona.

These machines were perfect to be used with open source 3D files processing software like [Cura](https://ultimaker.com/en/products/cura-software) or [Slicer](https://www.slicer.org/), but also with proprietary software like [Simplify3D](https://www.simplify3d.com/) or [Netfabb](https://www.autodesk.com/products/netfabb/overview). They were also MacOS, Windows or Linux compatible.

As stated in their website, these machines were "thought for covering the needs of both expert and novice profiles", but I was working at the Fab Lab when 2 assembled units of this machine arrived (I assembled the 3rd one myself during a 2 days workshop at BCN 3D Technologies headquarters in Casteldefels) and I would say that they were pretty... unreliable!? 

IMHO: They seemed to need continuous care (i.e.: replacing the 3d printed joints, multiple calibrations,...) in order to get good results, which end up being very tiring when having a high working load and lots of different people using then with very little concern. 

On the good side, the printer was also suitable for [paste printing](https://www.youtube.com/watch?v=vRuBullFlxs) and compatible with a dual extruder. Both [documentation](https://www.bcn3dtechnologies.com/es/bcn3d-plus-getting-started/) and customer service were great, as far as I can tell.

Around that time, these printers were already discontinued and replaced by the [BCN3D Sigma](https://www.bcn3dtechnologies.com/es/3d-printer/).


### **Ultimaker 2 by** [**Ultimaker**](https://ultimaker.com/)

![Ultimaker 2 FDM 3D printer available at Fab Lab Bcn](https://www.dropbox.com/s/l5s64t1kel5lw3x/reviewing-the-3d-printers-available-02.jpg?dl=1)


This was another FDM 3D printer but with a semi-open frame. The printing volume of this machine was 230 x 225 x 205 mm and it also had a heated (glass) build plate. 

ABS or PLA, Nylon, CPE, CPE+, PC, TPU 95A and other “exotic” 2.85 mm diameter filaments could be used. And the layer resolution when using a 0.80 mm nozzle was supposed to be up to 20 microns! 

As the BCN3D+ printers, it could work as standalone 3D printer using a SD card (the way it was being used) or connected to a computer via USB. It supported open-source hardware and software, so it was MacOS, Windows and Linux compatible too. 

As slicing software, the right choice for this machine was Cura (specially developed by Ultimaker for working with their machines), although 3rd party slicing software could be used too.

I had never used an Ultimaker before but their reputation preceded them and they also gained some important awards, so I was looking forward to the opportunity of working with it.

This 3D printer was also discontinued. And replaced by the [Ultimaker 2+](https://ultimaker.com/en/products/ultimaker-2-plus) and the [Ultimaker 3](https://ultimaker.com/en/products/ultimaker-3).


### **Form 1 by** [**Formlabs**](https://formlabs.com/)

![Form 1 SLA 3D printer available at Fab Lab Bcn](https://www.dropbox.com/s/wyf5npout8fq8iy/reviewing-the-3d-printers-available-03.jpg?dl=1)


This was a laser-based [SLA (Stereolithography)](https://en.wikipedia.org/wiki/Stereolithography) desktop 3d printer with a build volume of 125 x 125 x 165 cm that could print in transparent, white, grey, back and other resins up to 25 microns.

Unlike previous FDM machines, this 3D printer used [a dedicated software called Preform](https://formlabs.com/tools/preform/) in order to prepare the models. And it could only work when connected to a computer via USB.

While working at Fab Lab Barcelona, I had the chance to experiment a couple of times with this machine and, although its ability of more complex 3D printing and a better resolution, it proved to be pretty messy to work with resin. 

A dedicated workbench and separate work area for cleaning the printed models were needed. Along with a very methodic and careful usage that seemed not to have been carried out, as the machine was completely out of service.

In any case, the [documentation](https://support.formlabs.com/hc/en-us?utm_content=main-nav) and the customer support team were really amazing.

This 3D printer was also discontinued and replaced by the [Form 2](https://formlabs.com/3d-printers/form-2/?utm_content=main-nav).


### **Spectrum Z510 by Z Corporation**


![Spectrum Z510 MJ 3D printer available at Fab Lab Bcn](https://www.dropbox.com/s/k0ahieqcqr0c56i/reviewing-the-3d-printers-available-04.jpg?dl=1)


This was a high-definition and full-color Material Jetting (MJ) 3D printer manufactured by Z Corporation (acquired by [3D Systems](https://www.3dsystems.com/) in 2013) with a build size of 254 x 356 x 203 mm and a maximum resolution of 600 x 540 dpi. 

It used the [ZPrint proprietary software](http://infocenter.3dsystems.com/product-library/z-printer/zprinter-software). And it could accept solid models in STL, VRML PLY, and 3DS file formats as input. But only run on Microsoft Windows* NT, 2000 Professional, XP Professional and Vista.

The resulting printed parts consisted of porous plaster that could be infiltrated with epoxy later.

Unfortunately, I could not work with this machine. Much to my regret, this machine was also out of service at that time.


### [**M200 3D printer**](https://zortrax.com/printers/zortrax-m200/) **by** [**Zortrax**](https://zortrax.com/)

On the other hand, I also had access to a [M200 3D printer](https://zortrax.com/printers/zortrax-m200/) by [Zortrax](https://zortrax.com/)


![Zortrax M200 FDM 3D printer available at my friend's studio](https://www.dropbox.com/s/f1btba2b6ozcnz9/reviewing-the-3d-printers-available-05.jpg?dl=1)


Zortrax is a Polish manufacturer of FDM technology 3D printers and filaments. This machine was their flagship 3D printer at that time, although they already had 2 more models in the market.

It was an open-frame FDM 3D printer, but side covers and front door were available to purchase separately as an accessory and covert it in a semi-open (almost closed) 3D printer. 

The printing volume of this machine was 200 x 200 x 185 mm and it had a perforated heated build platform. The minimum layer height was 90 microns.

It also worked with [a dedicated software called Z-SUITE](https://zortrax.com/products/z-suite/) and 1.75 mm diameter filaments. 

The Z-SUITE software was created specifically for Zortrax machines to work with both Windows and Mac operating systems (unfortunately not linux). It could open STL, [OBJ](https://en.wikipedia.org/wiki/Object_file) or [DXF](https://en.wikipedia.org/wiki/AutoCAD_DXF) files in order to set the printing preferences and convert them into ZCODE files, that would be lately send to the machine via SD card. 

At that time, Zortrax M200 dedicated printing materials were: Z-ABS, Z-ULTRAT, Z-GLASS, Z-HIPS, Z-PCABS and Z-PETG. When I used this machine for the first time, third party filaments were not recommended at all but, incidentally  while writing these lines, they released a new version of their Z-SUITE software that allowed using them.

In any case, I was very lucky to have a friend with a Zortrax M200 3D printing machine and open to lend it to me for a couple of weeks. I had always read and heard that this machine was pretty robust and reliable. And I had also seen it working many times before but I never had the chance to work directly with one, so I felt really excited about the idea of having one just for me!


## **3D printing with the Zortrax M200**

One of the things that really impressed me when I went to pick up the machine to my friend's studio is how heavy this machine was if I compared it with any other printer I have had to carry/move before. It was the same feeling that I had when I lifted/carried a Technics SL1200 turntable for the first time, and these turntables are indestructible!

With the machine already at my place, I needed to find a good spot to locate it during the next two weeks. My desk turned out to be the most handy place, although I had to move the screen + laptop set up a little bit to accommodate the printer.


![Zortrax M200 3D printer accommodated in my house's desk](https://www.dropbox.com/s/css92jdld426ex1/3d-printing-with-the-zortrax-m200-01.jpg?dl=1)


### **Browsing through the menu**

Before going through the internet in search of all the documentation available for this machine (which was a lot!), I turned on the machine by pressing the ON/OFF button on the rear-left side corner of the machine, and I used the front knob to browse through the menus.

The Zortrax M200 machine's main menu had 5 main categories:


- “MODELS” → were I could select the part to be printed.


- “MAINTENANCE” → to auto calibrate the bed, heat the extruder, move the bed up/down and run a fan test.


- “MATERIAL” → for loading/unloading the filament


- "SETTINGS” → were I could change machine's LED behavior, enable/disable the buzzer, enable/disable the sleep mode, and select the language I wanted to use.


- “INFORMATION” → for checking the firmware version and other details of the machine such as the serial number.


![Zortrax M200 3D printer's display showing the main menu](https://www.dropbox.com/s/ca4fbemfzia3ic3/3d-printing-with-the-zortrax-m200-02.jpg?dl=1)


As commented above, the options available in the menu were not many and they were also pretty clear, so it did not take me long to go through all of them. 

The next step was to go deeper into the machine’s working characteristics and workflow! 


### **Reading the documentation**

In order to do that, I went online and searched for "Zortrax M200". 

The first result in the list was [Zortrax's website](https://zortrax.com/). I opened the website by clicking the link in the search list and I navigated through the site until I found all the information that I was looking for.

First stop was the [Zortrax M Series specifications web page](http://support.zortrax.com/m200-specifications/), were I was able to double-check very relevant information about the printer such as printing workspace, layer resolution, etc.


![Zortrax M Series specifications page at Zortrax Support Center web site](https://www.dropbox.com/s/izp4esbuevh2xj1/3d-printing-with-the-zortrax-m200-03.jpg?dl=1)


Second stop was the [Zortrax's Support Center web page](http://support.zortrax.com/), were I found all the things to have into account when using a Zortrax M200 3D printing machine. 

Obviously, the right spot for me in that webpage was the ["Quick Start"](http://support.zortrax.com/quick-start/) category. 


![Zortrax M200 Quick Start category page at Zortrax Support Center web site](https://www.dropbox.com/s/bqt512bzqzakyu7/3d-printing-with-the-zortrax-m200-04.jpg?dl=1)


### **Preparing the machine**

After clicking in the "Quick Start" category,  I [selected “Zortrax M200” in the machines’ menu](http://support.zortrax.com/quick-start/?printers=m200). And then, I went straight to the ["Preparation for the First Use of M200" tutorial](http://support.zortrax.com/preparation-for-the-first-use-m200/) that was available there.

The first step of that tutorial was “Prepare your Printer to Work" and it was related to the unboxing/mounting of the machine. But as the machine was already unboxed, mounted and ready to be used when I picked it up, I just skimmed through it until the last part, where it explained how to calibrate the machine. 

On the other hand, the 3D printer was supposed to be calibrated, but I wanted to see the process so I followed the suggested link and visited the [“Platform Calibration” video manual](http://support.zortrax.com/platform-calibration/) page.

As recommended by the manufacturer, before performing the calibration process I visited the [“M200 Platform Maintenance” web page](http://support.zortrax.com/platform-maintenance-m200/) and I ended up doing the whole maintenance procedure.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526378639622_IMG-2731.jpg)


With the platform back in the machine (and me back in the [platform calibration page](http://support.zortrax.com/platform-calibration/)), I followed the step by step guide until the whole calibration process was finished. 

The second step in the "Preparation for the First Use of M200" page was the "Z-Firmware installation".  At that point, the latest firmware available to download in [the “Firmware Updates for Zortrax M200” page](http://support.zortrax.com/downloads/) was version 1.0.5, which was the firmware version installed in the machine, so there was nothing else I should do there.


![](https://www.dropbox.com/s/dmo3jtsay3x6q00/3d-printing-with-the-zortrax-m200-06.jpg?dl=1)


### **Installing Z-suite**

The third step in the "Preparation for the First Use of M200" page was “Z-SUITE Software Installation”. And as Zortrax does have this proprietary software for preparing the files for their machines, I was forced to install the Z-SUITE application in my computer.

The whole installation process was nothing out of the ordinary. I just downloaded the latest Z-SUITE version available at [Zortrax Support’s Downloads page](http://support.zortrax.com/downloads/) and followed the installation instructions. 

The only remarkable thing during the installation process was that I needed the serial number of the printer at some point. In order to find this number, I selected “INFORMATION” in the machine’s display menu using the dial, and I pressed the dial over “Serial Number.” 


![](https://www.dropbox.com/s/jj8smjqi49odh5s/3d-printing-with-the-zortrax-m200-07.jpg?dl=1)



### **Loading the filament**

With the 3D printer calibrated and the Z-Suite software installed, it was time for “Loading the material” into the machine, which was also the fourth step in the "Preparation for the First Use of M200" tutorial.

Loading the material was also very easy, I just selected “MATERIAL” in the 3D printer’s menu display using the dial. And then, I pressed the dial over “Load the material”. 

When the display menu showed the message “Insert new material”, I inserted the free end of the filament into the extruder and I pressed the dial again.

After the machine finished loading the material… it was ready to start printing!


![](https://www.dropbox.com/s/msflsp8m1t1e9bd/3d-printing-with-the-zortrax-m200-08.jpg?dl=1)


At that point, the machine was completely ready to start sending prints but the file I had in my computer with #3DBenchy’s model was still an STL file. As I needed a ZCODE file for the machine to recognize it, I had to open the STL file using Z-SUITE and convert it.


### **Preparing the printing file**

In order to prepare the printing file, I launched the Z-SUITE application in my computer.

Then, I opened the *.STL file by pressing the “+” symbol at the top-right of the screen program.

As soon as I pressed the “+” symbol, a browser window opened allowing me to search for the file. 

Once I found the file, I selected it and I clicked the “Open” button.


![](https://www.dropbox.com/s/xje0225jwavlb40/3d-printing-with-the-zortrax-m200-09.jpg?dl=1)


With the 3D model of #3DBenchy opened in Z-SUITE, I played around with the buttons of the main menu at the right of the screen program. First, with the buttons under “View” that allowed me to rotate, move and change the view. And later, with the buttons under “Tools” that allowed me to rotate, move, resize, auto-arrange or split the 3D model.

Then I undid all the changes performed and clicked the “PRINT” button in the lower-left corner of the screen program. This opened a new window named “PRINT SETTINGS”, which as its name suggests, allowed me to choose among several options for printing:
 

- “MATERIAL TYPE” → where I could chose between all the Zortrax materials that were available at that time (Z-ABS, Z-GLASS, Z-HIPS, Z-PCABS, Z-PETG, Z-ULTRAT or Z-ESD).


- “LAYER THICKNESS” → that allowed me to choose the height of one layer (0.09mm, 0.14mm, 0.19mm, 0.29mm or 0,39mm) depending on the material.


- “QUALITY” → that could be HIGH or NORMAL, depending on the desired look for the print.


- “INFILL” → that could be MAXIMUM, HIGH, MEDIUM or LOW depending on the level of strenght that wanted for the finished part.


- “SUPPORT” → where I could select the angle (0º, 10º, 20º, 30º, 40º, 50º, 60º, 70º or 80º) at which the support structure was generated.


- “SUPPORT LITE” → to generate the support structure without the outer perimeter, meaning that the support would be more easier to remove.


- “FAN SPEED” → where I could select “AUTO” so the printer will adjust the fan speed accordingly. Or deselect “AUTO” and adjust the fan speed from 0% to  100%.


![](https://www.dropbox.com/s/p8v4cgpmsg2pi2r/3d-printing-with-the-zortrax-m200-10.jpg?dl=1)


At the end of the “PRINT SETTINGS” window, there was also an “ADVANCED SETTINGS” button. When I clicked on it, the “PRINT SETTINGS” window got bigger and new options appeared along with the ones that where previously available:


- “MATERIAL GROUP” → were I could select between “ZORTRAX MATERIALS” or “EXTERNAL MATERIALS” depending on the origin of the filament that I was going to use.


- “SEAM” → allowing me to choose the starting point of the new layer (“NORMAL” or “RANDOM”)


- More “INFILL” options → “MESH”, which prints the model with 0% infill with horizontal surfaces (top and bottom) and the walls, and “SHELL”, which prints the model with 0% infill and no top/bottom surface layers. 


- “OFFSETS” → were I could establish the offsets for “OUTER COUNTOUR” and “HOLES”, helping to correct the internal and external dimensions of a model.


- “SMART BRIDGES” → which enabled to print an object with small holes (max. 8mm).


- SURFACE LAYERS → were I could select the number of surface layers, in the “TOP” (from 4 to 10) and the “BOTTOM” (from 3 to 6), that I wanted for my print. 


![](https://www.dropbox.com/s/ih296szketxjvq7/3d-printing-with-the-zortrax-m200-11.jpg?dl=1)


After examining all the possibilities available with Z-SUITE, I realized that when choosing the “ZORTRAX MATERIALS” under “MATERIAL GROUP” the options available were not as many as with Cura, Slicer or other 3D printing software tools I have had used before. 

Even when clicking “ADVANCED SETTINGS” the options available to modify settings as nozzle or bed temperature, printing speed, retraction, etc. were null if using “ZORTRAX MATERIALS”.

When choosing “ADVANCED SETTINGS”, and then “EXTERNAL MATERIALS” under “MATERIAL GROUP”, I could easily modify parameters as the extrusion temperature, platform temperature, printing speed, retraction speed, retraction distance or extruder flow ratio. But it still was much more less options that other 3D printing processing software I had use before!


![](https://www.dropbox.com/s/saaathx641u4cpu/3d-printing-with-the-zortrax-m200-12.jpg?dl=1)


In any case, the [Z-SUITE manual](http://support.zortrax.com/z-suite-manual/) available in Zortrax's Support Center web page, was very useful in order to understand all the options given by this 3D printing software application.

As I was going to use Zortrax Z-ABS filament to print the #3Dbenchy (borrowed along with the machine), I had to be satisfied with the little options given by Z-SUITE for this material and trust in not encountering any problems within Zortrax’s ecosystem. 

At the end, although I would have liked to have the chance of playing around a little bit more with different settings and configurations, the Zortrax M200 3D printer proven to be bullet-proof (at least for small prints as I will explain later).

The (advanced) settings I selected in order to print the #3Dbenchy for the first time were:


- “MATERIAL GROUP” → ZORTRAX MATERIALS


- “MATERIAL TYPE” → Z-ABS


- “LAYER THICKNESS” → 0.14 mm


- “QUALITY” → HIGH


- “SEAM” → RANDOM


- “INFILL” → MEDIUM


- “SUPPORT” > “ANGLE” → 0º (meaning that not support was going to be printed)


- “SUPPORT” > “SUPPORT LITE” → Disabled


- “OFFSETS” > “OUTER CONTOURS” → 0.00


- “OFFSETS” > “HOLES” → 0.00


- “SMART BRIDGES” → Disabled


- “SURFACE LAYERS” > “TOP” → 9


- “SURFACE LAYERS” > “BOTTOM” → 4


- “FAN SPEED” → AUTO

Once the whole settings were settled, I clicked the “PREPARE TO PRINT” button at the lower-left corner of the window program. A progress bar appeared at the bottom of the screen program, meaning that the file was being prepared.


![](https://www.dropbox.com/s/7wm32tpionygddw/3d-printing-with-the-zortrax-m200-13.jpg?dl=1)


Once the progress bar arrived the 100%, the 3D model in the program screen showed how the 3D part was going to be built. The “[raft](https://www.simplify3d.com/support/articles/rafts-skirts-and-brims/)” had been added under the model, and I could also see a couple of interesting details as the “Estimated print time” or “Filament usage” in the bar at the bottom of the program screen. 


![](https://www.dropbox.com/s/4qzdzedu0bxwryx/3d-printing-with-the-zortrax-m200-14.jpg?dl=1)


Then, I clicked the “SAVE TO PRINT BUTTON” that appeared in the lower-left corner of the screen program after the file preparation finished (just were the “PRINT” button was before). 

And I saved the ZCODE file in the SD card that I had previously inserted in my computer.


### **Launching the 3D print**

Right after the file was saved, I removed the SD card from my computer and I inserted it in the 3D printer’s SD card slot, making sure that it was correctly oriented.


![](https://www.dropbox.com/s/v2asrduz9knm072/3d-printing-with-the-zortrax-m200-15.jpg?dl=1)


Then I used the 3D printer’s dial in order to navigate again though the menu.

I selected “Models” by pressing the dial.  

And then I selected “3DBenchy” from the parts list that appeared. 

Finally, I pressed the dial again over “Print: 3DBenchy.zcode” in order to select the printing file. 


![](https://www.dropbox.com/s/4sqaqn02j4034k5/3d-printing-with-the-zortrax-m200-16.jpg?dl=1)


The machine started to run after a couple of minutes of bed and nozzle warm up. And approximately 2 hours later, the 3D print was finished and… the results were pretty good! 

The hull did not show any surface deviations. Neither did the concentrical cylindrical shapes of the chimney. The horizontal surfaces of the deck, box and chimney were smooth, and the sloped surfaces of the gunwale and roof showed perfect steps. The large horizontal holes of the rear window and the boat’s wheel were perfect. Also the small horizontal hole of the hawsepipe and the slanted small holes of the fishing-rod-holder. The tiny surface details of the letters on the stern and the shallow letters at the bottom of the boat were pretty clear. And I would only highlight as something negative the overhang surfaces inside the bridge, where a couple of strings of filament were loose. 
[](http://www.3dbenchy.com/features/)

![](https://www.dropbox.com/s/wv31z0z7ln9yp8z/3d-printing-with-the-zortrax-m200-17.jpg?dl=1)


I was very satisfied with the outcome achieved with the Zortrax M200 when printing the #3DBenchy but I also thought that it would be pretty fair to use the same torture test that the rest of students used for this part of the assignment.

I downloaded the STL file of the [“Test your 3D printer!” torture test](https://www.thingiverse.com/thing:704409) from Thingiverse, I converted it to ZCODE using Z-SUITE, and I launched the 3D print with the Zortrax M200.

In this case, the limits of the machine where pretty clear (at least with the first 3D printings settings that I tried). The hole and nut sizes where ok. And the surfaces, overhangs and bridges where also pretty fine. But the minimum distances between walls and the numbers were not well at all. 


![](https://www.dropbox.com/s/ha1hlsyi3vcvezr/3d-printing-with-the-zortrax-m200-18.jpg?dl=1)


In subsequent prints of the “Test your 3D printer!” torture test with different settings I managed to improve the minimum distances between walls but some of the numbers were still illegible.

Anyway, as the most part of the prints I made where pretty decent I though it was time to go a step further in this assignment and design a 3D object that could not be made substractively.


## **Designing a 3D object that could no be made substractively** 

The main premise specified for designing the object that I would have to 3D print  later was that it could not be made substractively, meaning that it could only be made using an additive process.

I guessed that all these beautifully intricate mathematical sculptures that could be found on the internet were part of this group of geometries that can only be manufactured with additive manufacturing, so I did not rack my brain too much and I decided to make one of those.

If the truth be told, it was much more complicated to decide which software to use for this part of the assignment… so many options available!! I finally decided to use [Blender](https://www.blender.org/) this one time!


![](https://www.dropbox.com/s/kw33ijvxllx948d/designing-a-3d-object-that-could-not-be-made-substractively-01.jpg?dl=1)


The reason why I decided to use Blender for designing this part, was mainly that I had never used it before (although I had been interested in trying for a long time). But also because I found online a bunch of really really nice [video-tutorials for making high dimensional forms](https://www.youtube.com/user/luxxeon3d) using 3dsMax and Blender by an American 3D modeler and CG artist called [John Malcolm](https://www.artstation.com/artist/luxxeon).


![](https://www.dropbox.com/s/c2pipq28917jpt3/designing-a-3d-object-that-could-not-be-made-substractively-02.jpg?dl=1)


Among all these video tutorials, the one that really caught my attention while surfing through John Malcolm’s YouTube channel was [“Model A Nested Dodecahedron Object”](https://www.youtube.com/watch?v=7uuna3-wiuQ&t=2s). 

It was a 24 minutes video tutorial, meaning that it was not the shortest (neither the longest) of the video tutorials available in that YouTube channel, but the resulting model once the step by step guide was followed was so eye catching to me that I could no resist and I went for it!


![](https://www.dropbox.com/s/glsv0m4lxpbugz0/designing-a-3d-object-that-could-not-be-made-substractively-03.jpg?dl=1)


After [downloading Blender from the downloads page](https://www.blender.org/download/) and installing it on my Mac, I had to make some changes in the application’s preferences. Basically, I had to add a few specific add-ons developed by the [Blender community](https://www.blender.org/community/).

The fact was that in Blender only certain type of objects could be added by default, so the first change I had to do was activating an non-official extension called ["Add Mesh Extra Objects”](https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Add_Mesh/Add_Mesh_Extra_Objects) that allowed me to extends Blender’s functionality and add a regular object to the workspace using the “Object Type based on Math Calculations" option (not available by default). 

In order to do that, I clicked on “File” in the main menu bar and selected “User Preferences”. This opened the “Blender User Preferences” program window.


![](https://www.dropbox.com/s/bu3irxee772595m/designing-a-3d-object-that-could-not-be-made-substractively-04.jpg?dl=1)


Then, I selected “Add-ons” in the main menu at the top of the “Blender User Preferences” program window.  And I selected “Community” under “Supported Level” and “Add Mesh” under “Categories” in the menu at the right of the same window. 

In that way, all the mesh-related add-ons developed by the community were listed. 

Next, I selected the "Add Mesh Extra Objects” add-on and I enabled it by checking the box on the right of the add-on. Finally, I clicked on the “Save User Setting” button and the “Blender User Preferences” closed.


![](https://www.dropbox.com/s/wh0je7wt7x6poyp/designing-a-3d-object-that-could-not-be-made-substractively-05.jpg?dl=1)


The second change I had to do was activating also [the "Loop Tools” add-on](https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Modeling/LoopTools). 

This add-on  could be found under “Mesh”, so I basically repeated the previous process but selecting “Community” under “Supported Level” and “Mesh” under “Categories” once the the “Blender User Preferences” program window was opened again. 

Then, I selected the "Loop Tools” add-on and I activated it by checking the box on the right of the add-on.


![](https://www.dropbox.com/s/dvjkr0y7pqty2ea/designing-a-3d-object-that-could-not-be-made-substractively-06.jpg?dl=1)


After both the ["Add Mesh Extra Objects”](https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Add_Mesh/Add_Mesh_Extra_Objects) and ["Loop Tools”](https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Modeling/LoopTools) add-ons were activated, it was time to continue with the [“Model A Nested Dodecahedron Object”](https://www.youtube.com/watch?v=7uuna3-wiuQ&t=2s) video-tutorial. 

It took me a while and several viewings to complete the exercise but it was very fun, instructive, and… I finally had my Nested Dodecahedron completed! 

The final step before moving to the 3D printer was exporting the Nested Dodecahedron as STL by clicking on “File > Export > STL”,and saving the exported file in my computer.


![](https://www.dropbox.com/s/iilna7869xrof03/designing-a-3d-object-that-could-not-be-made-substractively-07.jpg?dl=1)



## **3D printing the designed object** 

I was sure that using the Form 1 or the Spectrum Z510 would have been much more appropriate than using a FDM 3D printer for the geometry of the Nested Dodecahedron. But both machines were our of service at that time, so I had to use a FDM 3D printer instead.

As I still had the Zortrax M200 available at home and it had proven to be the most reliable machine among the FDM 3D printing machines I had access to, I though that it would be interesting to give it a try with this geometry.

### **Opening the STL file**

Thus, I opened the previously exported STL file in Z-SUITE. 

Note that before the 3D model appeared on the screen, a dialog window called “Convert units” appeared exposing that the size of the model had been saved in inches, and asking me if I wanted to convert it to millimeters. I clicked on “YES” and the 3D model of the Nested Dodecahedron was placed on the 3D workspace. 


![](https://www.dropbox.com/s/vm0jx0hkprlusif/3d-printing-the-designed-object-01.jpg?dl=1)


### **Setting the size**

The size of the model was ~ 50 mm, which I though it was ok. Making it bigger could be an option for subsequent prints, but for the first print I left the size of the 3D model it as it was.

In any case, note that I did use “blender units” while designing the Nested Dodecahedron in Blender, so I did not know the real size of the object until I opened the model in Z-SUITE. Based on the information I had, one “blender unit” was supposed to be ~ 1 mm. 

On the other hand, I knew in advance that I was going to be able to set the size of the 3D model with any of the 3D processing software that I was going to use later, so the initial design size was not critical due to the topology of the object.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526463227988_Captura+de+pantalla+2018-05-16+a+las+11.33.27.png)


### **Setting the orientation**

While deciding the size for the first print of my Nested Dodecahedron, I realized that 3D model was not resting over the bed plane the way I thought it would be best for 3D printing it.

Thus, I re-oriented the Nested Dodecahedron by changing the view with the “Select View” button under “VIEW” and using the "Rotate Object” button under “TOOLS”, both in the menu at the right of the screen program. Then I clicked the “SAVE OBJECT” button to save a ZPROJ file the model in my computer with desired orientation.


![](https://www.dropbox.com/s/10h2rpr17xiu7ux/3d-printing-the-designed-object-02.jpg?dl=1)



### **Configuring the print settings**

Once the size and orientation of the Nested Dodecahedron in Z-SUITE were set, I clicked the “PRINT” button in the lower-left corner of the screen program to open the “PRINT SETTINGS” dialog window. 

In the “PRINT SETTINGS” dialog window, I configured the settings as follows:


- “MATERIAL GROUP” → ZORTRAX MATERIALS


- “MATERIAL TYPE” → Z-ABS


- “LAYER THICKNESS” → 0.14 mm


- “QUALITY” → HIGH


- “SEAM” → RANDOM


- “INFILL” → MEDIUM


- “SUPPORT” > “ANGLE” → 0º (meaning that not support was going to be printed)


- “SUPPORT” > “SUPPORT LITE” → Disabled


- “OFFSETS” > “OUTER CONTOURS” → 0.00


- “OFFSETS” > “HOLES” → 0.00


- “SMART BRIDGES” → Disabled


- “SURFACE LAYERS” > “TOP” → 4


- “SURFACE LAYERS” > “BOTTOM” → 4


- “FAN SPEED” → AUTO

Then, I clicked the “PREPARE TO PRINT” button at the lower-left corner of the window program. 

When progress bar arrived the 100% the “SAVE TO PRINT BUTTON” appeared in the lower-left corner of the screen program, I clicked on it and I saved the ZCODE file in a SD card.


### **Sending the print**

On the ZCODE file was saved in the SD card, I removed the SD card from my computer.

Then, I inserted the SD card in the 3D printer and I used the dial to launch the print. The 3D print started to run and the whole 3D printing process went well. 

Approximately 4 hours later, I had a physical model of the Nested Dodecahedron in my hands.


![](https://www.dropbox.com/s/i0ciaxpuynerlor/3d-printing-the-designed-object-03.jpg?dl=1)


### **Checking the 3D print**

The truth is that the print was not perfect at all. Some overhang surfaces were not totally smooth and few loose strings of filament could be seen in the lower half of the Nested Dodecahedron. 

That’a what happens when FDM printing a 3D model with lots of extreme overhangs or long bridges and not using supports. 

Hopefully, I would be able to print it using a SLA machine in the future.

The time had come for 3D scanning an object!
  


## **3D scanning an object**

3D scanning is a technique to capture the shape of an object using a 3D scanner. The result is a 3D file of the object on a computer, which can be saved, edited, and even 3D printed.

Although I had seen tons of times people 3D scanning at the lab, I have never been involved in such a process. Neither the scanning or the subsequent cleaning, etc.


### **Choosing which scanning method to use**

Among  the different 3D scanning technologies available there were two methods of 3D scanning that I was really interested to try: [structured light sensor](https://en.wikipedia.org/wiki/Structured-light_3D_scanner) and [photogrammetry](https://en.wikipedia.org/wiki/Photogrammetry).

Unfortunately, I only had time to try one of them, so I decided to go for the structured light sensor technology, leaving the photogrammetry for another time.


### **Using Kinect and Skanect to scan an object**

Structured light 3D scanning technology measures the deformation of a light pattern on a surface to 3D scan the shape of the surface. And the device used to measure that deformation it is called Structured-light 3D Scanner.

Structured-light 3D scanners measure the three-dimensional shape of an object using [projected light patterns](https://en.wikipedia.org/wiki/Structured_light) and a [camera](https://en.wikipedia.org/wiki/Camera) system. They are not particularly cheap but it is quite easy to make an affordable structured-light 3D scanning system using [Kinect](https://en.wikipedia.org/wiki/Kinect) and [Skanect software](http://skanect.occipital.com/).  

I had a Kinect at home and Luciana provided me with a Skanect Pro license, meaning that I had everything I needed to get started with 3D scanning.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526572055717_IMG-3400.jpg)


Installing Skanect on my computer was really easy, and I also found a lot  of interesting resources about how to 3D scan using a Kinect.

While reviewing these resources, I found a couple of accessories that would make the 3D canning process much more easy like these 3D printed [Kinect handle](https://www.thingiverse.com/thing:242208) and [Kinect tripod mount](https://www.thingiverse.com/thing:992426); or like the [IKEA’s Rotating Board](https://www.ikea.com/es/es/catalog/products/90074483/?query=SNUDDA+Estante+giratorioas).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526594938820_IMG-3403.jpg)


### **Learning how to use skanect**

Once I had Skanect Pro installed in my computer, I visited the [Skanect’s website Support page](http://skanect.occipital.com/support/) and I watched [all the video tutorials available there](https://www.youtube.com/channel/UC70CKZQPj_ZAJ0Osrm6TyTg).

After watching the video-tutorials, I had a rough idea about how to scan myself or small objects. And I also had a better idea about some of the post processing methods and tools available after the 3D scan was done.

Anyway, I search for more tutorials explaining the whole 3D scanning process with Kinect and Skanect, and I found a few interesting facts:


- Lighting is very important in order to get an even colored scan
- Walking around your model vs Using a spinning base
- It is always better to get a high frame rate → around 18-24 frames
- A Kinect is not the best option for scanning small objects


### **Setting up the scanning area**

Despite what I found about scanning small objects with the Kinect, I really wanted to try scanning a coupe of small objects that I had at my place.

In order to do so, I improvised a 3D scanning space using the kitchen’s table and a tripod.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526574229208_IMG-3395.jpg)


Unfortunately,  all my 3D scanning attempts with small objects were fruitless. 

Finally, I was able to complete a couple of scans, but the resulting resolution was so low that I could not consider that this exercise was full filled.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526575370899_Captura+de+pantalla+2018-05-17+a+las+18.42.27.png)


In order to complete the 3D scanning assignment satisfyingly, I though that scanning a bigger object or a person should work better.  But as I did not have a bigger object to scan at my place, my only option was scanning a person. 

Luckily, my neighbourg was happy to help me with this project, and I end up 3d scanning him. 

The light was no the best one, and I guess that the 3d scanning could be also improved. However, the resulting 3D model was much better that the small ones. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526556511614_Captura+de+pantalla+2018-05-17+a+las+13.28.14.png)


Once the scan was processed, I played around with the different Mesh, Geometry an Color tools available in Skanect. I found essential the “Move & Crop” tool in order to straighten the model and removing undesired parts. In any case, it was really fun to test all the options available.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526556914474_Captura+de+pantalla+2018-05-17+a+las+13.34.36.png)


On the other hand, I really wanted to try any of the external editors that I had seen in the video tutorials, so I clicked “External Edit” under “Mesh”, and I selected the “View Mesh in Explorer” option, in order to open the “edit_me.ply” file with [Meshlab](http://www.meshlab.net/).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526557099586_Captura_de_pantalla_2018-05-17_a_las_13_36_42.png)


### **Cleaning the mesh with Meshlab**

[MeshLab](https://en.wikipedia.org/wiki/MeshLab) is a free and open-source 3D [mesh processing](https://en.wikipedia.org/wiki/Geometry_processing) software system that provides a set of tools for editing, cleaning, healing, inspecting, rendering, and converting large [meshes](https://en.wikipedia.org/wiki/Polygon_mesh). 

In order to get a better idea about how this software works I visited the [Meshlab’s website Support page](http://www.meshlab.net/#support) in, and I watched the videos available under “[Meshlab Basics](https://www.youtube.com/playlist?list=PL8B1E816EAE236B4D)” and “[Cleaning](https://www.youtube.com/playlist?list=PLBBF41579E4B65566)”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526572513816_Captura+de+pantalla+2018-05-17+a+las+13.40.26.png)


After watching the videos, I played around for a few minutes with the visualization modes and the selection methods. I also applied some filters to the whole mesh or to mesh selections, but my computer’s response was always sow slow that I desisted.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526572536208_Captura+de+pantalla+2018-05-17+a+las+17.54.32.png)


In any case, I saved the slightly edited file.


### **Export as stl** 

Back in Skanect, I wanted to upload the slight changes I had done in Meshlab, so I clicked “External Edit” under “Mesh”, and I selected the “Reload edited mesh from disk” option.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526573076253_Captura_de_pantalla_2018-05-17_a_las_18_03_49BIS.png)


Once the “edit_me.ply” file was updated, I used the “Move & Crop” tool in order to prepare a model of my neighbour’s bust. The plan was to 3d print it as a small gift for his help!

Then, I clicked in Skanect’s main menu “Share” tab, and I used the ”Export Model” option to export the 3d model as STL file. From the Export Model options, I selected “STL” under “Format”, and I changed the “Scale” to “Milimeters”.  

Finally, I clicked the “Export” button.  


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526573656924_Captura+de+pantalla+2018-05-17+a+las+18.13.56.png)



## **3D Printing the scanned object**

Open the STL file with Z suite


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526574540421_Captura+de+pantalla+2018-05-17+a+las+18.28.36.png)


Scale model

Generate ZCODE files for Zortrax M200


- Print Settings Profile = Small Prints - Economy Mode

Export ZCODE files to SD card

Send to print


![](https://d2mxuefqeaa7sj.cloudfront.net/s_BA8C22579DAD7D0C9EF305EDA65CA8856D9645B051F9D7543513AC04CFA61935_1526594436151_IMG-3405.jpg)



## **What I have learned**

How to test a 3d printer

Printing within the Zortrax ecosystem

Parametric design with Fusion360

Using Kinect And Skanect for 3d scanning

Using Meshlab



## **Issues that I had**

Scanning small objects seemed impossible with the Kinect

Skanect crushing many times during processing

my computer crushing all the time


## **Tools used** 


- For CAD file development and conversion
  - MacBook Pro with macOS High Sierra v10.13.3
  - Blender v2.78c


- For STL file manipulation
  - MacBook Pro with macOS High Sierra v10.13.3
  - Zortrax Z-SUITE v1.13.1.1


- For preparing the 3D printer and building up
  - Zortrax M200
  - 1,75mm ABS Filament
  - SD Card Reader
  - SD Card
  - Tweezers
  - Pliers
  - Spatula
  - Safety Glasses
  - Safety Gloves
  
- For post-processing the 3D print
  - Scalpel


- For 3D scanning
  - MacBook Pro with macOS High Sierra v10.13.3
  - Microsoft Kinect
  - Camera Tripod
  - Skanect v1.9.1
  - Meshlab v2016.12


## **Files to download**

