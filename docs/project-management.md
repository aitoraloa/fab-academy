# Project Management

The aim of the [project management week](http://academy.cba.mit.edu/classes/project_management/index.html) assignment was learning how to use Git, and building a personal site in the class archive describing me and my final project. 

I did not know what to make as a final project yet, but I was very excited about learning Git and creating a website that could accommodate all the content in the best way possible. 


## **Learning Git**

As seen in the [wikipedia](https://es.wikipedia.org/wiki/Wikipedia:Portada), [Git](https://en.wikipedia.org/wiki/Git) is a [free and open source](https://en.wikipedia.org/wiki/Free_and_open-source_software) distributed [version control system](https://en.wikipedia.org/wiki/Version_control_system) for tracking changes in [computer files](https://en.wikipedia.org/wiki/Computer_file) and coordinating work on those files among multiple people. 

In order to start practicing with Git, I visited the [Git Project website](https://git-scm.com/).  

Most of the information available in the Git Project website, was nicely exposed by [Fiore](http://fabacademy.org/archives/2014/students/basile.fiore/) during the first half of his [version control recitation](https://vimeopro.com/academany/fab-2018/video/252229788).  Anyway, I enjoyed browsing the Git Project website and finding [an astonishing amount of official resources](https://git-scm.com/doc) to continue learning Git. 

I could not resist to click the “**Learn Git in your browser for free with** [**Try Git**](https://try.github.io/)” link at the welcome page, and if you are reading this, I really recommend you to do the same .

The [Code School](https://www.codeschool.com/) website where the “[Try Git](https://www.codeschool.com/courses/try-git)” course was located is also very recommendable.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522353297221_project-management-01.png)


After I earned my tryGit badge, I watched the second half of [Fiore](http://fabacademy.org/archives/2014/students/basile.fiore/)’s [version control recitation](https://vimeopro.com/academany/fab-2018/video/252229788), and then I felt confident enough as to install Git in my computer. 


## **Installing Git**

In order to install Git in my laptop, I needed to download it first. 

So, I went back to the [Git Project website](https://git-scm.com/), and I browsed to the [downloads page](https://git-scm.com/downloads) in search for the latest version available for Mac at that time. Once there, it was fair simple to find it, as the website seemed to recognize the most suitable version for my OS. I just had to click the “Download 2.16.2 for Mac” button, and the download started.

When the corresponding mountable [disk image](https://en.wikipedia.org/wiki/Disk_image) (.dmg file) was completely downloaded, I double-clicked it, and this prompted a new tab on Finder with the [installation package](https://en.wikipedia.org/wiki/Installer_(macOS)#Installer_package) (.pkg file) on it . 

Then, I double-clicked the .pkg file and the installation process started! From there on, I just had to follow the installation instructions until the installation process finished.  


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522438590159_project-management-02.png)


The first thing I did after Git was installed in my computer was opening the [Terminal](https://en.wikipedia.org/wiki/Terminal_(macOS)) application in my computer to double-check that the Git installation was correctly done. 

Once the Terminal window was open, I typed:

    $ git --version

Which confirmed that the version installed in my machine was the latest:

    git version 2.16.12 (Apple Git-98)

Then, I followed the instructions at the “[First-Time Git Setup](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)” section of [Pro Git book online version](https://git-scm.com/book/en/v2), and I set my user name and email address by typing: 

    $ git config --global user.name aitoraloa
    $ git config --global user.email aitoraloa@gmail.com

This last step was very important, because every Git commit done later would be using that identification information, and these user name and email address details were going to be immutably baked into all the future commits that I was going to make.

After my identity was created and checked, the next step was [generating a public SSH key](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key) so I could link my computer to [my remote Fab Academy student repository](https://gitlab.fabcloud.org/academany/fabacademy/2018/labs/barcelona/students/aitor-aloa/) in the [Fabcloud server.](https://gitlab.fabcloud.org/)

[SSH (Secure Shell)](https://en.wikipedia.org/wiki/Secure_Shell) is the network protocol that was going to allow me to access my remote repository during the whole Fab Academy in a secure way. Therefore, I needed to [generate a public SSH key pair](https://gitlab.fabcloud.org/help/ssh/README) in order to [add the SSH public key to my GitLab account](https://about.gitlab.com/2014/03/04/add-ssh-key-screencast/) later.

In order to generate the SSH key, I used the Terminal program again:

    $ ssh-keygen -t rsa -C "aitoraloa@gmail.com” -b 4096

And the terminal window prompted a few lines of information, including the location of my computer where the SSH key pair just created had been saved.
 
    Generating public/private rsa key pair.
    Enter file in which to save the key (/Users/aitoraloa/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /Users/aitoraloa/.ssh/id_rsa.
    Your public key has been saved in /User/aitoraloa/.ssh/id_rsa.pub.
    The key fingerprint is:
    45:b1:86:76:5d:f9:a3:ba:9b:92:00:30:9a:2e:b0:b0 aitoraloa@gmail.com
    The key's randomart image is:
    +--[ RSA 5196]----+
    | .+o.o.          |
    | .=. .           |
    | * .             |
    | . + .           |
    | S . . .         |
    | o E             |
    | ..=o.           |
    +-----------------+

Then, in order to access the generated SSH key, I typed:

    $ cat ~/.ssh/id_rsa.pub

And the public key was prompted in the console:

    ssh-rsa ABBBB3NzaC1yc2999990000ccc222lllgggQCw3qxUmh4/A0pL9n8H7f4D3CVd21AZZZ443DY/RkDIj5fjaldkjnotiu990903j4lMSO023NLZGUTWOPBN90SkZK2hpv7tkRkwuPSerI7h7hAKhBop0nmRVbjE31EiaHgq/0Jxqphz/6nwDxC89wkxYrLLCy96HxcGEJUzkXsNW0XwhxkO+7o62fl2HOP[CZVLKJDREALFRTIQ0A9BQWLKJMGTu37g+lUOIZASUYZQsFYhQEJHvIxKb43plk+UEyQtnK+sP7LqyMILKeyqjfl9FG2K48GAJ7V73xj4860t+2YC0d/2QGNwqxFvbnmhax/zj/07gkn385a aitoraloa@gmail.com

Next, I selected the prompted public key, and I copied it to the clipboard. 

Immediately after, I went back to my GitLab’s user account, and navigated to “User Settings > SSH Keys”, where I pasted the SSH public key from the clipboard into the "Key" field.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522529394959_project-management-03.png)


Then, I clicked the “Add key” green button, and the public key was added to my GitLab account.

Note that the field under “Title” was autocompleted with my email address when I pasted the SSH public key into the "Key" field and I did not consider necessary to change that information. Anyway, a more descriptive label for the new key could also have been used (i.e. “MacBook").


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522529535638_project-management-04.png)


Once the public SSH key was saved, the next thing I to do was initializing Git in my computer.

In any case, before doing that, I created a local repository in my computer’s desktop using the terminal window:

    $ cd ~/Desktop/
    $ mkdir aitor.aloa

And I navigated to it:

    $ cd ~/Desktop/aitor.aloa/

Once in my working repository, I initialized Git by typing:

    $ git init

At this point, I had a Git repository initialized in my laptop, and I could securely connect to my Fab Academy student repository in the Fabcloud server.

The next step was cloning my remote repository in GitLab to my local Git installation.

In order to do that, I needed my remote repository’s [URL](https://en.wikipedia.org/wiki/URL), so I went back to the [Fab Cloud,](https://gitlab.fabcloud.org/academany) and I navigated to “[Academany > Fab Academy > 2018 > labs > Barcelona > Barcelona students > aitor.aloa > Details](https://gitlab.fabcloud.org/academany/fabacademy/2018/labs/barcelona/students/aitor-aloa)”. 

Once there, I copied my remote repository’s URL in the clipboard.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522529549602_project-management-05.png)


Then, I went back to the Terminal program, and I used the URL that I had just copied in the command line, right after the  `git clone` command:

    $ git clone git@gitlab.fabcloud.org:academany/fabacademy/2018/labs/barcelona/students/aitor-aloa.git

Which prompted:

    Cloning into 'aitor-aloa'...
    The authenticity of host 'gitlab.fabcloud.org (13.59.248.79)' can't be established.
    ECDSA key fingerprint is SHA256:Rplnjz212wPLjTJcyAip0xozt0hOoFgT9hl5eZfxzd0.
    Are you sure you want to continue connecting (yes/no)?

I typed “yes”, and the cloning process continued:

    Warning: Permanently added 'gitlab.fabcloud.org,13.59.248.79' (ECDSA) to the list of known hosts.
    remote: Counting objects: 3, done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Receiving objects: 100% (3/3), done.

Meaning that my local Git repository and my remote repository were sync. Yes!

It was time to start moving files from the local repository to the remote repository in order to make my personal website available online but… I needed to build one first!! 😜 


## **Building my personal website**

As former web designer, [HTML (Hypertext Markup Language)](https://en.wikipedia.org/wiki/HTML) was one of the languages I had to learn. Unfortunately, in the last few years I have not done [web design](https://en.wikipedia.org/wiki/Web_design) at all, so I though that developing my Fab Academy webpage from scratch using HTML would be refreshing.

In any case, a couple of weeks after the academy started, I realized that writing the HTML code directly would be unreasonably time consuming (even using [Sublime](https://www.sublimetext.com/) or [Brackets](http://brackets.io/) as text editors). Specially, taking into account the huge amount of things that I had to learn/make during the Fab Academy course, and how extensively I did wanted to document each assignment.

For that reason, and despite I really liked the welcome page that I had created for my personal Fab Academy site using HTML, [CSS (Cascading Style Sheet)](https://en.wikipedia.org/wiki/Cascading_Style_Sheets) and [JS (JavaScript)](https://en.wikipedia.org/wiki/JavaScript), I decided that changing the web development plan would be the best idea.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522775539140_project-management-06.gif)


Once I had completely discarded the “html-from-scratch” option, the logic alternative seemed to be using one of the many open-source [CSS frameworks](https://en.wikipedia.org/wiki/CSS_framework) available online.

Among the dozens of [CSS Frameworks available](http://cssframeworks.org/), there were a couple of them that stood out above the rest: [Bootstrap](https://getbootstrap.com/) or [Foundation](https://foundation.zurb.com/). From the two, Bootstrap seemed to be incredibly popular. And thanks to that, there were a lot of resources available ([tutorials](https://www.w3schools.com/bootstrap4/default.asp), extra [plugins](https://speckyboy.com/plugins-for-extending-bootstrap/), [themes](https://startbootstrap.com/), etc).


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522771333340_project-management-07.png)


Anyhow, while getting to know Bootstrap better, I also uncovered [static web pages](https://en.wikipedia.org/wiki/Static_web_page) and [Markdown](https://en.wikipedia.org/wiki/Markdown).

After searching for information online, I still needed to understand the whole static web generator thing better, but it was pretty clear that [using a static site generator had much more advantages](https://learn.cloudcannon.com/jekyll/why-use-a-static-site-generator/) for me than making a dynamic CMS site:  less complexity, faster serving, more security, improved scalability, and version control. Just what I needed! 😄 

It is necessary to say, that the amount of static site generators available at that time was massive and that it was really hard for me to decide which one to go for. [Jeckyll](https://jekyllrb.com/), [Hugo](http://gohugo.io/) or any of the first options at [StaticGen](https://www.staticgen.com/) seemed really good choices to continue the static site creation path. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1525717597089_Captura+de+pantalla+2018-05-07+a+las+20.26.09.png)


Among all the options available at StaticGen, I finally opted for using [GitBook](https://www.gitbook.com/). 

GitBook is both an online platform for writing and hosting documentation, and an open source [book format and toolchain](https://github.com/GitbookIO/gitbook) for building beautiful books using [GitHub](https://github.com/)/Git and Markdown. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522771482099_project-management-08.png)


In my particular case, I was planning to write the documentation locally using Markdown (although I finally used [Dropbox Paper](https://www.dropbox.com/paper?no_redirect=1), which works as Markdown editor but on the cloud).

Then, this Markdown formatted documentation (along with images, etc.) would be hosted in my local Git repository, and pushed to my remote GitLab repository every time I wanted to update [my Fab Academy student website](http://fab.academany.org/2018/labs/barcelona/students/aitor-aloa/).

The point here was that I was going to use GitBook’s command line tool, but I was not going to use GitBook’s platform to output the content as a website. I was going use GitLab instead! 

In any case, I signed up myself in GitBook and I followed the whole registration process. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522774583267_project-management-09.png)


Once I had verified my GitBook registration, I created a “new book” (or repository) called “test” :


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522774627488_project-management-10.png)


Then, I used GitBook’s “Clone and push changes with Git” option to copy the URL of the new book/repository that I had just created.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522775589954_project-management-12.gif)


And finally, I went back to the terminal window, and I cloned the GitBook’s “test” repository into my local Git repository by typing:


    $ git clone https://git.gitbook.com/aitoraloa/test.git

Which created a second working directory called “test” in my Git installation. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522775969876_project-management-13.png)


At that point, I had two working directories in my Git installation. One empty directory named “aitor-aloa” and connected to GitLab; and another working directory named “test” and connected to GitBook that contained three Markdown files: README.md, SUMMARY.md and chapter1.md. 

As the files in the “test” working directory were structured as to be published as a GitBook document, I moved these files from the “test” directory into the “aitor-aloa” directory.

Then, I used [Brackets](http://brackets.io/) text editor to update the SUMMARY.md file in the “aitor-aloa” working directory, so the content of this summary file matched the Fab Academy 2018 schedule.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522777070378_project-management-14.png)


Once the file SUMMARY.md was updated, I created three new markdown files for the three weekly assignments that I was going to publish at that moment (About me, Principles and Practices, Project Management), and I saved them in the “aitor-aloa” directory.

Note that I also deleted the file “chapter1.md” from the “aitor-aloa” directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522777578197_project-management-15.png)


So far so good! I had the file structure for my GitBook-based Fab Academy student page ready in my local repository, and I could start pushing files to my remote GitLab repository. 

In other words, I could start developing my Fab Academy documentation by adding content to the markdown files that I had already in my working directory; I could create new markdown files for the weekly assignments as the Fab Academy progressed; and I could use Git to send all the locally-generated documentation to my GitLab repository, so my public Fab Academy student website would be regularly updated. 


## **Editing/Creating markdown files**

Markdown is a [lightweight markup language](https://en.wikipedia.org/wiki/Lightweight_markup_language) with [plain text](https://en.wikipedia.org/wiki/Plain_text) formatting [syntax](https://en.wikipedia.org/wiki/Syntax) that allows non-programming types to create documents, [rich text](https://en.wikipedia.org/wiki/Formatted_text), or HTML markup using any plain text editor.

At the moment of this writing, the amount of markdown editors available both for macOS, Windows or Linux was huge. From apps like [Mou](http://25.io/mou/), [Typora](https://typora.io/), [Caret](https://caret.io/) or [Haroopad](http://pad.haroopress.com/), to in-browse editors like [StackEdit](https://stackedit.io/) or [Dillinger](https://dillinger.io/), going through many of the most common text and source code editors like Sublime, Brackets, [Atom](https://atom.io/), [MacVim](http://macvim-dev.github.io/macvim/), [Emacs](https://emacsformacosx.com/) and many more.

Despite I would have loved to test any of the apps or in-browse editors available (and I will probably do it in the future), I preferred to keep the things simple and continued using Brackets as I did before when I edited the SUMMARY.md file.

Anyway, I found out that there was [a Brackets extension for previewing markdown files](https://github.com/gruehle/MarkdownPreview) that seemed really useful, so I followed the installation instructions available at the extension’s download page, and a few minutes later I was already using it (along with a [markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)) to preview and edit the markdown files in my working directory.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522929099056_project-management-16.png)


On the other hand, using Brackets to create new markdown files for any of the Fab Academy weekly assignments that I was already documenting, or that I was about to document (about_me.md, principles_and_practices.md, and project_management.md files), was very simple.

As simple as opening a new file by clicking “File > New” in the main menu bar, and saving it as “FILENAME.md”, where FILENAME was the name of the assignment and the .md extension established that the file was a Markdown file.

Once the whole markdown files edition and creation thing was clear, it was time to start using Git to send these files to my Fab Academy student repository.


## **Updating my remote repository**

The first thing I did in order to start sending the files from my local repository to my GitLab repository was going back to the Terminal application and making sure that I still was in the right working directory by typing:

    $ cd ~/Desktop/aitor.aloa/aitor-aloa

Then, I double-checked the content inside the working directory by typing:

    $ ls

Which basically prompted the name of the files within that directory:

    README.md SUMMARY.md about_me.md principles_and_practices.md project_management.md

Then, I checked the status of the Git repository by typing:

    $ git status

And the terminal window prompted:

    On branch master
    Your branch is up to date with 'origin/master'.
    Untracked files:
    (use "git add ..." to include in what will be committed)
    README.md
    SUMMARY.md
    about_me.md
    principles_and_practices.md
    project_management.md
    nothing added to commit but untracked files present (use "git add" to track)

Meaning that I had several files that had been added to my working directory but that these files needed to be “stagged” (added to the staging area) so they could be “committed” later and sent to the local Git repository. 

Therefore, I added all the files to the staging area by typing:

    $ git add .

Then, I used the “git status” command again, and the terminal window prompted:

    On branch master
    Your branch is up to date with 'origin/master'.
    Changes to be committed:
    (use "git reset HEAD ..." to unstage)
    new file:   README.md
    new file:   SUMMARY.md
    new file:   about_me.md
    new file:   principles_and_practices.md
    new file:   project_management.md

Meaning that the files had been added to the stage area, and I could proceed with the commit.

In order to make my first commit, I typed:

    $ git commit -m "1st commit”

And the terminal window prompted:

    [master 4525015] 1st commit
     5 files changed, 41 insertions(+)
     create mode 100644 README.md
     create mode 100644 SUMMARY.md
     create mode 100644 about_me.md
     create mode 100644 principles_and_practices.md
     create mode 100644 project_management.md

Meaning that the files had been committed, and the data was safely stored in my local database.

Finally, I just needed to send the files in my local Git repository to my remote GitLab repository.

In order to do that, I typed:

    $ git push origin master

And the terminal window prompted:

    Counting objects: 5, done.
    Delta compression using up to 8 threads.
    Compressing objects: 100% (4/4), done.
    Writing objects: 100% (5/5), 1.98 KiB | 1.98 MiB/s, done.
    Total 5 (delta 0), reused 3 (delta 0)
    To gitlab.fabcloud.org:academany/fabacademy/2018/labs/barcelona/students/aitor-aloa.git
    0ad001b..4525015  master -> master

Meaning that the files had been sent to the remote GitLab repository. 

And indeed they were! 🙌 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1522943909799_project-management-17.png)



## **Making my website visible**

At this point the only thing I have left to do in order to make my website visible for everybody in the [Fab Academy students directory](http://fab.academany.org/2018/people.html), was creating a valid [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/) configuration.

At the very beginning, I was not sure what did “GitLab Continuous Integration” exactly mean but, fortunately, Fiore’s lectures and the material available at [GitLab Documentation](https://docs.gitlab.com/ee/ci/README.html) site, helped me to solve the majority of the doubts I had.

After few minutes processing the information, I realized that I had no choice than to [add a .gitlab-ci.yml file](https://docs.gitlab.com/ee/ci/yaml/README.html) to the root directory of my GitLab repository, and configure it to use a [runner](https://docs.gitlab.com/ee/ci/runners/README.html), so each push or commit would triggered my CI [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html).

In order to do that, I navigated to the main page of my GitLab repository, and I pressed the “+” symbol available there, so I could add a new file.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1523041194504_project-management-18.png)


In the new page that opened, I added the name  `.gitlab-ci.yml` under “master/File name”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1523041341142_project-management-19.png)


And instead of using any of the templates available at the top dropdown menus (as explained during Fiore’s lecture), I pasted the [YAML](https://en.wikipedia.org/wiki/YAML) code that I had copied from the “[Example GitBook site using GitLab Pages](https://gitlab.com/pages/gitbook/blob/master/.gitlab-ci.yml)” repository, as it contained the definitions of how my project should be built.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1523042178549_project-management-20.png)


Then, I updated the "Commit message” at the bottom of the page to “Add CI”, and I pressed the “Commit changes” button in order to save the `.gitlab-ci.yml` file. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1523042178609_project-management-21.png)


Finally, I went back to my GitLab’s repository main page, and the newly committed file was there, along with all the markdown files that I had previously “pushed” from my local repository.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1523042549525_project-management-22.png)


A few minutes later, my GitBook-formatted [Fab academy student page](http://fab.academany.org/2018/labs/barcelona/students/aitor-aloa/) was online.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_CF7A2136D7BF5DA7E591A969B1DFE3764934E95A35B8414FED7C56A9CB16AC23_1523042721293_project-management-23.png)


From that point on, I just needed to copy the `.gitlab-ci.yml` that I had created in GitLab to my Git repository using the terminal window:

    $ git pull origin master

So my local branch would be up to date with 'origin/master’.

And continue updating my work!


## **What I have learned**

The amount of information handled during this first week was a bit overwhelming.

Anyway, it was very satisfying to see how everything ended up fitting and working well, despite all the initial frustration while trying to understand the whole version control procedure.

Maybe, I did not refresh my HTML skills as initially intended to do, but I learned a lot about the new website development tools that are available, and this will probably be really useful.

I still had to get used to all new things I learned but… it was just a matter of time and practice!  


## **Issues that I had**

Actually, I had not remarkable issues. 

All the resources provided during the recitations and available online for each of the exercises that needed to be completed during this week's assignment were extensive and really really helpful. 

Apart from a few error responses in the console due to errors while typing the commands, I guess that the main issue was understanding what I was doing.


## **Tools used** 

- MacBook Pro with macOS High Sierra v10.13.3
- Git v2.16.2
- GitLab Community Edition v10.7.1
- GitBook v3.2.3
- Terminal v2.8
- Brackets v1.12
- MarkdownPreview v1.0.11


